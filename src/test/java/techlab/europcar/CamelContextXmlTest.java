package techlab.europcar;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class,webEnvironment=SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CamelContextXmlTest  {

	// TODO Create test message bodies that work for the route(s) being tested
	// Expected message bodies
	
	@Produce(uri = "http4:localhost:8080/services/camel/resource")
	protected ProducerTemplate inputEndpoint;

	@EndpointInject(uri = "mock:output")
	protected MockEndpoint outputEndpoint;
	
	@Autowired
	CamelContext context;

	@Test
	public void testCamelRoute() throws Exception {

		// Create routes from the output endpoints to our mock endpoints so we can assert expectations
		AdviceWithRouteBuilder interceptFlow = new AdviceWithRouteBuilder() {

			@Override
			public void configure() throws Exception {
				// mock the for testing
				interceptFrom().
				to(outputEndpoint);
			}
		};
		
		context.getRouteDefinition("dispatcher").adviceWith(context, interceptFlow);
		
		String ts = new Long(System.currentTimeMillis()).toString();
		// Define some expectations
		outputEndpoint.expectedHeaderReceived("id", ts);
		//Run the test
		Object result =  inputEndpoint.requestBodyAndHeader(null, Exchange.HTTP_QUERY,"id="+ts);
		System.out.println("################# RESULT : " + result.toString());
		// Validate our expectations
		outputEndpoint.assertIsSatisfied();
	}

}
