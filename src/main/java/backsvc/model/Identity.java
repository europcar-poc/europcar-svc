package backsvc.model;

import io.swagger.annotations.ApiModel;
import org.joda.time.LocalDate;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Identity
 **/
@ApiModel(description="Identity")
public class Identity  {
  
  @ApiModelProperty(required = true, value = "")
  private String firstName = null;

  @ApiModelProperty(required = true, value = "")
  private String lastName = null;

  @ApiModelProperty(required = true, value = "")
  private LocalDate dateOfBirth = null;

  @ApiModelProperty(value = "")
  private String cityOfBirth = null;

  @ApiModelProperty(value = "")
  private String countryOfBirthCd = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private String countryOfBirthLb = null;


@XmlType(name="GenderEnum")
@XmlEnum(String.class)
public enum GenderEnum {

@XmlEnumValue("M") M(String.valueOf("M")), @XmlEnumValue("F") F(String.valueOf("F"));


    private String value;

    GenderEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static GenderEnum fromValue(String v) {
        for (GenderEnum b : GenderEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "")
  private GenderEnum gender = null;

  @ApiModelProperty(value = "")
  private String civility = null;

  @ApiModelProperty(value = "")
  private String countryOfResidenceCd = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private String countryOfResidenceLb = null;

  @ApiModelProperty(value = "")
  private String languageCd = null;

  @ApiModelProperty(value = "")
  private String prevLanguageCd = null;

  @ApiModelProperty(value = "The fiscal Code.")
 /**
   * The fiscal Code.  
  **/
  private String fiscalCd = null;
 /**
   * Get firstName
   * @return firstName
  **/
  @JsonProperty("firstName")
  @NotNull
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Identity firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

 /**
   * Get lastName
   * @return lastName
  **/
  @JsonProperty("lastName")
  @NotNull
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Identity lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

 /**
   * Get dateOfBirth
   * @return dateOfBirth
  **/
  @JsonProperty("dateOfBirth")
  @NotNull
  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public Identity dateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
    return this;
  }

 /**
   * Get cityOfBirth
   * @return cityOfBirth
  **/
  @JsonProperty("cityOfBirth")
  public String getCityOfBirth() {
    return cityOfBirth;
  }

  public void setCityOfBirth(String cityOfBirth) {
    this.cityOfBirth = cityOfBirth;
  }

  public Identity cityOfBirth(String cityOfBirth) {
    this.cityOfBirth = cityOfBirth;
    return this;
  }

 /**
   * Get countryOfBirthCd
   * @return countryOfBirthCd
  **/
  @JsonProperty("countryOfBirthCd")
  public String getCountryOfBirthCd() {
    return countryOfBirthCd;
  }

  public void setCountryOfBirthCd(String countryOfBirthCd) {
    this.countryOfBirthCd = countryOfBirthCd;
  }

  public Identity countryOfBirthCd(String countryOfBirthCd) {
    this.countryOfBirthCd = countryOfBirthCd;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * @return countryOfBirthLb
  **/
  @JsonProperty("countryOfBirthLb")
  public String getCountryOfBirthLb() {
    return countryOfBirthLb;
  }

  public void setCountryOfBirthLb(String countryOfBirthLb) {
    this.countryOfBirthLb = countryOfBirthLb;
  }

  public Identity countryOfBirthLb(String countryOfBirthLb) {
    this.countryOfBirthLb = countryOfBirthLb;
    return this;
  }

 /**
   * Get gender
   * @return gender
  **/
  @JsonProperty("gender")
  @NotNull
  public String getGender() {
    if (gender == null) {
      return null;
    }
    return gender.value();
  }

  public void setGender(GenderEnum gender) {
    this.gender = gender;
  }

  public Identity gender(GenderEnum gender) {
    this.gender = gender;
    return this;
  }

 /**
   * Get civility
   * @return civility
  **/
  @JsonProperty("civility")
  public String getCivility() {
    return civility;
  }

  public void setCivility(String civility) {
    this.civility = civility;
  }

  public Identity civility(String civility) {
    this.civility = civility;
    return this;
  }

 /**
   * Get countryOfResidenceCd
   * @return countryOfResidenceCd
  **/
  @JsonProperty("countryOfResidenceCd")
  public String getCountryOfResidenceCd() {
    return countryOfResidenceCd;
  }

  public void setCountryOfResidenceCd(String countryOfResidenceCd) {
    this.countryOfResidenceCd = countryOfResidenceCd;
  }

  public Identity countryOfResidenceCd(String countryOfResidenceCd) {
    this.countryOfResidenceCd = countryOfResidenceCd;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * @return countryOfResidenceLb
  **/
  @JsonProperty("countryOfResidenceLb")
  public String getCountryOfResidenceLb() {
    return countryOfResidenceLb;
  }

  public void setCountryOfResidenceLb(String countryOfResidenceLb) {
    this.countryOfResidenceLb = countryOfResidenceLb;
  }

  public Identity countryOfResidenceLb(String countryOfResidenceLb) {
    this.countryOfResidenceLb = countryOfResidenceLb;
    return this;
  }

 /**
   * Get languageCd
   * @return languageCd
  **/
  @JsonProperty("languageCd")
  public String getLanguageCd() {
    return languageCd;
  }

  public void setLanguageCd(String languageCd) {
    this.languageCd = languageCd;
  }

  public Identity languageCd(String languageCd) {
    this.languageCd = languageCd;
    return this;
  }

 /**
   * Get prevLanguageCd
   * @return prevLanguageCd
  **/
  @JsonProperty("prevLanguageCd")
  public String getPrevLanguageCd() {
    return prevLanguageCd;
  }

  public void setPrevLanguageCd(String prevLanguageCd) {
    this.prevLanguageCd = prevLanguageCd;
  }

  public Identity prevLanguageCd(String prevLanguageCd) {
    this.prevLanguageCd = prevLanguageCd;
    return this;
  }

 /**
   * The fiscal Code.
   * @return fiscalCd
  **/
  @JsonProperty("fiscalCd")
  public String getFiscalCd() {
    return fiscalCd;
  }

  public void setFiscalCd(String fiscalCd) {
    this.fiscalCd = fiscalCd;
  }

  public Identity fiscalCd(String fiscalCd) {
    this.fiscalCd = fiscalCd;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Identity {\n");
    
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    dateOfBirth: ").append(toIndentedString(dateOfBirth)).append("\n");
    sb.append("    cityOfBirth: ").append(toIndentedString(cityOfBirth)).append("\n");
    sb.append("    countryOfBirthCd: ").append(toIndentedString(countryOfBirthCd)).append("\n");
    sb.append("    countryOfBirthLb: ").append(toIndentedString(countryOfBirthLb)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    civility: ").append(toIndentedString(civility)).append("\n");
    sb.append("    countryOfResidenceCd: ").append(toIndentedString(countryOfResidenceCd)).append("\n");
    sb.append("    countryOfResidenceLb: ").append(toIndentedString(countryOfResidenceLb)).append("\n");
    sb.append("    languageCd: ").append(toIndentedString(languageCd)).append("\n");
    sb.append("    prevLanguageCd: ").append(toIndentedString(prevLanguageCd)).append("\n");
    sb.append("    fiscalCd: ").append(toIndentedString(fiscalCd)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

