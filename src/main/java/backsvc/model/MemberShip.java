package backsvc.model;

import io.swagger.annotations.ApiModel;
import org.joda.time.LocalDate;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Member Ship of the Customer
 **/
@ApiModel(description="Member Ship of the Customer")
public class MemberShip  {
  
  @ApiModelProperty(required = true, value = "")
  private String memberNbr = null;

  @ApiModelProperty(value = "Enrollement source rsleCode")
 /**
   * Enrollement source rsleCode  
  **/
  private String enrollementSourceCd = null;

  @ApiModelProperty(value = "Label ref table REF_LLTY_SRC_ENRL")
 /**
   * Label ref table REF_LLTY_SRC_ENRL  
  **/
  private String enrollementSourceLb = null;


@XmlType(name="StatusEnum")
@XmlEnum(String.class)
public enum StatusEnum {

@XmlEnumValue("A") A(String.valueOf("A")), @XmlEnumValue("P") P(String.valueOf("P")), @XmlEnumValue("C") C(String.valueOf("C")), @XmlEnumValue("K") K(String.valueOf("K"));


    private String value;

    StatusEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static StatusEnum fromValue(String v) {
        for (StatusEnum b : StatusEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "A= Approved, P= Pending, C= Cancel, K=special")
 /**
   * A= Approved, P= Pending, C= Cancel, K=special  
  **/
  private StatusEnum status = null;


@XmlType(name="IdfPrgmEnum")
@XmlEnum(String.class)
public enum IdfPrgmEnum {

@XmlEnumValue("PPC") PPC(String.valueOf("PPC"));


    private String value;

    IdfPrgmEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static IdfPrgmEnum fromValue(String v) {
        for (IdfPrgmEnum b : IdfPrgmEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "PPC= Privilege Program Europcar")
 /**
   * PPC= Privilege Program Europcar  
  **/
  private IdfPrgmEnum idfPrgm = null;

  @ApiModelProperty(value = "")
  private LocalDate enrolmentDate = null;

  @ApiModelProperty(value = "")
  private String changedBy = null;

  @ApiModelProperty(value = "")
  private String forcedLevelCd = null;

  @ApiModelProperty(value = "Label ref table REF_MBR_LLTY_FRCD_LVL")
 /**
   * Label ref table REF_MBR_LLTY_FRCD_LVL  
  **/
  private String forcedLevelLb = null;


@XmlType(name="VipOnInvitationEnum")
@XmlEnum(String.class)
public enum VipOnInvitationEnum {

@XmlEnumValue("Y") Y(String.valueOf("Y")), @XmlEnumValue("N") N(String.valueOf("N"));


    private String value;

    VipOnInvitationEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static VipOnInvitationEnum fromValue(String v) {
        for (VipOnInvitationEnum b : VipOnInvitationEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(value = "")
  private VipOnInvitationEnum vipOnInvitation = null;

  @ApiModelProperty(value = "Tier level Code = rptlCode")
 /**
   * Tier level Code = rptlCode  
  **/
  private String tierLevelCd = null;

  @ApiModelProperty(value = "Label ref table REF_PTNS_TIER_LVL")
 /**
   * Label ref table REF_PTNS_TIER_LVL  
  **/
  private String tierLevelLb = null;

  @ApiModelProperty(value = "")
  private LocalDate tierLevelstartDate = null;

  @ApiModelProperty(value = "")
  private LocalDate tierLevelEndDate = null;
 /**
   * Get memberNbr
   * @return memberNbr
  **/
  @JsonProperty("memberNbr")
  @NotNull
  public String getMemberNbr() {
    return memberNbr;
  }

  public void setMemberNbr(String memberNbr) {
    this.memberNbr = memberNbr;
  }

  public MemberShip memberNbr(String memberNbr) {
    this.memberNbr = memberNbr;
    return this;
  }

 /**
   * Enrollement source rsleCode
   * @return enrollementSourceCd
  **/
  @JsonProperty("enrollementSourceCd")
  public String getEnrollementSourceCd() {
    return enrollementSourceCd;
  }

  public void setEnrollementSourceCd(String enrollementSourceCd) {
    this.enrollementSourceCd = enrollementSourceCd;
  }

  public MemberShip enrollementSourceCd(String enrollementSourceCd) {
    this.enrollementSourceCd = enrollementSourceCd;
    return this;
  }

 /**
   * Label ref table REF_LLTY_SRC_ENRL
   * @return enrollementSourceLb
  **/
  @JsonProperty("enrollementSourceLb")
  public String getEnrollementSourceLb() {
    return enrollementSourceLb;
  }

  public void setEnrollementSourceLb(String enrollementSourceLb) {
    this.enrollementSourceLb = enrollementSourceLb;
  }

  public MemberShip enrollementSourceLb(String enrollementSourceLb) {
    this.enrollementSourceLb = enrollementSourceLb;
    return this;
  }

 /**
   * A&#x3D; Approved, P&#x3D; Pending, C&#x3D; Cancel, K&#x3D;special
   * @return status
  **/
  @JsonProperty("status")
  @NotNull
  public String getStatus() {
    if (status == null) {
      return null;
    }
    return status.value();
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public MemberShip status(StatusEnum status) {
    this.status = status;
    return this;
  }

 /**
   * PPC&#x3D; Privilege Program Europcar
   * @return idfPrgm
  **/
  @JsonProperty("idfPrgm")
  @NotNull
  public String getIdfPrgm() {
    if (idfPrgm == null) {
      return null;
    }
    return idfPrgm.value();
  }

  public void setIdfPrgm(IdfPrgmEnum idfPrgm) {
    this.idfPrgm = idfPrgm;
  }

  public MemberShip idfPrgm(IdfPrgmEnum idfPrgm) {
    this.idfPrgm = idfPrgm;
    return this;
  }

 /**
   * Get enrolmentDate
   * @return enrolmentDate
  **/
  @JsonProperty("enrolmentDate")
  public LocalDate getEnrolmentDate() {
    return enrolmentDate;
  }

  public void setEnrolmentDate(LocalDate enrolmentDate) {
    this.enrolmentDate = enrolmentDate;
  }

  public MemberShip enrolmentDate(LocalDate enrolmentDate) {
    this.enrolmentDate = enrolmentDate;
    return this;
  }

 /**
   * Get changedBy
   * @return changedBy
  **/
  @JsonProperty("changedBy")
  public String getChangedBy() {
    return changedBy;
  }

  public void setChangedBy(String changedBy) {
    this.changedBy = changedBy;
  }

  public MemberShip changedBy(String changedBy) {
    this.changedBy = changedBy;
    return this;
  }

 /**
   * Get forcedLevelCd
   * @return forcedLevelCd
  **/
  @JsonProperty("forcedLevelCd")
  public String getForcedLevelCd() {
    return forcedLevelCd;
  }

  public void setForcedLevelCd(String forcedLevelCd) {
    this.forcedLevelCd = forcedLevelCd;
  }

  public MemberShip forcedLevelCd(String forcedLevelCd) {
    this.forcedLevelCd = forcedLevelCd;
    return this;
  }

 /**
   * Label ref table REF_MBR_LLTY_FRCD_LVL
   * @return forcedLevelLb
  **/
  @JsonProperty("forcedLevelLb")
  public String getForcedLevelLb() {
    return forcedLevelLb;
  }

  public void setForcedLevelLb(String forcedLevelLb) {
    this.forcedLevelLb = forcedLevelLb;
  }

  public MemberShip forcedLevelLb(String forcedLevelLb) {
    this.forcedLevelLb = forcedLevelLb;
    return this;
  }

 /**
   * Get vipOnInvitation
   * @return vipOnInvitation
  **/
  @JsonProperty("vipOnInvitation")
  public String getVipOnInvitation() {
    if (vipOnInvitation == null) {
      return null;
    }
    return vipOnInvitation.value();
  }

  public void setVipOnInvitation(VipOnInvitationEnum vipOnInvitation) {
    this.vipOnInvitation = vipOnInvitation;
  }

  public MemberShip vipOnInvitation(VipOnInvitationEnum vipOnInvitation) {
    this.vipOnInvitation = vipOnInvitation;
    return this;
  }

 /**
   * Tier level Code &#x3D; rptlCode
   * @return tierLevelCd
  **/
  @JsonProperty("tierLevelCd")
  public String getTierLevelCd() {
    return tierLevelCd;
  }

  public void setTierLevelCd(String tierLevelCd) {
    this.tierLevelCd = tierLevelCd;
  }

  public MemberShip tierLevelCd(String tierLevelCd) {
    this.tierLevelCd = tierLevelCd;
    return this;
  }

 /**
   * Label ref table REF_PTNS_TIER_LVL
   * @return tierLevelLb
  **/
  @JsonProperty("tierLevelLb")
  public String getTierLevelLb() {
    return tierLevelLb;
  }

  public void setTierLevelLb(String tierLevelLb) {
    this.tierLevelLb = tierLevelLb;
  }

  public MemberShip tierLevelLb(String tierLevelLb) {
    this.tierLevelLb = tierLevelLb;
    return this;
  }

 /**
   * Get tierLevelstartDate
   * @return tierLevelstartDate
  **/
  @JsonProperty("tierLevelstartDate")
  public LocalDate getTierLevelstartDate() {
    return tierLevelstartDate;
  }

  public void setTierLevelstartDate(LocalDate tierLevelstartDate) {
    this.tierLevelstartDate = tierLevelstartDate;
  }

  public MemberShip tierLevelstartDate(LocalDate tierLevelstartDate) {
    this.tierLevelstartDate = tierLevelstartDate;
    return this;
  }

 /**
   * Get tierLevelEndDate
   * @return tierLevelEndDate
  **/
  @JsonProperty("tierLevelEndDate")
  public LocalDate getTierLevelEndDate() {
    return tierLevelEndDate;
  }

  public void setTierLevelEndDate(LocalDate tierLevelEndDate) {
    this.tierLevelEndDate = tierLevelEndDate;
  }

  public MemberShip tierLevelEndDate(LocalDate tierLevelEndDate) {
    this.tierLevelEndDate = tierLevelEndDate;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MemberShip {\n");
    
    sb.append("    memberNbr: ").append(toIndentedString(memberNbr)).append("\n");
    sb.append("    enrollementSourceCd: ").append(toIndentedString(enrollementSourceCd)).append("\n");
    sb.append("    enrollementSourceLb: ").append(toIndentedString(enrollementSourceLb)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    idfPrgm: ").append(toIndentedString(idfPrgm)).append("\n");
    sb.append("    enrolmentDate: ").append(toIndentedString(enrolmentDate)).append("\n");
    sb.append("    changedBy: ").append(toIndentedString(changedBy)).append("\n");
    sb.append("    forcedLevelCd: ").append(toIndentedString(forcedLevelCd)).append("\n");
    sb.append("    forcedLevelLb: ").append(toIndentedString(forcedLevelLb)).append("\n");
    sb.append("    vipOnInvitation: ").append(toIndentedString(vipOnInvitation)).append("\n");
    sb.append("    tierLevelCd: ").append(toIndentedString(tierLevelCd)).append("\n");
    sb.append("    tierLevelLb: ").append(toIndentedString(tierLevelLb)).append("\n");
    sb.append("    tierLevelstartDate: ").append(toIndentedString(tierLevelstartDate)).append("\n");
    sb.append("    tierLevelEndDate: ").append(toIndentedString(tierLevelEndDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

