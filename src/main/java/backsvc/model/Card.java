package backsvc.model;

import io.swagger.annotations.ApiModel;
import org.joda.time.LocalDate;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Card
 **/
@ApiModel(description="Card")
public class Card  {
  
  @ApiModelProperty(value = "The value MUST be null in the create operation.")
 /**
   * The value MUST be null in the create operation.  
  **/
  private String id = null;


@XmlType(name="TypeCdEnum")
@XmlEnum(String.class)
public enum TypeCdEnum {

@XmlEnumValue("CID") CID(String.valueOf("CID")), @XmlEnumValue("PAS") PAS(String.valueOf("PAS")), @XmlEnumValue("DVL") DVL(String.valueOf("DVL")), @XmlEnumValue("ECC") ECC(String.valueOf("ECC")), @XmlEnumValue("PTN") PTN(String.valueOf("PTN")), @XmlEnumValue("EID") EID(String.valueOf("EID")), @XmlEnumValue("PAY") PAY(String.valueOf("PAY"));


    private String value;

    TypeCdEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static TypeCdEnum fromValue(String v) {
        for (TypeCdEnum b : TypeCdEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "It's possible to define many cards with the same typeCd by customer.  CID= Id card, PAS= Passport, DVL= Driving license, ECC= EuropcarCard, PTN= Partner Card, EID= Privilege card, PAY= Preferred payement card")
 /**
   * It's possible to define many cards with the same typeCd by customer.  CID= Id card, PAS= Passport, DVL= Driving license, ECC= EuropcarCard, PTN= Partner Card, EID= Privilege card, PAY= Preferred payement card  
  **/
  private TypeCdEnum typeCd = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private String typeLb = null;

  @ApiModelProperty(value = "Each type of card can have a different sub type domain. Used for exemple to descrybe type of driving licence")
 /**
   * Each type of card can have a different sub type domain. Used for exemple to descrybe type of driving licence  
  **/
  private String subTypeCd = null;

  @ApiModelProperty(value = "")
  private String subTypeDesc = null;

  @ApiModelProperty(required = true, value = "")
  private String number = null;

  @ApiModelProperty(value = "")
  private String issueCountryCd = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private String issueCountryLb = null;

  @ApiModelProperty(value = "")
  private String issueCity = null;

  @ApiModelProperty(value = "")
  private String issueAuthority = null;

  @ApiModelProperty(value = "")
  private LocalDate issueDate = null;

  @ApiModelProperty(value = "")
  private LocalDate expirationDate = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private Integer confidenceRank = null;
 /**
   * The value MUST be null in the create operation.
   * @return id
  **/
  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Card id(String id) {
    this.id = id;
    return this;
  }

 /**
   * It&#39;s possible to define many cards with the same typeCd by customer.  CID&#x3D; Id card, PAS&#x3D; Passport, DVL&#x3D; Driving license, ECC&#x3D; EuropcarCard, PTN&#x3D; Partner Card, EID&#x3D; Privilege card, PAY&#x3D; Preferred payement card
   * @return typeCd
  **/
  @JsonProperty("typeCd")
  @NotNull
  public String getTypeCd() {
    if (typeCd == null) {
      return null;
    }
    return typeCd.value();
  }

  public void setTypeCd(TypeCdEnum typeCd) {
    this.typeCd = typeCd;
  }

  public Card typeCd(TypeCdEnum typeCd) {
    this.typeCd = typeCd;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * @return typeLb
  **/
  @JsonProperty("typeLb")
  public String getTypeLb() {
    return typeLb;
  }

  public void setTypeLb(String typeLb) {
    this.typeLb = typeLb;
  }

  public Card typeLb(String typeLb) {
    this.typeLb = typeLb;
    return this;
  }

 /**
   * Each type of card can have a different sub type domain. Used for exemple to descrybe type of driving licence
   * @return subTypeCd
  **/
  @JsonProperty("subTypeCd")
  public String getSubTypeCd() {
    return subTypeCd;
  }

  public void setSubTypeCd(String subTypeCd) {
    this.subTypeCd = subTypeCd;
  }

  public Card subTypeCd(String subTypeCd) {
    this.subTypeCd = subTypeCd;
    return this;
  }

 /**
   * Get subTypeDesc
   * @return subTypeDesc
  **/
  @JsonProperty("subTypeDesc")
  public String getSubTypeDesc() {
    return subTypeDesc;
  }

  public void setSubTypeDesc(String subTypeDesc) {
    this.subTypeDesc = subTypeDesc;
  }

  public Card subTypeDesc(String subTypeDesc) {
    this.subTypeDesc = subTypeDesc;
    return this;
  }

 /**
   * Get number
   * @return number
  **/
  @JsonProperty("number")
  @NotNull
  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Card number(String number) {
    this.number = number;
    return this;
  }

 /**
   * Get issueCountryCd
   * @return issueCountryCd
  **/
  @JsonProperty("issueCountryCd")
  public String getIssueCountryCd() {
    return issueCountryCd;
  }

  public void setIssueCountryCd(String issueCountryCd) {
    this.issueCountryCd = issueCountryCd;
  }

  public Card issueCountryCd(String issueCountryCd) {
    this.issueCountryCd = issueCountryCd;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * @return issueCountryLb
  **/
  @JsonProperty("issueCountryLb")
  public String getIssueCountryLb() {
    return issueCountryLb;
  }

  public void setIssueCountryLb(String issueCountryLb) {
    this.issueCountryLb = issueCountryLb;
  }

  public Card issueCountryLb(String issueCountryLb) {
    this.issueCountryLb = issueCountryLb;
    return this;
  }

 /**
   * Get issueCity
   * @return issueCity
  **/
  @JsonProperty("issueCity")
  public String getIssueCity() {
    return issueCity;
  }

  public void setIssueCity(String issueCity) {
    this.issueCity = issueCity;
  }

  public Card issueCity(String issueCity) {
    this.issueCity = issueCity;
    return this;
  }

 /**
   * Get issueAuthority
   * @return issueAuthority
  **/
  @JsonProperty("issueAuthority")
  public String getIssueAuthority() {
    return issueAuthority;
  }

  public void setIssueAuthority(String issueAuthority) {
    this.issueAuthority = issueAuthority;
  }

  public Card issueAuthority(String issueAuthority) {
    this.issueAuthority = issueAuthority;
    return this;
  }

 /**
   * Get issueDate
   * @return issueDate
  **/
  @JsonProperty("issueDate")
  public LocalDate getIssueDate() {
    return issueDate;
  }

  public void setIssueDate(LocalDate issueDate) {
    this.issueDate = issueDate;
  }

  public Card issueDate(LocalDate issueDate) {
    this.issueDate = issueDate;
    return this;
  }

 /**
   * Get expirationDate
   * @return expirationDate
  **/
  @JsonProperty("expirationDate")
  public LocalDate getExpirationDate() {
    return expirationDate;
  }

  public void setExpirationDate(LocalDate expirationDate) {
    this.expirationDate = expirationDate;
  }

  public Card expirationDate(LocalDate expirationDate) {
    this.expirationDate = expirationDate;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * minimum: 0
   * maximum: 99
   * @return confidenceRank
  **/
  @JsonProperty("confidenceRank")
 @Min(0) @Max(99)  public Integer getConfidenceRank() {
    return confidenceRank;
  }

  public void setConfidenceRank(Integer confidenceRank) {
    this.confidenceRank = confidenceRank;
  }

  public Card confidenceRank(Integer confidenceRank) {
    this.confidenceRank = confidenceRank;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Card {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    typeCd: ").append(toIndentedString(typeCd)).append("\n");
    sb.append("    typeLb: ").append(toIndentedString(typeLb)).append("\n");
    sb.append("    subTypeCd: ").append(toIndentedString(subTypeCd)).append("\n");
    sb.append("    subTypeDesc: ").append(toIndentedString(subTypeDesc)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    issueCountryCd: ").append(toIndentedString(issueCountryCd)).append("\n");
    sb.append("    issueCountryLb: ").append(toIndentedString(issueCountryLb)).append("\n");
    sb.append("    issueCity: ").append(toIndentedString(issueCity)).append("\n");
    sb.append("    issueAuthority: ").append(toIndentedString(issueAuthority)).append("\n");
    sb.append("    issueDate: ").append(toIndentedString(issueDate)).append("\n");
    sb.append("    expirationDate: ").append(toIndentedString(expirationDate)).append("\n");
    sb.append("    confidenceRank: ").append(toIndentedString(confidenceRank)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

