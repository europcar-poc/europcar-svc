package backsvc.model;

import io.swagger.annotations.ApiModel;
import java.util.Date;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Customer services, as eInvoicing preference, Taxeo ID
 **/
@ApiModel(description="Customer services, as eInvoicing preference, Taxeo ID")
public class CustService  {
  
  @ApiModelProperty(value = "The value MUST be null in the create operation.")
 /**
   * The value MUST be null in the create operation.  
  **/
  private String id = null;

  @ApiModelProperty(required = true, value = "It's possible to define only one service value for a given svcCd.  EINV= eInvoicing Preference, TAXEO= Taxeo ID of the customer Any new service code needs setup in the REF_CUST_SVC table, otherway, should not be processed by the API.")
 /**
   * It's possible to define only one service value for a given svcCd.  EINV= eInvoicing Preference, TAXEO= Taxeo ID of the customer Any new service code needs setup in the REF_CUST_SVC table, otherway, should not be processed by the API.  
  **/
  private String serviceCd = null;

  @ApiModelProperty(value = "This is the type code (INT, FLG, STR), which is used by CustRepo daemon only, when processing SVC message, is returned in the viewCustomer response")
 /**
   * This is the type code (INT, FLG, STR), which is used by CustRepo daemon only, when processing SVC message, is returned in the viewCustomer response  
  **/
  private String type = null;

  @ApiModelProperty(value = "Each service of Yes/No flag type will be saved into this field, as EINV, null value will not be saved(no choice)")
 /**
   * Each service of Yes/No flag type will be saved into this field, as EINV, null value will not be saved(no choice)  
  **/
  private Boolean prefFlag = null;

  @ApiModelProperty(value = "Each service of string or number(as TAXEO Id) type will be saved into this field, null value is not allowed ")
 /**
   * Each service of string or number(as TAXEO Id) type will be saved into this field, null value is not allowed   
  **/
  private String val = null;

  @ApiModelProperty(value = "userLogin who applied this update, ex, OITP510")
 /**
   * userLogin who applied this update, ex, OITP510  
  **/
  private String updatedBy = null;

  @ApiModelProperty(value = "Each time when possible, fill this field with the location information, where the update has been applied, for example, update by GWY GUI, this field is filled by the stationCode. For update through website, filled with \"moveup\"")
 /**
   * Each time when possible, fill this field with the location information, where the update has been applied, for example, update by GWY GUI, this field is filled by the stationCode. For update through website, filled with \"moveup\"  
  **/
  private String updateSrc = null;

  @ApiModelProperty(value = "Date-Time when the current value has been set.")
 /**
   * Date-Time when the current value has been set.  
  **/
  private Date updateAT = null;

  @ApiModelProperty(value = "application code which issued the last update, ex, GWY, API")
 /**
   * application code which issued the last update, ex, GWY, API  
  **/
  private String lastUpdateApp = null;
 /**
   * The value MUST be null in the create operation.
   * @return id
  **/
  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CustService id(String id) {
    this.id = id;
    return this;
  }

 /**
   * It&#39;s possible to define only one service value for a given svcCd.  EINV&#x3D; eInvoicing Preference, TAXEO&#x3D; Taxeo ID of the customer Any new service code needs setup in the REF_CUST_SVC table, otherway, should not be processed by the API.
   * @return serviceCd
  **/
  @JsonProperty("serviceCd")
  @NotNull
  public String getServiceCd() {
    return serviceCd;
  }

  public void setServiceCd(String serviceCd) {
    this.serviceCd = serviceCd;
  }

  public CustService serviceCd(String serviceCd) {
    this.serviceCd = serviceCd;
    return this;
  }

 /**
   * This is the type code (INT, FLG, STR), which is used by CustRepo daemon only, when processing SVC message, is returned in the viewCustomer response
   * @return type
  **/
  @JsonProperty("type")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public CustService type(String type) {
    this.type = type;
    return this;
  }

 /**
   * Each service of Yes/No flag type will be saved into this field, as EINV, null value will not be saved(no choice)
   * @return prefFlag
  **/
  @JsonProperty("prefFlag")
  public Boolean isPrefFlag() {
    return prefFlag;
  }

  public void setPrefFlag(Boolean prefFlag) {
    this.prefFlag = prefFlag;
  }

  public CustService prefFlag(Boolean prefFlag) {
    this.prefFlag = prefFlag;
    return this;
  }

 /**
   * Each service of string or number(as TAXEO Id) type will be saved into this field, null value is not allowed 
   * @return val
  **/
  @JsonProperty("val")
  public String getVal() {
    return val;
  }

  public void setVal(String val) {
    this.val = val;
  }

  public CustService val(String val) {
    this.val = val;
    return this;
  }

 /**
   * userLogin who applied this update, ex, OITP510
   * @return updatedBy
  **/
  @JsonProperty("updatedBy")
  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public CustService updatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

 /**
   * Each time when possible, fill this field with the location information, where the update has been applied, for example, update by GWY GUI, this field is filled by the stationCode. For update through website, filled with \&quot;moveup\&quot;
   * @return updateSrc
  **/
  @JsonProperty("updateSrc")
  public String getUpdateSrc() {
    return updateSrc;
  }

  public void setUpdateSrc(String updateSrc) {
    this.updateSrc = updateSrc;
  }

  public CustService updateSrc(String updateSrc) {
    this.updateSrc = updateSrc;
    return this;
  }

 /**
   * Date-Time when the current value has been set.
   * @return updateAT
  **/
  @JsonProperty("updateAT")
  public Date getUpdateAT() {
    return updateAT;
  }

  public void setUpdateAT(Date updateAT) {
    this.updateAT = updateAT;
  }

  public CustService updateAT(Date updateAT) {
    this.updateAT = updateAT;
    return this;
  }

 /**
   * application code which issued the last update, ex, GWY, API
   * @return lastUpdateApp
  **/
  @JsonProperty("lastUpdateApp")
  public String getLastUpdateApp() {
    return lastUpdateApp;
  }

  public void setLastUpdateApp(String lastUpdateApp) {
    this.lastUpdateApp = lastUpdateApp;
  }

  public CustService lastUpdateApp(String lastUpdateApp) {
    this.lastUpdateApp = lastUpdateApp;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustService {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    serviceCd: ").append(toIndentedString(serviceCd)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    prefFlag: ").append(toIndentedString(prefFlag)).append("\n");
    sb.append("    val: ").append(toIndentedString(val)).append("\n");
    sb.append("    updatedBy: ").append(toIndentedString(updatedBy)).append("\n");
    sb.append("    updateSrc: ").append(toIndentedString(updateSrc)).append("\n");
    sb.append("    updateAT: ").append(toIndentedString(updateAT)).append("\n");
    sb.append("    lastUpdateApp: ").append(toIndentedString(lastUpdateApp)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

