package backsvc.model;

import backsvc.model.Address;
import backsvc.model.Card;
import backsvc.model.CustService;
import backsvc.model.Email;
import backsvc.model.ExternalID;
import backsvc.model.Identity;
import backsvc.model.MemberShip;
import backsvc.model.Phone;
import backsvc.model.Status;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer  {
  
  @ApiModelProperty(required = true, value = "")
  private String id = null;

  @ApiModelProperty(required = true, value = "")
  private Identity identity = null;

  @ApiModelProperty(required = true, value = "")
  private Status status = null;

  @ApiModelProperty(value = "")
  private MemberShip memberShip = null;

  @ApiModelProperty(value = "")
  private List<Address> addresses = null;

  @ApiModelProperty(value = "")
  private List<Phone> phones = null;

  @ApiModelProperty(value = "")
  private List<Email> emails = null;

  @ApiModelProperty(value = "")
  private List<Card> cards = null;

  @ApiModelProperty(value = "")
  private List<CustService> custServices = null;

  @ApiModelProperty(value = "The value will be ignored in the update operation.")
 /**
   * The value will be ignored in the update operation.  
  **/
  private List<ExternalID> externalIDs = null;
 /**
   * Get id
   * @return id
  **/
  @JsonProperty("id")
  @NotNull
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Customer id(String id) {
    this.id = id;
    return this;
  }

 /**
   * Get identity
   * @return identity
  **/
  @JsonProperty("identity")
  @NotNull
  public Identity getIdentity() {
    return identity;
  }

  public void setIdentity(Identity identity) {
    this.identity = identity;
  }

  public Customer identity(Identity identity) {
    this.identity = identity;
    return this;
  }

 /**
   * Get status
   * @return status
  **/
  @JsonProperty("status")
  @NotNull
  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public Customer status(Status status) {
    this.status = status;
    return this;
  }

 /**
   * Get memberShip
   * @return memberShip
  **/
  @JsonProperty("memberShip")
  public MemberShip getMemberShip() {
    return memberShip;
  }

  public void setMemberShip(MemberShip memberShip) {
    this.memberShip = memberShip;
  }

  public Customer memberShip(MemberShip memberShip) {
    this.memberShip = memberShip;
    return this;
  }

 /**
   * Get addresses
   * @return addresses
  **/
  @JsonProperty("addresses")
  public List<Address> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<Address> addresses) {
    this.addresses = addresses;
  }

  public Customer addresses(List<Address> addresses) {
    this.addresses = addresses;
    return this;
  }

  public Customer addAddressesItem(Address addressesItem) {
    this.addresses.add(addressesItem);
    return this;
  }

 /**
   * Get phones
   * @return phones
  **/
  @JsonProperty("phones")
  public List<Phone> getPhones() {
    return phones;
  }

  public void setPhones(List<Phone> phones) {
    this.phones = phones;
  }

  public Customer phones(List<Phone> phones) {
    this.phones = phones;
    return this;
  }

  public Customer addPhonesItem(Phone phonesItem) {
    this.phones.add(phonesItem);
    return this;
  }

 /**
   * Get emails
   * @return emails
  **/
  @JsonProperty("emails")
  public List<Email> getEmails() {
    return emails;
  }

  public void setEmails(List<Email> emails) {
    this.emails = emails;
  }

  public Customer emails(List<Email> emails) {
    this.emails = emails;
    return this;
  }

  public Customer addEmailsItem(Email emailsItem) {
    this.emails.add(emailsItem);
    return this;
  }

 /**
   * Get cards
   * @return cards
  **/
  @JsonProperty("cards")
  public List<Card> getCards() {
    return cards;
  }

  public void setCards(List<Card> cards) {
    this.cards = cards;
  }

  public Customer cards(List<Card> cards) {
    this.cards = cards;
    return this;
  }

  public Customer addCardsItem(Card cardsItem) {
    this.cards.add(cardsItem);
    return this;
  }

 /**
   * Get custServices
   * @return custServices
  **/
  @JsonProperty("custServices")
  public List<CustService> getCustServices() {
    return custServices;
  }

  public void setCustServices(List<CustService> custServices) {
    this.custServices = custServices;
  }

  public Customer custServices(List<CustService> custServices) {
    this.custServices = custServices;
    return this;
  }

  public Customer addCustServicesItem(CustService custServicesItem) {
    this.custServices.add(custServicesItem);
    return this;
  }

 /**
   * The value will be ignored in the update operation.
   * @return externalIDs
  **/
  @JsonProperty("externalIDs")
  public List<ExternalID> getExternalIDs() {
    return externalIDs;
  }

  public void setExternalIDs(List<ExternalID> externalIDs) {
    this.externalIDs = externalIDs;
  }

  public Customer externalIDs(List<ExternalID> externalIDs) {
    this.externalIDs = externalIDs;
    return this;
  }

  public Customer addExternalIDsItem(ExternalID externalIDsItem) {
    this.externalIDs.add(externalIDsItem);
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Customer {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    identity: ").append(toIndentedString(identity)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    memberShip: ").append(toIndentedString(memberShip)).append("\n");
    sb.append("    addresses: ").append(toIndentedString(addresses)).append("\n");
    sb.append("    phones: ").append(toIndentedString(phones)).append("\n");
    sb.append("    emails: ").append(toIndentedString(emails)).append("\n");
    sb.append("    cards: ").append(toIndentedString(cards)).append("\n");
    sb.append("    custServices: ").append(toIndentedString(custServices)).append("\n");
    sb.append("    externalIDs: ").append(toIndentedString(externalIDs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

