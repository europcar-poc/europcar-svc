package backsvc.model;

import io.swagger.annotations.ApiModel;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Phone information. The combination of values (main,phoneType,approachType,status) used must match with a combination of values (main,typeCd,approachType,status) used by one address of the customer.
 **/
@ApiModel(description="Phone information. The combination of values (main,phoneType,approachType,status) used must match with a combination of values (main,typeCd,approachType,status) used by one address of the customer.")
public class Phone  {
  
  @ApiModelProperty(value = "")
  private String id = null;

  @ApiModelProperty(required = true, value = "")
  private String areaCd = null;

  @ApiModelProperty(required = true, value = "")
  private String number = null;

  @ApiModelProperty(value = "")
  private String extension = null;


@XmlType(name="TypeCdEnum")
@XmlEnum(String.class)
public enum TypeCdEnum {

@XmlEnumValue("H") H(String.valueOf("H")), @XmlEnumValue("P") P(String.valueOf("P"));


    private String value;

    TypeCdEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static TypeCdEnum fromValue(String v) {
        for (TypeCdEnum b : TypeCdEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "H= Home, P= Pro")
 /**
   * H= Home, P= Pro  
  **/
  private TypeCdEnum typeCd = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private String normalizedNumber = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private Integer confidenceRank = null;

  @ApiModelProperty(value = "")
  private Boolean indAuthOptin = null;

  @ApiModelProperty(required = true, value = "")
  private Boolean main = null;


@XmlType(name="PhoneTypeEnum")
@XmlEnum(String.class)
public enum PhoneTypeEnum {

@XmlEnumValue("P") P(String.valueOf("P")), @XmlEnumValue("F") F(String.valueOf("F")), @XmlEnumValue("M") M(String.valueOf("M"));


    private String value;

    PhoneTypeEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static PhoneTypeEnum fromValue(String v) {
        for (PhoneTypeEnum b : PhoneTypeEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "It's possible to define only one phone by phoneType and by customer. F= Fax, M= Mobile, P= Phone")
 /**
   * It's possible to define only one phone by phoneType and by customer. F= Fax, M= Mobile, P= Phone  
  **/
  private PhoneTypeEnum phoneType = null;


@XmlType(name="ApproachTypeEnum")
@XmlEnum(String.class)
public enum ApproachTypeEnum {

@XmlEnumValue("D") D(String.valueOf("D")), @XmlEnumValue("B") B(String.valueOf("B"));


    private String value;

    ApproachTypeEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static ApproachTypeEnum fromValue(String v) {
        for (ApproachTypeEnum b : ApproachTypeEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "D= default, B= Billing")
 /**
   * D= default, B= Billing  
  **/
  private ApproachTypeEnum approachType = null;


@XmlType(name="StatusEnum")
@XmlEnum(String.class)
public enum StatusEnum {

@XmlEnumValue("A") A(String.valueOf("A")), @XmlEnumValue("I") I(String.valueOf("I")), @XmlEnumValue("U") U(String.valueOf("U"));


    private String value;

    StatusEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static StatusEnum fromValue(String v) {
        for (StatusEnum b : StatusEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(value = "")
  private StatusEnum status = null;

  @ApiModelProperty(required = true, value = "")
  private Boolean phoneMain = null;

  @ApiModelProperty(value = "If the value is null, the value will be calculated by the API.")
 /**
   * If the value is null, the value will be calculated by the API.  
  **/
  private Boolean incorrectFlag = null;
 /**
   * Get id
   * @return id
  **/
  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Phone id(String id) {
    this.id = id;
    return this;
  }

 /**
   * Get areaCd
   * @return areaCd
  **/
  @JsonProperty("areaCd")
  @NotNull
  public String getAreaCd() {
    return areaCd;
  }

  public void setAreaCd(String areaCd) {
    this.areaCd = areaCd;
  }

  public Phone areaCd(String areaCd) {
    this.areaCd = areaCd;
    return this;
  }

 /**
   * Get number
   * @return number
  **/
  @JsonProperty("number")
  @NotNull
  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Phone number(String number) {
    this.number = number;
    return this;
  }

 /**
   * Get extension
   * @return extension
  **/
  @JsonProperty("extension")
  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public Phone extension(String extension) {
    this.extension = extension;
    return this;
  }

 /**
   * H&#x3D; Home, P&#x3D; Pro
   * @return typeCd
  **/
  @JsonProperty("typeCd")
  @NotNull
  public String getTypeCd() {
    if (typeCd == null) {
      return null;
    }
    return typeCd.value();
  }

  public void setTypeCd(TypeCdEnum typeCd) {
    this.typeCd = typeCd;
  }

  public Phone typeCd(TypeCdEnum typeCd) {
    this.typeCd = typeCd;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * @return normalizedNumber
  **/
  @JsonProperty("normalizedNumber")
  public String getNormalizedNumber() {
    return normalizedNumber;
  }

  public void setNormalizedNumber(String normalizedNumber) {
    this.normalizedNumber = normalizedNumber;
  }

  public Phone normalizedNumber(String normalizedNumber) {
    this.normalizedNumber = normalizedNumber;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * minimum: 0
   * maximum: 99
   * @return confidenceRank
  **/
  @JsonProperty("confidenceRank")
 @Min(0) @Max(99)  public Integer getConfidenceRank() {
    return confidenceRank;
  }

  public void setConfidenceRank(Integer confidenceRank) {
    this.confidenceRank = confidenceRank;
  }

  public Phone confidenceRank(Integer confidenceRank) {
    this.confidenceRank = confidenceRank;
    return this;
  }

 /**
   * Get indAuthOptin
   * @return indAuthOptin
  **/
  @JsonProperty("indAuthOptin")
  public Boolean isIndAuthOptin() {
    return indAuthOptin;
  }

  public void setIndAuthOptin(Boolean indAuthOptin) {
    this.indAuthOptin = indAuthOptin;
  }

  public Phone indAuthOptin(Boolean indAuthOptin) {
    this.indAuthOptin = indAuthOptin;
    return this;
  }

 /**
   * Get main
   * @return main
  **/
  @JsonProperty("main")
  @NotNull
  public Boolean isMain() {
    return main;
  }

  public void setMain(Boolean main) {
    this.main = main;
  }

  public Phone main(Boolean main) {
    this.main = main;
    return this;
  }

 /**
   * It&#39;s possible to define only one phone by phoneType and by customer. F&#x3D; Fax, M&#x3D; Mobile, P&#x3D; Phone
   * @return phoneType
  **/
  @JsonProperty("phoneType")
  @NotNull
  public String getPhoneType() {
    if (phoneType == null) {
      return null;
    }
    return phoneType.value();
  }

  public void setPhoneType(PhoneTypeEnum phoneType) {
    this.phoneType = phoneType;
  }

  public Phone phoneType(PhoneTypeEnum phoneType) {
    this.phoneType = phoneType;
    return this;
  }

 /**
   * D&#x3D; default, B&#x3D; Billing
   * @return approachType
  **/
  @JsonProperty("approachType")
  @NotNull
  public String getApproachType() {
    if (approachType == null) {
      return null;
    }
    return approachType.value();
  }

  public void setApproachType(ApproachTypeEnum approachType) {
    this.approachType = approachType;
  }

  public Phone approachType(ApproachTypeEnum approachType) {
    this.approachType = approachType;
    return this;
  }

 /**
   * Get status
   * @return status
  **/
  @JsonProperty("status")
  public String getStatus() {
    if (status == null) {
      return null;
    }
    return status.value();
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Phone status(StatusEnum status) {
    this.status = status;
    return this;
  }

 /**
   * Get phoneMain
   * @return phoneMain
  **/
  @JsonProperty("phoneMain")
  @NotNull
  public Boolean isPhoneMain() {
    return phoneMain;
  }

  public void setPhoneMain(Boolean phoneMain) {
    this.phoneMain = phoneMain;
  }

  public Phone phoneMain(Boolean phoneMain) {
    this.phoneMain = phoneMain;
    return this;
  }

 /**
   * If the value is null, the value will be calculated by the API.
   * @return incorrectFlag
  **/
  @JsonProperty("incorrectFlag")
  public Boolean isIncorrectFlag() {
    return incorrectFlag;
  }

  public void setIncorrectFlag(Boolean incorrectFlag) {
    this.incorrectFlag = incorrectFlag;
  }

  public Phone incorrectFlag(Boolean incorrectFlag) {
    this.incorrectFlag = incorrectFlag;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Phone {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    areaCd: ").append(toIndentedString(areaCd)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    extension: ").append(toIndentedString(extension)).append("\n");
    sb.append("    typeCd: ").append(toIndentedString(typeCd)).append("\n");
    sb.append("    normalizedNumber: ").append(toIndentedString(normalizedNumber)).append("\n");
    sb.append("    confidenceRank: ").append(toIndentedString(confidenceRank)).append("\n");
    sb.append("    indAuthOptin: ").append(toIndentedString(indAuthOptin)).append("\n");
    sb.append("    main: ").append(toIndentedString(main)).append("\n");
    sb.append("    phoneType: ").append(toIndentedString(phoneType)).append("\n");
    sb.append("    approachType: ").append(toIndentedString(approachType)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    phoneMain: ").append(toIndentedString(phoneMain)).append("\n");
    sb.append("    incorrectFlag: ").append(toIndentedString(incorrectFlag)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

