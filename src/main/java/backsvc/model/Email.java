package backsvc.model;

import io.swagger.annotations.ApiModel;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Email information. The combination of values (main,emailType,approachType) used must match with a combination of values (main,typeCd,approachType) used by one address of the customer.
 **/
@ApiModel(description="Email information. The combination of values (main,emailType,approachType) used must match with a combination of values (main,typeCd,approachType) used by one address of the customer.")
public class Email  {
  
  @ApiModelProperty(value = "")
  private String id = null;

  @ApiModelProperty(required = true, value = "")
  private String email = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private String normalizedEmail = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private Integer confidenceRank = null;

  @ApiModelProperty(value = "")
  private Boolean indAuthOptin = null;

  @ApiModelProperty(required = true, value = "")
  private Boolean main = null;


@XmlType(name="EmailTypeEnum")
@XmlEnum(String.class)
public enum EmailTypeEnum {

@XmlEnumValue("H") H(String.valueOf("H")), @XmlEnumValue("P") P(String.valueOf("P"));


    private String value;

    EmailTypeEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static EmailTypeEnum fromValue(String v) {
        for (EmailTypeEnum b : EmailTypeEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "H= home, P= Pro")
 /**
   * H= home, P= Pro  
  **/
  private EmailTypeEnum emailType = null;


@XmlType(name="ApproachTypeEnum")
@XmlEnum(String.class)
public enum ApproachTypeEnum {

@XmlEnumValue("D") D(String.valueOf("D")), @XmlEnumValue("B") B(String.valueOf("B"));


    private String value;

    ApproachTypeEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static ApproachTypeEnum fromValue(String v) {
        for (ApproachTypeEnum b : ApproachTypeEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "D= default, B= Billing")
 /**
   * D= default, B= Billing  
  **/
  private ApproachTypeEnum approachType = null;

  @ApiModelProperty(value = "If the value is null, the value will be calculated by the API.")
 /**
   * If the value is null, the value will be calculated by the API.  
  **/
  private Boolean incorrectFlag = null;
 /**
   * Get id
   * @return id
  **/
  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Email id(String id) {
    this.id = id;
    return this;
  }

 /**
   * Get email
   * @return email
  **/
  @JsonProperty("email")
  @NotNull
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Email email(String email) {
    this.email = email;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * @return normalizedEmail
  **/
  @JsonProperty("normalizedEmail")
  public String getNormalizedEmail() {
    return normalizedEmail;
  }

  public void setNormalizedEmail(String normalizedEmail) {
    this.normalizedEmail = normalizedEmail;
  }

  public Email normalizedEmail(String normalizedEmail) {
    this.normalizedEmail = normalizedEmail;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * minimum: 0
   * maximum: 99
   * @return confidenceRank
  **/
  @JsonProperty("confidenceRank")
 @Min(0) @Max(99)  public Integer getConfidenceRank() {
    return confidenceRank;
  }

  public void setConfidenceRank(Integer confidenceRank) {
    this.confidenceRank = confidenceRank;
  }

  public Email confidenceRank(Integer confidenceRank) {
    this.confidenceRank = confidenceRank;
    return this;
  }

 /**
   * Get indAuthOptin
   * @return indAuthOptin
  **/
  @JsonProperty("indAuthOptin")
  public Boolean isIndAuthOptin() {
    return indAuthOptin;
  }

  public void setIndAuthOptin(Boolean indAuthOptin) {
    this.indAuthOptin = indAuthOptin;
  }

  public Email indAuthOptin(Boolean indAuthOptin) {
    this.indAuthOptin = indAuthOptin;
    return this;
  }

 /**
   * Get main
   * @return main
  **/
  @JsonProperty("main")
  @NotNull
  public Boolean isMain() {
    return main;
  }

  public void setMain(Boolean main) {
    this.main = main;
  }

  public Email main(Boolean main) {
    this.main = main;
    return this;
  }

 /**
   * H&#x3D; home, P&#x3D; Pro
   * @return emailType
  **/
  @JsonProperty("emailType")
  @NotNull
  public String getEmailType() {
    if (emailType == null) {
      return null;
    }
    return emailType.value();
  }

  public void setEmailType(EmailTypeEnum emailType) {
    this.emailType = emailType;
  }

  public Email emailType(EmailTypeEnum emailType) {
    this.emailType = emailType;
    return this;
  }

 /**
   * D&#x3D; default, B&#x3D; Billing
   * @return approachType
  **/
  @JsonProperty("approachType")
  @NotNull
  public String getApproachType() {
    if (approachType == null) {
      return null;
    }
    return approachType.value();
  }

  public void setApproachType(ApproachTypeEnum approachType) {
    this.approachType = approachType;
  }

  public Email approachType(ApproachTypeEnum approachType) {
    this.approachType = approachType;
    return this;
  }

 /**
   * If the value is null, the value will be calculated by the API.
   * @return incorrectFlag
  **/
  @JsonProperty("incorrectFlag")
  public Boolean isIncorrectFlag() {
    return incorrectFlag;
  }

  public void setIncorrectFlag(Boolean incorrectFlag) {
    this.incorrectFlag = incorrectFlag;
  }

  public Email incorrectFlag(Boolean incorrectFlag) {
    this.incorrectFlag = incorrectFlag;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Email {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    normalizedEmail: ").append(toIndentedString(normalizedEmail)).append("\n");
    sb.append("    confidenceRank: ").append(toIndentedString(confidenceRank)).append("\n");
    sb.append("    indAuthOptin: ").append(toIndentedString(indAuthOptin)).append("\n");
    sb.append("    main: ").append(toIndentedString(main)).append("\n");
    sb.append("    emailType: ").append(toIndentedString(emailType)).append("\n");
    sb.append("    approachType: ").append(toIndentedString(approachType)).append("\n");
    sb.append("    incorrectFlag: ").append(toIndentedString(incorrectFlag)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

