package backsvc.model;

import io.swagger.annotations.ApiModel;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * External Customer ID.
 **/
@ApiModel(description="External Customer ID.")
public class ExternalID  {
  
  @ApiModelProperty(value = "It's the ID of the customer in another application. The information is coded as an URI. The format of URI is the following: [ApplicationOwner]:[ApplicationCode]:[ObjectType]:[ObjectId]  where [ApplicationOwner] is a code for the owner of the application, [ApplicationCode] is a code for the application, [ObjectType] is the code of the object type in the application and [ObjectId] is the instance ID of the customer in the application. The maximum length of a code is three char. Example in Greenway with a driver: 'EC:GWY:DVR:1234567',  Example in sfdc with a personnal account: 'EC:SFDC:PA:234567'")
 /**
   * It's the ID of the customer in another application. The information is coded as an URI. The format of URI is the following: [ApplicationOwner]:[ApplicationCode]:[ObjectType]:[ObjectId]  where [ApplicationOwner] is a code for the owner of the application, [ApplicationCode] is a code for the application, [ObjectType] is the code of the object type in the application and [ObjectId] is the instance ID of the customer in the application. The maximum length of a code is three char. Example in Greenway with a driver: 'EC:GWY:DVR:1234567',  Example in sfdc with a personnal account: 'EC:SFDC:PA:234567'  
  **/
  private String uri = null;

  @ApiModelProperty(value = "")
  private Boolean main = null;
 /**
   * It&#39;s the ID of the customer in another application. The information is coded as an URI. The format of URI is the following: [ApplicationOwner]:[ApplicationCode]:[ObjectType]:[ObjectId]  where [ApplicationOwner] is a code for the owner of the application, [ApplicationCode] is a code for the application, [ObjectType] is the code of the object type in the application and [ObjectId] is the instance ID of the customer in the application. The maximum length of a code is three char. Example in Greenway with a driver: &#39;EC:GWY:DVR:1234567&#39;,  Example in sfdc with a personnal account: &#39;EC:SFDC:PA:234567&#39;
   * @return uri
  **/
  @JsonProperty("uri")
  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public ExternalID uri(String uri) {
    this.uri = uri;
    return this;
  }

 /**
   * Get main
   * @return main
  **/
  @JsonProperty("main")
  public Boolean isMain() {
    return main;
  }

  public void setMain(Boolean main) {
    this.main = main;
  }

  public ExternalID main(Boolean main) {
    this.main = main;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExternalID {\n");
    
    sb.append("    uri: ").append(toIndentedString(uri)).append("\n");
    sb.append("    main: ").append(toIndentedString(main)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

