package backsvc.model;

import io.swagger.annotations.ApiModel;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Address information. Only one address by combination of values (main,typeCd,approachType) must be defined by customer.
 **/
@ApiModel(description="Address information. Only one address by combination of values (main,typeCd,approachType) must be defined by customer.")
public class Address  {
  
  @ApiModelProperty(value = "The technical id of the address.")
 /**
   * The technical id of the address.  
  **/
  private String id = null;

  @ApiModelProperty(required = true, value = "Address lines.")
 /**
   * Address lines.  
  **/
  private List<String> lines = new ArrayList<String>();

  @ApiModelProperty(required = true, value = "")
  private String zipCd = null;

  @ApiModelProperty(required = true, value = "")
  private String city = null;

  @ApiModelProperty(required = true, value = "")
  private String countryCd = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations.")
 /**
   * The value will be ignored in the write operations.  
  **/
  private String countryLb = null;

  @ApiModelProperty(value = "")
  private String stateCityProvDept = null;

  @ApiModelProperty(value = "")
  private Boolean indAuthOptin = null;

  @ApiModelProperty(required = true, value = "")
  private Boolean main = null;


@XmlType(name="TypeCdEnum")
@XmlEnum(String.class)
public enum TypeCdEnum {

@XmlEnumValue("H") H(String.valueOf("H")), @XmlEnumValue("P") P(String.valueOf("P"));


    private String value;

    TypeCdEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static TypeCdEnum fromValue(String v) {
        for (TypeCdEnum b : TypeCdEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "H= home, P= Pro")
 /**
   * H= home, P= Pro  
  **/
  private TypeCdEnum typeCd = null;


@XmlType(name="ApproachTypeEnum")
@XmlEnum(String.class)
public enum ApproachTypeEnum {

@XmlEnumValue("D") D(String.valueOf("D")), @XmlEnumValue("B") B(String.valueOf("B"));


    private String value;

    ApproachTypeEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static ApproachTypeEnum fromValue(String v) {
        for (ApproachTypeEnum b : ApproachTypeEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "D= default, B= Billing")
 /**
   * D= default, B= Billing  
  **/
  private ApproachTypeEnum approachType = null;


@XmlType(name="StatusEnum")
@XmlEnum(String.class)
public enum StatusEnum {

@XmlEnumValue("A") A(String.valueOf("A")), @XmlEnumValue("I") I(String.valueOf("I")), @XmlEnumValue("U") U(String.valueOf("U"));


    private String value;

    StatusEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static StatusEnum fromValue(String v) {
        for (StatusEnum b : StatusEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(value = "")
  private StatusEnum status = null;
 /**
   * The technical id of the address.
   * @return id
  **/
  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Address id(String id) {
    this.id = id;
    return this;
  }

 /**
   * Address lines.
   * @return lines
  **/
  @JsonProperty("lines")
  @NotNull
  public List<String> getLines() {
    return lines;
  }

  public void setLines(List<String> lines) {
    this.lines = lines;
  }

  public Address lines(List<String> lines) {
    this.lines = lines;
    return this;
  }

  public Address addLinesItem(String linesItem) {
    this.lines.add(linesItem);
    return this;
  }

 /**
   * Get zipCd
   * @return zipCd
  **/
  @JsonProperty("zipCd")
  @NotNull
  public String getZipCd() {
    return zipCd;
  }

  public void setZipCd(String zipCd) {
    this.zipCd = zipCd;
  }

  public Address zipCd(String zipCd) {
    this.zipCd = zipCd;
    return this;
  }

 /**
   * Get city
   * @return city
  **/
  @JsonProperty("city")
  @NotNull
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public Address city(String city) {
    this.city = city;
    return this;
  }

 /**
   * Get countryCd
   * @return countryCd
  **/
  @JsonProperty("countryCd")
  @NotNull
  public String getCountryCd() {
    return countryCd;
  }

  public void setCountryCd(String countryCd) {
    this.countryCd = countryCd;
  }

  public Address countryCd(String countryCd) {
    this.countryCd = countryCd;
    return this;
  }

 /**
   * The value will be ignored in the write operations.
   * @return countryLb
  **/
  @JsonProperty("countryLb")
  public String getCountryLb() {
    return countryLb;
  }

  public void setCountryLb(String countryLb) {
    this.countryLb = countryLb;
  }

  public Address countryLb(String countryLb) {
    this.countryLb = countryLb;
    return this;
  }

 /**
   * Get stateCityProvDept
   * @return stateCityProvDept
  **/
  @JsonProperty("stateCityProvDept")
  public String getStateCityProvDept() {
    return stateCityProvDept;
  }

  public void setStateCityProvDept(String stateCityProvDept) {
    this.stateCityProvDept = stateCityProvDept;
  }

  public Address stateCityProvDept(String stateCityProvDept) {
    this.stateCityProvDept = stateCityProvDept;
    return this;
  }

 /**
   * Get indAuthOptin
   * @return indAuthOptin
  **/
  @JsonProperty("indAuthOptin")
  public Boolean isIndAuthOptin() {
    return indAuthOptin;
  }

  public void setIndAuthOptin(Boolean indAuthOptin) {
    this.indAuthOptin = indAuthOptin;
  }

  public Address indAuthOptin(Boolean indAuthOptin) {
    this.indAuthOptin = indAuthOptin;
    return this;
  }

 /**
   * Get main
   * @return main
  **/
  @JsonProperty("main")
  @NotNull
  public Boolean isMain() {
    return main;
  }

  public void setMain(Boolean main) {
    this.main = main;
  }

  public Address main(Boolean main) {
    this.main = main;
    return this;
  }

 /**
   * H&#x3D; home, P&#x3D; Pro
   * @return typeCd
  **/
  @JsonProperty("typeCd")
  @NotNull
  public String getTypeCd() {
    if (typeCd == null) {
      return null;
    }
    return typeCd.value();
  }

  public void setTypeCd(TypeCdEnum typeCd) {
    this.typeCd = typeCd;
  }

  public Address typeCd(TypeCdEnum typeCd) {
    this.typeCd = typeCd;
    return this;
  }

 /**
   * D&#x3D; default, B&#x3D; Billing
   * @return approachType
  **/
  @JsonProperty("approachType")
  @NotNull
  public String getApproachType() {
    if (approachType == null) {
      return null;
    }
    return approachType.value();
  }

  public void setApproachType(ApproachTypeEnum approachType) {
    this.approachType = approachType;
  }

  public Address approachType(ApproachTypeEnum approachType) {
    this.approachType = approachType;
    return this;
  }

 /**
   * Get status
   * @return status
  **/
  @JsonProperty("status")
  public String getStatus() {
    if (status == null) {
      return null;
    }
    return status.value();
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Address status(StatusEnum status) {
    this.status = status;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Address {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    lines: ").append(toIndentedString(lines)).append("\n");
    sb.append("    zipCd: ").append(toIndentedString(zipCd)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    countryCd: ").append(toIndentedString(countryCd)).append("\n");
    sb.append("    countryLb: ").append(toIndentedString(countryLb)).append("\n");
    sb.append("    stateCityProvDept: ").append(toIndentedString(stateCityProvDept)).append("\n");
    sb.append("    indAuthOptin: ").append(toIndentedString(indAuthOptin)).append("\n");
    sb.append("    main: ").append(toIndentedString(main)).append("\n");
    sb.append("    typeCd: ").append(toIndentedString(typeCd)).append("\n");
    sb.append("    approachType: ").append(toIndentedString(approachType)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

