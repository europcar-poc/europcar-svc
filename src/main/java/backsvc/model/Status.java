package backsvc.model;

import io.swagger.annotations.ApiModel;
import org.joda.time.LocalDate;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Status of the Customer
 **/
@ApiModel(description="Status of the Customer")
public class Status  {
  

@XmlType(name="StatusEnum")
@XmlEnum(String.class)
public enum StatusEnum {

@XmlEnumValue("N") N(String.valueOf("N")), @XmlEnumValue("O") O(String.valueOf("O")), @XmlEnumValue("R") R(String.valueOf("R")), @XmlEnumValue("I") I(String.valueOf("I")), @XmlEnumValue("C") C(String.valueOf("C")), @XmlEnumValue("H") H(String.valueOf("H"));


    private String value;

    StatusEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static StatusEnum fromValue(String v) {
        for (StatusEnum b : StatusEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "N= New, O= One Time, R= Repeat, I= Inactive, C= Cold, H= Hot")
 /**
   * N= New, O= One Time, R= Repeat, I= Inactive, C= Cold, H= Hot  
  **/
  private StatusEnum status = null;


@XmlType(name="TypeEnum")
@XmlEnum(String.class)
public enum TypeEnum {

@XmlEnumValue("C") C(String.valueOf("C")), @XmlEnumValue("P") P(String.valueOf("P"));


    private String value;

    TypeEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static TypeEnum fromValue(String v) {
        for (TypeEnum b : TypeEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "C= Customer, P= Prospect")
 /**
   * C= Customer, P= Prospect  
  **/
  private TypeEnum type = null;

  @ApiModelProperty(value = "The value will be ignored in the write operations. Updated only by Greenway")
 /**
   * The value will be ignored in the write operations. Updated only by Greenway  
  **/
  private String driverStatus = null;

  @ApiModelProperty(value = "First date when the customer was known in the information system. Default value is the current date. Only date the currend date or a date in the past is authorized. Not updatable.")
 /**
   * First date when the customer was known in the information system. Default value is the current date. Only date the currend date or a date in the past is authorized. Not updatable.  
  **/
  private LocalDate createdAt = null;

  @ApiModelProperty(value = "")
  private LocalDate invalidatedAt = null;

  @ApiModelProperty(required = true, value = "")
  private Boolean indAuthOptin = null;

  @ApiModelProperty(value = "")
  private String watchListed = null;
 /**
   * N&#x3D; New, O&#x3D; One Time, R&#x3D; Repeat, I&#x3D; Inactive, C&#x3D; Cold, H&#x3D; Hot
   * @return status
  **/
  @JsonProperty("status")
  @NotNull
  public String getStatus() {
    if (status == null) {
      return null;
    }
    return status.value();
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Status status(StatusEnum status) {
    this.status = status;
    return this;
  }

 /**
   * C&#x3D; Customer, P&#x3D; Prospect
   * @return type
  **/
  @JsonProperty("type")
  @NotNull
  public String getType() {
    if (type == null) {
      return null;
    }
    return type.value();
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Status type(TypeEnum type) {
    this.type = type;
    return this;
  }

 /**
   * The value will be ignored in the write operations. Updated only by Greenway
   * @return driverStatus
  **/
  @JsonProperty("driverStatus")
  public String getDriverStatus() {
    return driverStatus;
  }

  public void setDriverStatus(String driverStatus) {
    this.driverStatus = driverStatus;
  }

  public Status driverStatus(String driverStatus) {
    this.driverStatus = driverStatus;
    return this;
  }

 /**
   * First date when the customer was known in the information system. Default value is the current date. Only date the currend date or a date in the past is authorized. Not updatable.
   * @return createdAt
  **/
  @JsonProperty("createdAt")
  public LocalDate getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDate createdAt) {
    this.createdAt = createdAt;
  }

  public Status createdAt(LocalDate createdAt) {
    this.createdAt = createdAt;
    return this;
  }

 /**
   * Get invalidatedAt
   * @return invalidatedAt
  **/
  @JsonProperty("invalidatedAt")
  public LocalDate getInvalidatedAt() {
    return invalidatedAt;
  }

  public void setInvalidatedAt(LocalDate invalidatedAt) {
    this.invalidatedAt = invalidatedAt;
  }

  public Status invalidatedAt(LocalDate invalidatedAt) {
    this.invalidatedAt = invalidatedAt;
    return this;
  }

 /**
   * Get indAuthOptin
   * @return indAuthOptin
  **/
  @JsonProperty("indAuthOptin")
  @NotNull
  public Boolean isIndAuthOptin() {
    return indAuthOptin;
  }

  public void setIndAuthOptin(Boolean indAuthOptin) {
    this.indAuthOptin = indAuthOptin;
  }

  public Status indAuthOptin(Boolean indAuthOptin) {
    this.indAuthOptin = indAuthOptin;
    return this;
  }

 /**
   * Get watchListed
   * @return watchListed
  **/
  @JsonProperty("watchListed")
  public String getWatchListed() {
    return watchListed;
  }

  public void setWatchListed(String watchListed) {
    this.watchListed = watchListed;
  }

  public Status watchListed(String watchListed) {
    this.watchListed = watchListed;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Status {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    driverStatus: ").append(toIndentedString(driverStatus)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    invalidatedAt: ").append(toIndentedString(invalidatedAt)).append("\n");
    sb.append("    indAuthOptin: ").append(toIndentedString(indAuthOptin)).append("\n");
    sb.append("    watchListed: ").append(toIndentedString(watchListed)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

