package backsvc.api;

import backsvc.model.Customer;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import org.apache.cxf.jaxrs.ext.multipart.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import io.swagger.jaxrs.PATCH;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/")
@Api(value = "/", description = "")
public interface CustomersvcApi  {

    /**
     * View customer details
     *
     * 
     *
     */
    @GET
    @Path("/customers/{id}")
    @Produces({ "application/json" })
    @ApiOperation(value = "View customer details", tags={ "customersvc" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "success", response = Customer.class) })
    public Customer viewcustomerbyid(@PathParam("id") String id, @QueryParam("view") @NotNull String view);
}

