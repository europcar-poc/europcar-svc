package techlab.europcar;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import backsvc.model.Customer;
import backsvc.model.Identity;
import backsvc.model.Status;

@Component(value="mockRestProcessor")
public class MockRestProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {

		//mapping data		
		Customer customer = new Customer();
		customer.setId(exchange.getIn().getHeader("id",String.class));
		
		Identity identity = new Identity();
		identity.setFirstName("Jean");
		identity.setLastName("Bernard");
		
		
		Status s = new Status();
		s.setStatus(Status.StatusEnum.N);
		
		customer.setIdentity(identity);
		customer.setStatus(s);
		exchange.getIn().setBody(customer);
	}

}
