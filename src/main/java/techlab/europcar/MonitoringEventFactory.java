package techlab.europcar;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.expression.EvaluationException;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;

@Component(value="monitoringEventFactory")
public class MonitoringEventFactory {

	ObjectMapper mapper = new ObjectMapper();

	

	public MonitoringEventFactory() {
		super();
	
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		// TODO Auto-generated constructor stub
	}



	public String generateEvent(String flowInstanceID,String state,String replayQueue , Object payload) throws Exception {
		Map<String,Object> event = new HashMap<String,Object>();
		
		event.put("eventID", UUID.randomUUID().toString());
		event.put("flowInstanceID", flowInstanceID);
		event.put("timestamp",new Date());
		event.put("state", state);
		event.put("replayQueue", replayQueue);
		event.put("payload",payload);
		String jsonInString = mapper.writeValueAsString(event);
		
		return jsonInString;
	}
}
