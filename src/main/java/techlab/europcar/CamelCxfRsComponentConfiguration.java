package techlab.europcar;


import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.cxf.feature.Feature;
import org.apache.cxf.jaxrs.swagger.Swagger2Feature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class CamelCxfRsComponentConfiguration {

  private static final Logger log = LoggerFactory.getLogger(CamelCxfRsComponentConfiguration.class);
  
  @Bean
  List<Feature> cxfFeatures(ApplicationContext applicationContext) {
    List<Feature> cxfFeatures = new ArrayList<>();
    Swagger2Feature swagger2Feature = new Swagger2Feature();
    // Override some defaults.
    swagger2Feature.setPrettyPrint(true);
    swagger2Feature.setRunAsFilter(true);
    cxfFeatures.add(swagger2Feature);
    return cxfFeatures;
  }
  
  
  
  @Bean
  List<Object> cxfProviders(ApplicationContext applicationContext) {
    List<Object> cxfProviders = new ArrayList<>();
    cxfProviders.add( new JacksonJsonProvider());
    return cxfProviders;
  }
  
  
  
  
}