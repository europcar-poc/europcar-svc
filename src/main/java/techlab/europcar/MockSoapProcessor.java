package techlab.europcar;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.europcar.services.api.mkt.cust.v1_0.ObjectFactory;
import com.europcar.services.api.mkt.cust.v1_0.ViewCustomerRSType;

import backsvc.model.Customer;
import backsvc.model.Identity;
import backsvc.model.Status;

@Component(value="mockSoapProcessor")
public class MockSoapProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		ObjectFactory o = new ObjectFactory();
		
		com.europcar.services.api.dto.cor.typ.ectypes.v1_2.customer.ObjectFactory custOf = new com.europcar.services.api.dto.cor.typ.ectypes.v1_2.customer.ObjectFactory();
		com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver.ObjectFactory driverOf = new com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver.ObjectFactory();
		
		ViewCustomerRSType viewCustomerRSType = o.createViewCustomerRSType();
		viewCustomerRSType.setResponse(o.createViewCustomerRSTypeResponse());
		viewCustomerRSType.getResponse().setCustomer(custOf.createCustomerDTO());
		viewCustomerRSType.getResponse().getCustomer().setDriver(driverOf.createDriverDTO());
		viewCustomerRSType.getResponse().getCustomer().getDriver().setFirstName("Optimus");
		viewCustomerRSType.getResponse().getCustomer().getDriver().setLastName("Prime");
		
		exchange.getIn().setBody(viewCustomerRSType);
	}

}
