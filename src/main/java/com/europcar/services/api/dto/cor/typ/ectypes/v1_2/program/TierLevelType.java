
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.program;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TierLevelType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TierLevelType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="tierLevelCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="tierLevelDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TierLevelType")
public class TierLevelType {

    @XmlAttribute(name = "tierLevelCode")
    protected String tierLevelCode;
    @XmlAttribute(name = "tierLevelDescription")
    protected String tierLevelDescription;

    /**
     * Gets the value of the tierLevelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierLevelCode() {
        return tierLevelCode;
    }

    /**
     * Sets the value of the tierLevelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierLevelCode(String value) {
        this.tierLevelCode = value;
    }

    /**
     * Gets the value of the tierLevelDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierLevelDescription() {
        return tierLevelDescription;
    }

    /**
     * Sets the value of the tierLevelDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierLevelDescription(String value) {
        this.tierLevelDescription = value;
    }

}
