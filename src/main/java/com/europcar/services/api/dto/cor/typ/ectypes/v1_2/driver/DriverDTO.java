
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ApproachPointListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.OrganisationExternalReferenceType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ReadyServiceListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.TrackingType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.meanofpayment.MeanOfPaymentListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.SexType;


/**
 * <p>Java class for DriverDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="approachPoints" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ApproachPointListType" minOccurs="0"/&gt;
 *         &lt;element name="licenses" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}LicenseListType" minOccurs="0"/&gt;
 *         &lt;element name="legalIds" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}LegalIdListType" minOccurs="0"/&gt;
 *         &lt;element name="meanOfPayments" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/meanofpayment}MeanOfPaymentListType" minOccurs="0"/&gt;
 *         &lt;element name="readyServices" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ReadyServiceListType" minOccurs="0"/&gt;
 *         &lt;element name="branding" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}BrandingType" minOccurs="0"/&gt;
 *         &lt;element name="customerExternalReferenceList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customerExternalReference" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}OrganisationExternalReferenceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="preferences" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="vehicleCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="language" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="isoLanguage" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="stationIdCo" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="stationIdCi" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="vehicleClassCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="selfServiceIndicator" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="tracking" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}TrackingType" minOccurs="0"/&gt;
 *         &lt;element name="services" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriversServicType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="driverId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="sex" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}SexType" /&gt;
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="birthDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="birthCity" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="birthCountry" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="countryOfResidence" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="age" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="contractId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="driverStatus" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriverStatusType" /&gt;
 *       &lt;attribute name="marketingStatus" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}MarketingStatusType" /&gt;
 *       &lt;attribute name="numberOfRentals" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="duplicate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverDTO", propOrder = {
    "approachPoints",
    "licenses",
    "legalIds",
    "meanOfPayments",
    "readyServices",
    "branding",
    "customerExternalReferenceList",
    "preferences",
    "tracking",
    "services"
})
@XmlSeeAlso({
    AdditionalDriverDTO.class
})
public class DriverDTO {

    protected ApproachPointListType approachPoints;
    protected LicenseListType licenses;
    protected LegalIdListType legalIds;
    protected MeanOfPaymentListType meanOfPayments;
    protected ReadyServiceListType readyServices;
    protected BrandingType branding;
    protected DriverDTO.CustomerExternalReferenceList customerExternalReferenceList;
    protected DriverDTO.Preferences preferences;
    protected TrackingType tracking;
    protected DriversServicType services;
    @XmlAttribute(name = "driverId")
    protected String driverId;
    @XmlAttribute(name = "firstName")
    protected String firstName;
    @XmlAttribute(name = "lastName")
    protected String lastName;
    @XmlAttribute(name = "sex")
    protected SexType sex;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "birthDate")
    protected String birthDate;
    @XmlAttribute(name = "birthCity")
    protected String birthCity;
    @XmlAttribute(name = "birthCountry")
    protected String birthCountry;
    @XmlAttribute(name = "countryOfResidence")
    protected String countryOfResidence;
    @XmlAttribute(name = "age")
    protected BigInteger age;
    @XmlAttribute(name = "contractId")
    protected String contractId;
    @XmlAttribute(name = "driverStatus")
    protected DriverStatusType driverStatus;
    @XmlAttribute(name = "marketingStatus")
    protected MarketingStatusType marketingStatus;
    @XmlAttribute(name = "numberOfRentals")
    protected BigInteger numberOfRentals;
    @XmlAttribute(name = "duplicate")
    protected String duplicate;

    /**
     * Gets the value of the approachPoints property.
     * 
     * @return
     *     possible object is
     *     {@link ApproachPointListType }
     *     
     */
    public ApproachPointListType getApproachPoints() {
        return approachPoints;
    }

    /**
     * Sets the value of the approachPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApproachPointListType }
     *     
     */
    public void setApproachPoints(ApproachPointListType value) {
        this.approachPoints = value;
    }

    /**
     * Gets the value of the licenses property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseListType }
     *     
     */
    public LicenseListType getLicenses() {
        return licenses;
    }

    /**
     * Sets the value of the licenses property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseListType }
     *     
     */
    public void setLicenses(LicenseListType value) {
        this.licenses = value;
    }

    /**
     * Gets the value of the legalIds property.
     * 
     * @return
     *     possible object is
     *     {@link LegalIdListType }
     *     
     */
    public LegalIdListType getLegalIds() {
        return legalIds;
    }

    /**
     * Sets the value of the legalIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link LegalIdListType }
     *     
     */
    public void setLegalIds(LegalIdListType value) {
        this.legalIds = value;
    }

    /**
     * Gets the value of the meanOfPayments property.
     * 
     * @return
     *     possible object is
     *     {@link MeanOfPaymentListType }
     *     
     */
    public MeanOfPaymentListType getMeanOfPayments() {
        return meanOfPayments;
    }

    /**
     * Sets the value of the meanOfPayments property.
     * 
     * @param value
     *     allowed object is
     *     {@link MeanOfPaymentListType }
     *     
     */
    public void setMeanOfPayments(MeanOfPaymentListType value) {
        this.meanOfPayments = value;
    }

    /**
     * Gets the value of the readyServices property.
     * 
     * @return
     *     possible object is
     *     {@link ReadyServiceListType }
     *     
     */
    public ReadyServiceListType getReadyServices() {
        return readyServices;
    }

    /**
     * Sets the value of the readyServices property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadyServiceListType }
     *     
     */
    public void setReadyServices(ReadyServiceListType value) {
        this.readyServices = value;
    }

    /**
     * Gets the value of the branding property.
     * 
     * @return
     *     possible object is
     *     {@link BrandingType }
     *     
     */
    public BrandingType getBranding() {
        return branding;
    }

    /**
     * Sets the value of the branding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BrandingType }
     *     
     */
    public void setBranding(BrandingType value) {
        this.branding = value;
    }

    /**
     * Gets the value of the customerExternalReferenceList property.
     * 
     * @return
     *     possible object is
     *     {@link DriverDTO.CustomerExternalReferenceList }
     *     
     */
    public DriverDTO.CustomerExternalReferenceList getCustomerExternalReferenceList() {
        return customerExternalReferenceList;
    }

    /**
     * Sets the value of the customerExternalReferenceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverDTO.CustomerExternalReferenceList }
     *     
     */
    public void setCustomerExternalReferenceList(DriverDTO.CustomerExternalReferenceList value) {
        this.customerExternalReferenceList = value;
    }

    /**
     * Gets the value of the preferences property.
     * 
     * @return
     *     possible object is
     *     {@link DriverDTO.Preferences }
     *     
     */
    public DriverDTO.Preferences getPreferences() {
        return preferences;
    }

    /**
     * Sets the value of the preferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverDTO.Preferences }
     *     
     */
    public void setPreferences(DriverDTO.Preferences value) {
        this.preferences = value;
    }

    /**
     * Gets the value of the tracking property.
     * 
     * @return
     *     possible object is
     *     {@link TrackingType }
     *     
     */
    public TrackingType getTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrackingType }
     *     
     */
    public void setTracking(TrackingType value) {
        this.tracking = value;
    }

    /**
     * Gets the value of the services property.
     * 
     * @return
     *     possible object is
     *     {@link DriversServicType }
     *     
     */
    public DriversServicType getServices() {
        return services;
    }

    /**
     * Sets the value of the services property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriversServicType }
     *     
     */
    public void setServices(DriversServicType value) {
        this.services = value;
    }

    /**
     * Gets the value of the driverId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverId() {
        return driverId;
    }

    /**
     * Sets the value of the driverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverId(String value) {
        this.driverId = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link SexType }
     *     
     */
    public SexType getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link SexType }
     *     
     */
    public void setSex(SexType value) {
        this.sex = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthDate(String value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the birthCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthCity() {
        return birthCity;
    }

    /**
     * Sets the value of the birthCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthCity(String value) {
        this.birthCity = value;
    }

    /**
     * Gets the value of the birthCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthCountry() {
        return birthCountry;
    }

    /**
     * Sets the value of the birthCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthCountry(String value) {
        this.birthCountry = value;
    }

    /**
     * Gets the value of the countryOfResidence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    /**
     * Sets the value of the countryOfResidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfResidence(String value) {
        this.countryOfResidence = value;
    }

    /**
     * Gets the value of the age property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAge(BigInteger value) {
        this.age = value;
    }

    /**
     * Gets the value of the contractId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * Sets the value of the contractId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractId(String value) {
        this.contractId = value;
    }

    /**
     * Gets the value of the driverStatus property.
     * 
     * @return
     *     possible object is
     *     {@link DriverStatusType }
     *     
     */
    public DriverStatusType getDriverStatus() {
        return driverStatus;
    }

    /**
     * Sets the value of the driverStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverStatusType }
     *     
     */
    public void setDriverStatus(DriverStatusType value) {
        this.driverStatus = value;
    }

    /**
     * Gets the value of the marketingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link MarketingStatusType }
     *     
     */
    public MarketingStatusType getMarketingStatus() {
        return marketingStatus;
    }

    /**
     * Sets the value of the marketingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketingStatusType }
     *     
     */
    public void setMarketingStatus(MarketingStatusType value) {
        this.marketingStatus = value;
    }

    /**
     * Gets the value of the numberOfRentals property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfRentals() {
        return numberOfRentals;
    }

    /**
     * Sets the value of the numberOfRentals property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfRentals(BigInteger value) {
        this.numberOfRentals = value;
    }

    /**
     * Gets the value of the duplicate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuplicate() {
        return duplicate;
    }

    /**
     * Sets the value of the duplicate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuplicate(String value) {
        this.duplicate = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="customerExternalReference" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}OrganisationExternalReferenceType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customerExternalReference"
    })
    public static class CustomerExternalReferenceList {

        protected List<OrganisationExternalReferenceType> customerExternalReference;

        /**
         * Gets the value of the customerExternalReference property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the customerExternalReference property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCustomerExternalReference().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link OrganisationExternalReferenceType }
         * 
         * 
         */
        public List<OrganisationExternalReferenceType> getCustomerExternalReference() {
            if (customerExternalReference == null) {
                customerExternalReference = new ArrayList<OrganisationExternalReferenceType>();
            }
            return this.customerExternalReference;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="vehicleCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="language" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="isoLanguage" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="stationIdCo" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="stationIdCi" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="vehicleClassCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="selfServiceIndicator" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Preferences {

        @XmlAttribute(name = "vehicleCategoryCode")
        protected String vehicleCategoryCode;
        @XmlAttribute(name = "language")
        protected String language;
        @XmlAttribute(name = "isoLanguage")
        protected String isoLanguage;
        @XmlAttribute(name = "stationIdCo")
        protected String stationIdCo;
        @XmlAttribute(name = "stationIdCi")
        protected String stationIdCi;
        @XmlAttribute(name = "vehicleClassCode")
        protected String vehicleClassCode;
        @XmlAttribute(name = "selfServiceIndicator")
        protected String selfServiceIndicator;

        /**
         * Gets the value of the vehicleCategoryCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleCategoryCode() {
            return vehicleCategoryCode;
        }

        /**
         * Sets the value of the vehicleCategoryCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleCategoryCode(String value) {
            this.vehicleCategoryCode = value;
        }

        /**
         * Gets the value of the language property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguage() {
            return language;
        }

        /**
         * Sets the value of the language property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguage(String value) {
            this.language = value;
        }

        /**
         * Gets the value of the isoLanguage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsoLanguage() {
            return isoLanguage;
        }

        /**
         * Sets the value of the isoLanguage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsoLanguage(String value) {
            this.isoLanguage = value;
        }

        /**
         * Gets the value of the stationIdCo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStationIdCo() {
            return stationIdCo;
        }

        /**
         * Sets the value of the stationIdCo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStationIdCo(String value) {
            this.stationIdCo = value;
        }

        /**
         * Gets the value of the stationIdCi property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStationIdCi() {
            return stationIdCi;
        }

        /**
         * Sets the value of the stationIdCi property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStationIdCi(String value) {
            this.stationIdCi = value;
        }

        /**
         * Gets the value of the vehicleClassCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleClassCode() {
            return vehicleClassCode;
        }

        /**
         * Sets the value of the vehicleClassCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleClassCode(String value) {
            this.vehicleClassCode = value;
        }

        /**
         * Gets the value of the selfServiceIndicator property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSelfServiceIndicator() {
            return selfServiceIndicator;
        }

        /**
         * Sets the value of the selfServiceIndicator property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSelfServiceIndicator(String value) {
            this.selfServiceIndicator = value;
        }

    }

}
