
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.LoyaltyCommunicationEnumType;


/**
 * <p>Java class for LoyaltyProgramType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoyaltyProgramType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="programCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="loyaltyProgramCommunication" use="required" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}LoyaltyCommunicationEnumType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoyaltyProgramType")
public class LoyaltyProgramType {

    @XmlAttribute(name = "programCode", required = true)
    protected String programCode;
    @XmlAttribute(name = "loyaltyProgramCommunication", required = true)
    protected LoyaltyCommunicationEnumType loyaltyProgramCommunication;

    /**
     * Gets the value of the programCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramCode() {
        return programCode;
    }

    /**
     * Sets the value of the programCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramCode(String value) {
        this.programCode = value;
    }

    /**
     * Gets the value of the loyaltyProgramCommunication property.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyCommunicationEnumType }
     *     
     */
    public LoyaltyCommunicationEnumType getLoyaltyProgramCommunication() {
        return loyaltyProgramCommunication;
    }

    /**
     * Sets the value of the loyaltyProgramCommunication property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyCommunicationEnumType }
     *     
     */
    public void setLoyaltyProgramCommunication(LoyaltyCommunicationEnumType value) {
        this.loyaltyProgramCommunication = value;
    }

}
