
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.meanofpayment;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.YNType;


/**
 * <p>Java class for VoucherPaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoucherPaymentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="voucherType" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/meanofpayment}VoucherEnumType" /&gt;
 *       &lt;attribute name="businessAccountId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="voucherId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="nilValue" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}YNType" /&gt;
 *       &lt;attribute name="fullCredit" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}YNType" /&gt;
 *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="vehicleCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="numberOfDays" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoucherPaymentType")
public class VoucherPaymentType {

    @XmlAttribute(name = "voucherType")
    protected VoucherEnumType voucherType;
    @XmlAttribute(name = "businessAccountId")
    protected BigInteger businessAccountId;
    @XmlAttribute(name = "voucherId")
    protected BigInteger voucherId;
    @XmlAttribute(name = "nilValue")
    protected YNType nilValue;
    @XmlAttribute(name = "fullCredit")
    protected YNType fullCredit;
    @XmlAttribute(name = "amount")
    protected BigDecimal amount;
    @XmlAttribute(name = "currencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "vehicleCategoryCode")
    protected String vehicleCategoryCode;
    @XmlAttribute(name = "numberOfDays")
    protected BigInteger numberOfDays;

    /**
     * Gets the value of the voucherType property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherEnumType }
     *     
     */
    public VoucherEnumType getVoucherType() {
        return voucherType;
    }

    /**
     * Sets the value of the voucherType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherEnumType }
     *     
     */
    public void setVoucherType(VoucherEnumType value) {
        this.voucherType = value;
    }

    /**
     * Gets the value of the businessAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBusinessAccountId() {
        return businessAccountId;
    }

    /**
     * Sets the value of the businessAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBusinessAccountId(BigInteger value) {
        this.businessAccountId = value;
    }

    /**
     * Gets the value of the voucherId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVoucherId() {
        return voucherId;
    }

    /**
     * Sets the value of the voucherId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVoucherId(BigInteger value) {
        this.voucherId = value;
    }

    /**
     * Gets the value of the nilValue property.
     * 
     * @return
     *     possible object is
     *     {@link YNType }
     *     
     */
    public YNType getNilValue() {
        return nilValue;
    }

    /**
     * Sets the value of the nilValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link YNType }
     *     
     */
    public void setNilValue(YNType value) {
        this.nilValue = value;
    }

    /**
     * Gets the value of the fullCredit property.
     * 
     * @return
     *     possible object is
     *     {@link YNType }
     *     
     */
    public YNType getFullCredit() {
        return fullCredit;
    }

    /**
     * Sets the value of the fullCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link YNType }
     *     
     */
    public void setFullCredit(YNType value) {
        this.fullCredit = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the vehicleCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleCategoryCode() {
        return vehicleCategoryCode;
    }

    /**
     * Sets the value of the vehicleCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleCategoryCode(String value) {
        this.vehicleCategoryCode = value;
    }

    /**
     * Gets the value of the numberOfDays property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfDays() {
        return numberOfDays;
    }

    /**
     * Sets the value of the numberOfDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfDays(BigInteger value) {
        this.numberOfDays = value;
    }

}
