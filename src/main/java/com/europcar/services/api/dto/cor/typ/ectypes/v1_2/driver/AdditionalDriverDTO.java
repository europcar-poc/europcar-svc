
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdditionalDriverDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdditionalDriverDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriverDTO"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="additionalDriverNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="youngDriverNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalDriverDTO")
public class AdditionalDriverDTO
    extends DriverDTO
{

    @XmlAttribute(name = "additionalDriverNumber")
    protected BigInteger additionalDriverNumber;
    @XmlAttribute(name = "youngDriverNumber")
    protected BigInteger youngDriverNumber;

    /**
     * Gets the value of the additionalDriverNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAdditionalDriverNumber() {
        return additionalDriverNumber;
    }

    /**
     * Sets the value of the additionalDriverNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAdditionalDriverNumber(BigInteger value) {
        this.additionalDriverNumber = value;
    }

    /**
     * Gets the value of the youngDriverNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getYoungDriverNumber() {
        return youngDriverNumber;
    }

    /**
     * Sets the value of the youngDriverNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setYoungDriverNumber(BigInteger value) {
        this.youngDriverNumber = value;
    }

}
