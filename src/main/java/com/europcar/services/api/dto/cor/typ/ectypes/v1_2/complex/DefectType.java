
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for defectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="defectType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="defectTypeId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="defectEstimationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="defectEstimatePrice" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "defectType")
public class DefectType {

    @XmlAttribute(name = "defectTypeId")
    protected String defectTypeId;
    @XmlAttribute(name = "defectEstimationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar defectEstimationDate;
    @XmlAttribute(name = "defectEstimatePrice")
    protected BigInteger defectEstimatePrice;

    /**
     * Gets the value of the defectTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefectTypeId() {
        return defectTypeId;
    }

    /**
     * Sets the value of the defectTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefectTypeId(String value) {
        this.defectTypeId = value;
    }

    /**
     * Gets the value of the defectEstimationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDefectEstimationDate() {
        return defectEstimationDate;
    }

    /**
     * Sets the value of the defectEstimationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDefectEstimationDate(XMLGregorianCalendar value) {
        this.defectEstimationDate = value;
    }

    /**
     * Gets the value of the defectEstimatePrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDefectEstimatePrice() {
        return defectEstimatePrice;
    }

    /**
     * Sets the value of the defectEstimatePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDefectEstimatePrice(BigInteger value) {
        this.defectEstimatePrice = value;
    }

}
