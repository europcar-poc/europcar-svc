
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoyaltyCommunicationEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LoyaltyCommunicationEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="M"/&gt;
 *     &lt;enumeration value="E"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LoyaltyCommunicationEnumType")
@XmlEnum
public enum LoyaltyCommunicationEnumType {

    M,
    E;

    public String value() {
        return name();
    }

    public static LoyaltyCommunicationEnumType fromValue(String v) {
        return valueOf(v);
    }

}
