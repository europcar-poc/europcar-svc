
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReadyServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReadyServiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="serviceLevel" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ServiceLevelType" minOccurs="0"/&gt;
 *         &lt;element name="serviceLevelInsurances" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ServiceLevelInsurancesType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReadyServiceType", propOrder = {
    "serviceLevel",
    "serviceLevelInsurances"
})
public class ReadyServiceType {

    protected ServiceLevelType serviceLevel;
    @XmlElement(required = true)
    protected ServiceLevelInsurancesType serviceLevelInsurances;

    /**
     * Gets the value of the serviceLevel property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceLevelType }
     *     
     */
    public ServiceLevelType getServiceLevel() {
        return serviceLevel;
    }

    /**
     * Sets the value of the serviceLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceLevelType }
     *     
     */
    public void setServiceLevel(ServiceLevelType value) {
        this.serviceLevel = value;
    }

    /**
     * Gets the value of the serviceLevelInsurances property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceLevelInsurancesType }
     *     
     */
    public ServiceLevelInsurancesType getServiceLevelInsurances() {
        return serviceLevelInsurances;
    }

    /**
     * Sets the value of the serviceLevelInsurances property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceLevelInsurancesType }
     *     
     */
    public void setServiceLevelInsurances(ServiceLevelInsurancesType value) {
        this.serviceLevelInsurances = value;
    }

}
