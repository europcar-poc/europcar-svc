
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargeCardType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeCardType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="chargeSequence" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="businessAccountId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ownershipType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="meanOfPaymentCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="cashOrChargeFlag" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeCardType")
public class ChargeCardType {

    @XmlAttribute(name = "chargeSequence")
    protected BigInteger chargeSequence;
    @XmlAttribute(name = "businessAccountId")
    protected String businessAccountId;
    @XmlAttribute(name = "ownershipType")
    protected String ownershipType;
    @XmlAttribute(name = "meanOfPaymentCode")
    protected String meanOfPaymentCode;
    @XmlAttribute(name = "cashOrChargeFlag")
    protected String cashOrChargeFlag;

    /**
     * Gets the value of the chargeSequence property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getChargeSequence() {
        return chargeSequence;
    }

    /**
     * Sets the value of the chargeSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setChargeSequence(BigInteger value) {
        this.chargeSequence = value;
    }

    /**
     * Gets the value of the businessAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessAccountId() {
        return businessAccountId;
    }

    /**
     * Sets the value of the businessAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessAccountId(String value) {
        this.businessAccountId = value;
    }

    /**
     * Gets the value of the ownershipType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnershipType() {
        return ownershipType;
    }

    /**
     * Sets the value of the ownershipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnershipType(String value) {
        this.ownershipType = value;
    }

    /**
     * Gets the value of the meanOfPaymentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeanOfPaymentCode() {
        return meanOfPaymentCode;
    }

    /**
     * Sets the value of the meanOfPaymentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeanOfPaymentCode(String value) {
        this.meanOfPaymentCode = value;
    }

    /**
     * Gets the value of the cashOrChargeFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashOrChargeFlag() {
        return cashOrChargeFlag;
    }

    /**
     * Sets the value of the cashOrChargeFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashOrChargeFlag(String value) {
        this.cashOrChargeFlag = value;
    }

}
