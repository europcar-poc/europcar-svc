
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseContextType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseContextType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="localisation" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}LocalisationType" minOccurs="0"/&gt;
 *         &lt;element name="IT_SystemSource" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}IT_SystemType"/&gt;
 *         &lt;element name="channel" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ChannelType"/&gt;
 *         &lt;element name="subChannel" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ChannelType" minOccurs="0"/&gt;
 *         &lt;element name="bookingChannel" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}BookingChannelType" minOccurs="0"/&gt;
 *         &lt;element name="brand" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}BrandType" minOccurs="0"/&gt;
 *         &lt;element name="serviceVersion" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}VersionType" minOccurs="0"/&gt;
 *         &lt;element name="requestSender" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}RequestSenderType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseContextType", propOrder = {
    "localisation",
    "itSystemSource",
    "channel",
    "subChannel",
    "bookingChannel",
    "brand",
    "serviceVersion",
    "requestSender"
})
public class ResponseContextType {

    protected LocalisationType localisation;
    @XmlElement(name = "IT_SystemSource", required = true)
    protected ITSystemType itSystemSource;
    @XmlElement(required = true)
    protected ChannelType channel;
    protected ChannelType subChannel;
    protected BookingChannelType bookingChannel;
    protected BrandType brand;
    protected VersionType serviceVersion;
    protected RequestSenderType requestSender;

    /**
     * Gets the value of the localisation property.
     * 
     * @return
     *     possible object is
     *     {@link LocalisationType }
     *     
     */
    public LocalisationType getLocalisation() {
        return localisation;
    }

    /**
     * Sets the value of the localisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalisationType }
     *     
     */
    public void setLocalisation(LocalisationType value) {
        this.localisation = value;
    }

    /**
     * Gets the value of the itSystemSource property.
     * 
     * @return
     *     possible object is
     *     {@link ITSystemType }
     *     
     */
    public ITSystemType getITSystemSource() {
        return itSystemSource;
    }

    /**
     * Sets the value of the itSystemSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITSystemType }
     *     
     */
    public void setITSystemSource(ITSystemType value) {
        this.itSystemSource = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelType }
     *     
     */
    public ChannelType getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelType }
     *     
     */
    public void setChannel(ChannelType value) {
        this.channel = value;
    }

    /**
     * Gets the value of the subChannel property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelType }
     *     
     */
    public ChannelType getSubChannel() {
        return subChannel;
    }

    /**
     * Sets the value of the subChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelType }
     *     
     */
    public void setSubChannel(ChannelType value) {
        this.subChannel = value;
    }

    /**
     * Gets the value of the bookingChannel property.
     * 
     * @return
     *     possible object is
     *     {@link BookingChannelType }
     *     
     */
    public BookingChannelType getBookingChannel() {
        return bookingChannel;
    }

    /**
     * Sets the value of the bookingChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingChannelType }
     *     
     */
    public void setBookingChannel(BookingChannelType value) {
        this.bookingChannel = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link BrandType }
     *     
     */
    public BrandType getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link BrandType }
     *     
     */
    public void setBrand(BrandType value) {
        this.brand = value;
    }

    /**
     * Gets the value of the serviceVersion property.
     * 
     * @return
     *     possible object is
     *     {@link VersionType }
     *     
     */
    public VersionType getServiceVersion() {
        return serviceVersion;
    }

    /**
     * Sets the value of the serviceVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionType }
     *     
     */
    public void setServiceVersion(VersionType value) {
        this.serviceVersion = value;
    }

    /**
     * Gets the value of the requestSender property.
     * 
     * @return
     *     possible object is
     *     {@link RequestSenderType }
     *     
     */
    public RequestSenderType getRequestSender() {
        return requestSender;
    }

    /**
     * Sets the value of the requestSender property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestSenderType }
     *     
     */
    public void setRequestSender(RequestSenderType value) {
        this.requestSender = value;
    }

}
