
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.meanofpayment;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BHPOPaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BHPOPaymentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="BHPO_Number" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessAccountId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BHPOPaymentType")
public class BHPOPaymentType {

    @XmlAttribute(name = "BHPO_Number")
    protected String bhpoNumber;
    @XmlAttribute(name = "businessAccountId")
    protected BigInteger businessAccountId;

    /**
     * Gets the value of the bhpoNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBHPONumber() {
        return bhpoNumber;
    }

    /**
     * Sets the value of the bhpoNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBHPONumber(String value) {
        this.bhpoNumber = value;
    }

    /**
     * Gets the value of the businessAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBusinessAccountId() {
        return businessAccountId;
    }

    /**
     * Sets the value of the businessAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBusinessAccountId(BigInteger value) {
        this.businessAccountId = value;
    }

}
