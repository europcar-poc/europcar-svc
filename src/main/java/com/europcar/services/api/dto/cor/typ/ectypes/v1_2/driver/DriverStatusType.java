
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DriverStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DriverStatusType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="A"/&gt;
 *     &lt;enumeration value="C"/&gt;
 *     &lt;enumeration value="NA"/&gt;
 *     &lt;enumeration value="NB"/&gt;
 *     &lt;enumeration value="NM"/&gt;
 *     &lt;enumeration value="NP"/&gt;
 *     &lt;enumeration value="NT"/&gt;
 *     &lt;enumeration value="NX"/&gt;
 *     &lt;enumeration value="NC"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DriverStatusType")
@XmlEnum
public enum DriverStatusType {

    A,
    C,
    NA,
    NB,
    NM,
    NP,
    NT,
    NX,
    NC;

    public String value() {
        return name();
    }

    public static DriverStatusType fromValue(String v) {
        return valueOf(v);
    }

}
