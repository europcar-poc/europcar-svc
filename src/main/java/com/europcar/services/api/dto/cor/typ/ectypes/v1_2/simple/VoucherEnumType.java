
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VoucherEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VoucherEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ETO"/&gt;
 *     &lt;enumeration value="ETV"/&gt;
 *     &lt;enumeration value="EOTTO"/&gt;
 *     &lt;enumeration value="EXO"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VoucherEnumType")
@XmlEnum
public enum VoucherEnumType {

    ETO,
    ETV,
    EOTTO,
    EXO;

    public String value() {
        return name();
    }

    public static VoucherEnumType fromValue(String v) {
        return valueOf(v);
    }

}
