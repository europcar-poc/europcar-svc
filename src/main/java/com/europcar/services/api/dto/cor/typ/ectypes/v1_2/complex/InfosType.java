
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for infosType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="infosType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="info1" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="info2" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="info3" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="info4" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "infosType")
public class InfosType {

    @XmlAttribute(name = "info1")
    protected String info1;
    @XmlAttribute(name = "info2")
    protected String info2;
    @XmlAttribute(name = "info3")
    protected String info3;
    @XmlAttribute(name = "info4")
    protected String info4;

    /**
     * Gets the value of the info1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo1() {
        return info1;
    }

    /**
     * Sets the value of the info1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo1(String value) {
        this.info1 = value;
    }

    /**
     * Gets the value of the info2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo2() {
        return info2;
    }

    /**
     * Sets the value of the info2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo2(String value) {
        this.info2 = value;
    }

    /**
     * Gets the value of the info3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo3() {
        return info3;
    }

    /**
     * Sets the value of the info3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo3(String value) {
        this.info3 = value;
    }

    /**
     * Gets the value of the info4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo4() {
        return info4;
    }

    /**
     * Sets the value of the info4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo4(String value) {
        this.info4 = value;
    }

}
