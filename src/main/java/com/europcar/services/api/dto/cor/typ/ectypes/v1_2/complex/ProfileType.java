
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProfileType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="right" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="componentId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="componentLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="rightParameter" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="applicationId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="applicationLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="profileId" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="profileLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileType", propOrder = {
    "right"
})
public class ProfileType {

    @XmlElement(required = true)
    protected List<ProfileType.Right> right;
    @XmlAttribute(name = "applicationId", required = true)
    protected String applicationId;
    @XmlAttribute(name = "applicationLabel", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String applicationLabel;
    @XmlAttribute(name = "profileId", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String profileId;
    @XmlAttribute(name = "profileLabel", required = true)
    protected String profileLabel;

    /**
     * Gets the value of the right property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the right property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRight().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProfileType.Right }
     * 
     * 
     */
    public List<ProfileType.Right> getRight() {
        if (right == null) {
            right = new ArrayList<ProfileType.Right>();
        }
        return this.right;
    }

    /**
     * Gets the value of the applicationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Sets the value of the applicationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationId(String value) {
        this.applicationId = value;
    }

    /**
     * Gets the value of the applicationLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationLabel() {
        return applicationLabel;
    }

    /**
     * Sets the value of the applicationLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationLabel(String value) {
        this.applicationLabel = value;
    }

    /**
     * Gets the value of the profileId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileId() {
        return profileId;
    }

    /**
     * Sets the value of the profileId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileId(String value) {
        this.profileId = value;
    }

    /**
     * Gets the value of the profileLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileLabel() {
        return profileLabel;
    }

    /**
     * Sets the value of the profileLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileLabel(String value) {
        this.profileLabel = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="componentId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="componentLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="rightParameter" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Right {

        @XmlAttribute(name = "componentId", required = true)
        protected String componentId;
        @XmlAttribute(name = "componentLabel", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String componentLabel;
        @XmlAttribute(name = "rightParameter", required = true)
        protected String rightParameter;

        /**
         * Gets the value of the componentId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComponentId() {
            return componentId;
        }

        /**
         * Sets the value of the componentId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComponentId(String value) {
            this.componentId = value;
        }

        /**
         * Gets the value of the componentLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComponentLabel() {
            return componentLabel;
        }

        /**
         * Sets the value of the componentLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComponentLabel(String value) {
            this.componentLabel = value;
        }

        /**
         * Gets the value of the rightParameter property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRightParameter() {
            return rightParameter;
        }

        /**
         * Sets the value of the rightParameter property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRightParameter(String value) {
            this.rightParameter = value;
        }

    }

}
