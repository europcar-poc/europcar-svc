
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BankCardPaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankCardPaymentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentCardType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="MOPType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="MOPCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankCardPaymentType")
public class BankCardPaymentType
    extends PaymentCardType
{

    @XmlAttribute(name = "sequenceNumber")
    protected BigInteger sequenceNumber;
    @XmlAttribute(name = "MOPType")
    protected String mopType;
    @XmlAttribute(name = "MOPCode")
    protected String mopCode;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNumber(BigInteger value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the mopType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMOPType() {
        return mopType;
    }

    /**
     * Sets the value of the mopType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMOPType(String value) {
        this.mopType = value;
    }

    /**
     * Gets the value of the mopCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMOPCode() {
        return mopCode;
    }

    /**
     * Sets the value of the mopCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMOPCode(String value) {
        this.mopCode = value;
    }

}
