
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GwyTimeZoneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GwyTimeZoneType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="javaSimpleTimeZoneObject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="posixFormat" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GwyTimeZoneType", propOrder = {
    "javaSimpleTimeZoneObject"
})
public class GwyTimeZoneType {

    protected String javaSimpleTimeZoneObject;
    @XmlAttribute(name = "posixFormat")
    protected String posixFormat;

    /**
     * Gets the value of the javaSimpleTimeZoneObject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJavaSimpleTimeZoneObject() {
        return javaSimpleTimeZoneObject;
    }

    /**
     * Sets the value of the javaSimpleTimeZoneObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJavaSimpleTimeZoneObject(String value) {
        this.javaSimpleTimeZoneObject = value;
    }

    /**
     * Gets the value of the posixFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosixFormat() {
        return posixFormat;
    }

    /**
     * Sets the value of the posixFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosixFormat(String value) {
        this.posixFormat = value;
    }

}
