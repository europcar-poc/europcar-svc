
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.program;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ProgramType;


/**
 * <p>Java class for ProgramDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProgramDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="program" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ProgramType" minOccurs="0"/&gt;
 *         &lt;element name="partnershipAnTierLevelList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/program}PartnershipAndTierLevelListType" minOccurs="0"/&gt;
 *         &lt;element name="byBrandSetupList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/program}ByBrandSetupListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProgramDTO", propOrder = {
    "program",
    "partnershipAnTierLevelList",
    "byBrandSetupList"
})
public class ProgramDTO {

    protected ProgramType program;
    protected PartnershipAndTierLevelListType partnershipAnTierLevelList;
    protected ByBrandSetupListType byBrandSetupList;

    /**
     * Gets the value of the program property.
     * 
     * @return
     *     possible object is
     *     {@link ProgramType }
     *     
     */
    public ProgramType getProgram() {
        return program;
    }

    /**
     * Sets the value of the program property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProgramType }
     *     
     */
    public void setProgram(ProgramType value) {
        this.program = value;
    }

    /**
     * Gets the value of the partnershipAnTierLevelList property.
     * 
     * @return
     *     possible object is
     *     {@link PartnershipAndTierLevelListType }
     *     
     */
    public PartnershipAndTierLevelListType getPartnershipAnTierLevelList() {
        return partnershipAnTierLevelList;
    }

    /**
     * Sets the value of the partnershipAnTierLevelList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnershipAndTierLevelListType }
     *     
     */
    public void setPartnershipAnTierLevelList(PartnershipAndTierLevelListType value) {
        this.partnershipAnTierLevelList = value;
    }

    /**
     * Gets the value of the byBrandSetupList property.
     * 
     * @return
     *     possible object is
     *     {@link ByBrandSetupListType }
     *     
     */
    public ByBrandSetupListType getByBrandSetupList() {
        return byBrandSetupList;
    }

    /**
     * Sets the value of the byBrandSetupList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ByBrandSetupListType }
     *     
     */
    public void setByBrandSetupList(ByBrandSetupListType value) {
        this.byBrandSetupList = value;
    }

}
