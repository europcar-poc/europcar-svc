
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.busacct;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.AddressType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.FBusinessAccountBilling;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.FOrganisationRoleID;


/**
 * <p>Java class for BusinessAccountDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessAccountDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parentOrganisationAddressList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="address" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="chargeTypes" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/busAcct}ChargeTypeListType" minOccurs="0"/&gt;
 *         &lt;element name="organisationBookOF" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}F_OrganisationRoleID" minOccurs="0"/&gt;
 *         &lt;element name="organisationHolder" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}F_OrganisationRoleID" minOccurs="0"/&gt;
 *         &lt;element name="billing" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}F_BusinessAccountBilling" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="businessAccountId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessAccountStatus" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessAccountLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessAccountName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessAccountType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessAccountBrand" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="organisationID" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessAccountDTO", propOrder = {
    "parentOrganisationAddressList",
    "chargeTypes",
    "organisationBookOF",
    "organisationHolder",
    "billing"
})
public class BusinessAccountDTO {

    protected BusinessAccountDTO.ParentOrganisationAddressList parentOrganisationAddressList;
    protected ChargeTypeListType chargeTypes;
    protected FOrganisationRoleID organisationBookOF;
    protected FOrganisationRoleID organisationHolder;
    protected FBusinessAccountBilling billing;
    @XmlAttribute(name = "businessAccountId")
    protected String businessAccountId;
    @XmlAttribute(name = "businessAccountStatus")
    protected String businessAccountStatus;
    @XmlAttribute(name = "businessAccountLabel")
    protected String businessAccountLabel;
    @XmlAttribute(name = "businessAccountName")
    protected String businessAccountName;
    @XmlAttribute(name = "businessAccountType")
    protected String businessAccountType;
    @XmlAttribute(name = "businessAccountBrand")
    protected String businessAccountBrand;
    @XmlAttribute(name = "organisationID")
    protected Integer organisationID;

    /**
     * Gets the value of the parentOrganisationAddressList property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessAccountDTO.ParentOrganisationAddressList }
     *     
     */
    public BusinessAccountDTO.ParentOrganisationAddressList getParentOrganisationAddressList() {
        return parentOrganisationAddressList;
    }

    /**
     * Sets the value of the parentOrganisationAddressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessAccountDTO.ParentOrganisationAddressList }
     *     
     */
    public void setParentOrganisationAddressList(BusinessAccountDTO.ParentOrganisationAddressList value) {
        this.parentOrganisationAddressList = value;
    }

    /**
     * Gets the value of the chargeTypes property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeTypeListType }
     *     
     */
    public ChargeTypeListType getChargeTypes() {
        return chargeTypes;
    }

    /**
     * Sets the value of the chargeTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeTypeListType }
     *     
     */
    public void setChargeTypes(ChargeTypeListType value) {
        this.chargeTypes = value;
    }

    /**
     * Gets the value of the organisationBookOF property.
     * 
     * @return
     *     possible object is
     *     {@link FOrganisationRoleID }
     *     
     */
    public FOrganisationRoleID getOrganisationBookOF() {
        return organisationBookOF;
    }

    /**
     * Sets the value of the organisationBookOF property.
     * 
     * @param value
     *     allowed object is
     *     {@link FOrganisationRoleID }
     *     
     */
    public void setOrganisationBookOF(FOrganisationRoleID value) {
        this.organisationBookOF = value;
    }

    /**
     * Gets the value of the organisationHolder property.
     * 
     * @return
     *     possible object is
     *     {@link FOrganisationRoleID }
     *     
     */
    public FOrganisationRoleID getOrganisationHolder() {
        return organisationHolder;
    }

    /**
     * Sets the value of the organisationHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link FOrganisationRoleID }
     *     
     */
    public void setOrganisationHolder(FOrganisationRoleID value) {
        this.organisationHolder = value;
    }

    /**
     * Gets the value of the billing property.
     * 
     * @return
     *     possible object is
     *     {@link FBusinessAccountBilling }
     *     
     */
    public FBusinessAccountBilling getBilling() {
        return billing;
    }

    /**
     * Sets the value of the billing property.
     * 
     * @param value
     *     allowed object is
     *     {@link FBusinessAccountBilling }
     *     
     */
    public void setBilling(FBusinessAccountBilling value) {
        this.billing = value;
    }

    /**
     * Gets the value of the businessAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessAccountId() {
        return businessAccountId;
    }

    /**
     * Sets the value of the businessAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessAccountId(String value) {
        this.businessAccountId = value;
    }

    /**
     * Gets the value of the businessAccountStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessAccountStatus() {
        return businessAccountStatus;
    }

    /**
     * Sets the value of the businessAccountStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessAccountStatus(String value) {
        this.businessAccountStatus = value;
    }

    /**
     * Gets the value of the businessAccountLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessAccountLabel() {
        return businessAccountLabel;
    }

    /**
     * Sets the value of the businessAccountLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessAccountLabel(String value) {
        this.businessAccountLabel = value;
    }

    /**
     * Gets the value of the businessAccountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessAccountName() {
        return businessAccountName;
    }

    /**
     * Sets the value of the businessAccountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessAccountName(String value) {
        this.businessAccountName = value;
    }

    /**
     * Gets the value of the businessAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessAccountType() {
        return businessAccountType;
    }

    /**
     * Sets the value of the businessAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessAccountType(String value) {
        this.businessAccountType = value;
    }

    /**
     * Gets the value of the businessAccountBrand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessAccountBrand() {
        return businessAccountBrand;
    }

    /**
     * Sets the value of the businessAccountBrand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessAccountBrand(String value) {
        this.businessAccountBrand = value;
    }

    /**
     * Gets the value of the organisationID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrganisationID() {
        return organisationID;
    }

    /**
     * Sets the value of the organisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrganisationID(Integer value) {
        this.organisationID = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="address" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "address"
    })
    public static class ParentOrganisationAddressList {

        @XmlElement(nillable = true)
        protected List<AddressType> address;

        /**
         * Gets the value of the address property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the address property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddress().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AddressType }
         * 
         * 
         */
        public List<AddressType> getAddress() {
            if (address == null) {
                address = new ArrayList<AddressType>();
            }
            return this.address;
        }

    }

}
