
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.program;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.europcar.services.api.dto.cor.typ.ectypes.v1_2.program package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.europcar.services.api.dto.cor.typ.ectypes.v1_2.program
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PartnershipAndTierLevelType }
     * 
     */
    public PartnershipAndTierLevelType createPartnershipAndTierLevelType() {
        return new PartnershipAndTierLevelType();
    }

    /**
     * Create an instance of {@link PartnershipType }
     * 
     */
    public PartnershipType createPartnershipType() {
        return new PartnershipType();
    }

    /**
     * Create an instance of {@link TierLevelListType }
     * 
     */
    public TierLevelListType createTierLevelListType() {
        return new TierLevelListType();
    }

    /**
     * Create an instance of {@link TierLevelType }
     * 
     */
    public TierLevelType createTierLevelType() {
        return new TierLevelType();
    }

    /**
     * Create an instance of {@link ProgramDTO }
     * 
     */
    public ProgramDTO createProgramDTO() {
        return new ProgramDTO();
    }

    /**
     * Create an instance of {@link PartnershipAndTierLevelListType }
     * 
     */
    public PartnershipAndTierLevelListType createPartnershipAndTierLevelListType() {
        return new PartnershipAndTierLevelListType();
    }

    /**
     * Create an instance of {@link ByBrandSetupListType }
     * 
     */
    public ByBrandSetupListType createByBrandSetupListType() {
        return new ByBrandSetupListType();
    }

    /**
     * Create an instance of {@link ByBrandSetupType }
     * 
     */
    public ByBrandSetupType createByBrandSetupType() {
        return new ByBrandSetupType();
    }

}
