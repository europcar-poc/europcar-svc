
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.program;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ByBrandSetupType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ByBrandSetupType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="brandCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="programStartDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="programEndDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="programTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="alternativeProgramType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ByBrandSetupType")
public class ByBrandSetupType {

    @XmlAttribute(name = "brandCode")
    protected String brandCode;
    @XmlAttribute(name = "programStartDate")
    protected String programStartDate;
    @XmlAttribute(name = "programEndDate")
    protected String programEndDate;
    @XmlAttribute(name = "programTypeCode")
    protected String programTypeCode;
    @XmlAttribute(name = "alternativeProgramType")
    protected String alternativeProgramType;

    /**
     * Gets the value of the brandCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandCode() {
        return brandCode;
    }

    /**
     * Sets the value of the brandCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandCode(String value) {
        this.brandCode = value;
    }

    /**
     * Gets the value of the programStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramStartDate() {
        return programStartDate;
    }

    /**
     * Sets the value of the programStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramStartDate(String value) {
        this.programStartDate = value;
    }

    /**
     * Gets the value of the programEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramEndDate() {
        return programEndDate;
    }

    /**
     * Sets the value of the programEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramEndDate(String value) {
        this.programEndDate = value;
    }

    /**
     * Gets the value of the programTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramTypeCode() {
        return programTypeCode;
    }

    /**
     * Sets the value of the programTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramTypeCode(String value) {
        this.programTypeCode = value;
    }

    /**
     * Gets the value of the alternativeProgramType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternativeProgramType() {
        return alternativeProgramType;
    }

    /**
     * Sets the value of the alternativeProgramType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternativeProgramType(String value) {
        this.alternativeProgramType = value;
    }

}
