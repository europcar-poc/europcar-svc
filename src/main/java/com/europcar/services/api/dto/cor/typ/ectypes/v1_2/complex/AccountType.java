
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestedCardCategory" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="counter" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}CounterType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tracking" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}TrackingType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="enrolmentDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="enrolmentSource" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="enrolmentDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="tierLevel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="forcedTierLevelCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="vipOnInvitation" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountType", propOrder = {
    "requestedCardCategory",
    "counter",
    "tracking"
})
public class AccountType {

    protected AccountType.RequestedCardCategory requestedCardCategory;
    @XmlElement(nillable = true)
    protected List<CounterType> counter;
    protected TrackingType tracking;
    @XmlAttribute(name = "enrolmentDate")
    protected String enrolmentDate;
    @XmlAttribute(name = "enrolmentSource")
    protected String enrolmentSource;
    @XmlAttribute(name = "enrolmentDescription")
    protected String enrolmentDescription;
    @XmlAttribute(name = "tierLevel")
    protected String tierLevel;
    @XmlAttribute(name = "forcedTierLevelCode")
    protected String forcedTierLevelCode;
    @XmlAttribute(name = "vipOnInvitation")
    protected Boolean vipOnInvitation;

    /**
     * Gets the value of the requestedCardCategory property.
     * 
     * @return
     *     possible object is
     *     {@link AccountType.RequestedCardCategory }
     *     
     */
    public AccountType.RequestedCardCategory getRequestedCardCategory() {
        return requestedCardCategory;
    }

    /**
     * Sets the value of the requestedCardCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountType.RequestedCardCategory }
     *     
     */
    public void setRequestedCardCategory(AccountType.RequestedCardCategory value) {
        this.requestedCardCategory = value;
    }

    /**
     * Gets the value of the counter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the counter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCounter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CounterType }
     * 
     * 
     */
    public List<CounterType> getCounter() {
        if (counter == null) {
            counter = new ArrayList<CounterType>();
        }
        return this.counter;
    }

    /**
     * Gets the value of the tracking property.
     * 
     * @return
     *     possible object is
     *     {@link TrackingType }
     *     
     */
    public TrackingType getTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrackingType }
     *     
     */
    public void setTracking(TrackingType value) {
        this.tracking = value;
    }

    /**
     * Gets the value of the enrolmentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnrolmentDate() {
        return enrolmentDate;
    }

    /**
     * Sets the value of the enrolmentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnrolmentDate(String value) {
        this.enrolmentDate = value;
    }

    /**
     * Gets the value of the enrolmentSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnrolmentSource() {
        return enrolmentSource;
    }

    /**
     * Sets the value of the enrolmentSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnrolmentSource(String value) {
        this.enrolmentSource = value;
    }

    /**
     * Gets the value of the enrolmentDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnrolmentDescription() {
        return enrolmentDescription;
    }

    /**
     * Sets the value of the enrolmentDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnrolmentDescription(String value) {
        this.enrolmentDescription = value;
    }

    /**
     * Gets the value of the tierLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierLevel() {
        return tierLevel;
    }

    /**
     * Sets the value of the tierLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierLevel(String value) {
        this.tierLevel = value;
    }

    /**
     * Gets the value of the forcedTierLevelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForcedTierLevelCode() {
        return forcedTierLevelCode;
    }

    /**
     * Sets the value of the forcedTierLevelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForcedTierLevelCode(String value) {
        this.forcedTierLevelCode = value;
    }

    /**
     * Gets the value of the vipOnInvitation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVipOnInvitation() {
        return vipOnInvitation;
    }

    /**
     * Sets the value of the vipOnInvitation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVipOnInvitation(Boolean value) {
        this.vipOnInvitation = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RequestedCardCategory {

        @XmlAttribute(name = "code")
        protected String code;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

    }

}
