
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BusinessTargetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BusinessTargetType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="B"/&gt;
 *     &lt;enumeration value="P"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "BusinessTargetType")
@XmlEnum
public enum BusinessTargetType {

    B,
    P;

    public String value() {
        return name();
    }

    public static BusinessTargetType fromValue(String v) {
        return valueOf(v);
    }

}
