
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.DocumentType;


/**
 * <p>Java class for LegalIdType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegalIdType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DocumentType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="legalType" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}LegalEnumType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegalIdType")
public class LegalIdType
    extends DocumentType
{

    @XmlAttribute(name = "legalType")
    protected LegalEnumType legalType;

    /**
     * Gets the value of the legalType property.
     * 
     * @return
     *     possible object is
     *     {@link LegalEnumType }
     *     
     */
    public LegalEnumType getLegalType() {
        return legalType;
    }

    /**
     * Sets the value of the legalType property.
     * 
     * @param value
     *     allowed object is
     *     {@link LegalEnumType }
     *     
     */
    public void setLegalType(LegalEnumType value) {
        this.legalType = value;
    }

}
