
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.AddressEnumType;


/**
 * <p>Java class for ApproachPointListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApproachPointListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="approachPoint" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ApproachPointType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="approachMain" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}AddressEnumType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApproachPointListType", propOrder = {
    "approachPoint"
})
public class ApproachPointListType {

    @XmlElement(required = true)
    protected List<ApproachPointType> approachPoint;
    @XmlAttribute(name = "approachMain")
    protected AddressEnumType approachMain;

    /**
     * Gets the value of the approachPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the approachPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApproachPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ApproachPointType }
     * 
     * 
     */
    public List<ApproachPointType> getApproachPoint() {
        if (approachPoint == null) {
            approachPoint = new ArrayList<ApproachPointType>();
        }
        return this.approachPoint;
    }

    /**
     * Gets the value of the approachMain property.
     * 
     * @return
     *     possible object is
     *     {@link AddressEnumType }
     *     
     */
    public AddressEnumType getApproachMain() {
        return approachMain;
    }

    /**
     * Sets the value of the approachMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressEnumType }
     *     
     */
    public void setApproachMain(AddressEnumType value) {
        this.approachMain = value;
    }

}
