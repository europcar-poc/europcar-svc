
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CheckOutInType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CheckOutInType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="driverDistance" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DistanceType" minOccurs="0"/&gt;
 *         &lt;element name="driverGeoLocation" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="postal" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" minOccurs="0"/&gt;
 *                   &lt;element name="geographical" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}GeographicLocationType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="contactPhone" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="deliveryCollectionMode" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="contactName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="instructionLine1" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="instructionLine2" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="instructionLine3" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="instructionLine4" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="deliveryCollectionDatetime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="checkOutInLocation" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}CheckOutInLocationStringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="vehicleReleaseMethod" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckOutInType", propOrder = {
    "driverDistance",
    "driverGeoLocation",
    "checkOutInLocation"
})
public class CheckOutInType {

    protected DistanceType driverDistance;
    protected CheckOutInType.DriverGeoLocation driverGeoLocation;
    protected CheckOutInLocationStringType checkOutInLocation;
    @XmlAttribute(name = "vehicleReleaseMethod")
    protected String vehicleReleaseMethod;

    /**
     * Gets the value of the driverDistance property.
     * 
     * @return
     *     possible object is
     *     {@link DistanceType }
     *     
     */
    public DistanceType getDriverDistance() {
        return driverDistance;
    }

    /**
     * Sets the value of the driverDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link DistanceType }
     *     
     */
    public void setDriverDistance(DistanceType value) {
        this.driverDistance = value;
    }

    /**
     * Gets the value of the driverGeoLocation property.
     * 
     * @return
     *     possible object is
     *     {@link CheckOutInType.DriverGeoLocation }
     *     
     */
    public CheckOutInType.DriverGeoLocation getDriverGeoLocation() {
        return driverGeoLocation;
    }

    /**
     * Sets the value of the driverGeoLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckOutInType.DriverGeoLocation }
     *     
     */
    public void setDriverGeoLocation(CheckOutInType.DriverGeoLocation value) {
        this.driverGeoLocation = value;
    }

    /**
     * Gets the value of the checkOutInLocation property.
     * 
     * @return
     *     possible object is
     *     {@link CheckOutInLocationStringType }
     *     
     */
    public CheckOutInLocationStringType getCheckOutInLocation() {
        return checkOutInLocation;
    }

    /**
     * Sets the value of the checkOutInLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckOutInLocationStringType }
     *     
     */
    public void setCheckOutInLocation(CheckOutInLocationStringType value) {
        this.checkOutInLocation = value;
    }

    /**
     * Gets the value of the vehicleReleaseMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleReleaseMethod() {
        return vehicleReleaseMethod;
    }

    /**
     * Sets the value of the vehicleReleaseMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleReleaseMethod(String value) {
        this.vehicleReleaseMethod = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="postal" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" minOccurs="0"/&gt;
     *         &lt;element name="geographical" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}GeographicLocationType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="contactPhone" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="deliveryCollectionMode" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="contactName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="instructionLine1" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="instructionLine2" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="instructionLine3" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="instructionLine4" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="deliveryCollectionDatetime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "postal",
        "geographical"
    })
    public static class DriverGeoLocation {

        protected AddressType postal;
        protected GeographicLocationType geographical;
        @XmlAttribute(name = "contactPhone")
        protected String contactPhone;
        @XmlAttribute(name = "deliveryCollectionMode")
        @XmlSchemaType(name = "anySimpleType")
        protected String deliveryCollectionMode;
        @XmlAttribute(name = "contactName")
        protected String contactName;
        @XmlAttribute(name = "instructionLine1")
        protected String instructionLine1;
        @XmlAttribute(name = "instructionLine2")
        @XmlSchemaType(name = "anySimpleType")
        protected String instructionLine2;
        @XmlAttribute(name = "instructionLine3")
        @XmlSchemaType(name = "anySimpleType")
        protected String instructionLine3;
        @XmlAttribute(name = "instructionLine4")
        @XmlSchemaType(name = "anySimpleType")
        protected String instructionLine4;
        @XmlAttribute(name = "deliveryCollectionDatetime")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar deliveryCollectionDatetime;

        /**
         * Gets the value of the postal property.
         * 
         * @return
         *     possible object is
         *     {@link AddressType }
         *     
         */
        public AddressType getPostal() {
            return postal;
        }

        /**
         * Sets the value of the postal property.
         * 
         * @param value
         *     allowed object is
         *     {@link AddressType }
         *     
         */
        public void setPostal(AddressType value) {
            this.postal = value;
        }

        /**
         * Gets the value of the geographical property.
         * 
         * @return
         *     possible object is
         *     {@link GeographicLocationType }
         *     
         */
        public GeographicLocationType getGeographical() {
            return geographical;
        }

        /**
         * Sets the value of the geographical property.
         * 
         * @param value
         *     allowed object is
         *     {@link GeographicLocationType }
         *     
         */
        public void setGeographical(GeographicLocationType value) {
            this.geographical = value;
        }

        /**
         * Gets the value of the contactPhone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContactPhone() {
            return contactPhone;
        }

        /**
         * Sets the value of the contactPhone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContactPhone(String value) {
            this.contactPhone = value;
        }

        /**
         * Gets the value of the deliveryCollectionMode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeliveryCollectionMode() {
            return deliveryCollectionMode;
        }

        /**
         * Sets the value of the deliveryCollectionMode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeliveryCollectionMode(String value) {
            this.deliveryCollectionMode = value;
        }

        /**
         * Gets the value of the contactName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContactName() {
            return contactName;
        }

        /**
         * Sets the value of the contactName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContactName(String value) {
            this.contactName = value;
        }

        /**
         * Gets the value of the instructionLine1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstructionLine1() {
            return instructionLine1;
        }

        /**
         * Sets the value of the instructionLine1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstructionLine1(String value) {
            this.instructionLine1 = value;
        }

        /**
         * Gets the value of the instructionLine2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstructionLine2() {
            return instructionLine2;
        }

        /**
         * Sets the value of the instructionLine2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstructionLine2(String value) {
            this.instructionLine2 = value;
        }

        /**
         * Gets the value of the instructionLine3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstructionLine3() {
            return instructionLine3;
        }

        /**
         * Sets the value of the instructionLine3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstructionLine3(String value) {
            this.instructionLine3 = value;
        }

        /**
         * Gets the value of the instructionLine4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstructionLine4() {
            return instructionLine4;
        }

        /**
         * Sets the value of the instructionLine4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstructionLine4(String value) {
            this.instructionLine4 = value;
        }

        /**
         * Gets the value of the deliveryCollectionDatetime property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDeliveryCollectionDatetime() {
            return deliveryCollectionDatetime;
        }

        /**
         * Sets the value of the deliveryCollectionDatetime property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDeliveryCollectionDatetime(XMLGregorianCalendar value) {
            this.deliveryCollectionDatetime = value;
        }

    }

}
