
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CardCategoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CardCategoryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idCard" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}IdCardType" minOccurs="0"/&gt;
 *         &lt;element name="chargeCard" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ChargeCardType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardCategoryType", propOrder = {
    "idCard",
    "chargeCard"
})
public class CardCategoryType {

    protected IdCardType idCard;
    protected ChargeCardType chargeCard;
    @XmlAttribute(name = "code")
    protected String code;

    /**
     * Gets the value of the idCard property.
     * 
     * @return
     *     possible object is
     *     {@link IdCardType }
     *     
     */
    public IdCardType getIdCard() {
        return idCard;
    }

    /**
     * Sets the value of the idCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdCardType }
     *     
     */
    public void setIdCard(IdCardType value) {
        this.idCard = value;
    }

    /**
     * Gets the value of the chargeCard property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeCardType }
     *     
     */
    public ChargeCardType getChargeCard() {
        return chargeCard;
    }

    /**
     * Sets the value of the chargeCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeCardType }
     *     
     */
    public void setChargeCard(ChargeCardType value) {
        this.chargeCard = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

}
