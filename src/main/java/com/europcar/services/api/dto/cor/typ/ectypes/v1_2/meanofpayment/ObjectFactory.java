
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.meanofpayment;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.europcar.services.api.dto.cor.typ.ectypes.v1_2.meanofpayment package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.europcar.services.api.dto.cor.typ.ectypes.v1_2.meanofpayment
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MeanOfPaymentListType }
     * 
     */
    public MeanOfPaymentListType createMeanOfPaymentListType() {
        return new MeanOfPaymentListType();
    }

    /**
     * Create an instance of {@link MeanOfPaymentDTO }
     * 
     */
    public MeanOfPaymentDTO createMeanOfPaymentDTO() {
        return new MeanOfPaymentDTO();
    }

    /**
     * Create an instance of {@link BankCardPaymentType }
     * 
     */
    public BankCardPaymentType createBankCardPaymentType() {
        return new BankCardPaymentType();
    }

    /**
     * Create an instance of {@link PaymentCardType }
     * 
     */
    public PaymentCardType createPaymentCardType() {
        return new PaymentCardType();
    }

    /**
     * Create an instance of {@link DebitCardPaymentType }
     * 
     */
    public DebitCardPaymentType createDebitCardPaymentType() {
        return new DebitCardPaymentType();
    }

    /**
     * Create an instance of {@link ChargeCardPaymentType }
     * 
     */
    public ChargeCardPaymentType createChargeCardPaymentType() {
        return new ChargeCardPaymentType();
    }

    /**
     * Create an instance of {@link BHPOPaymentType }
     * 
     */
    public BHPOPaymentType createBHPOPaymentType() {
        return new BHPOPaymentType();
    }

    /**
     * Create an instance of {@link VoucherPaymentType }
     * 
     */
    public VoucherPaymentType createVoucherPaymentType() {
        return new VoucherPaymentType();
    }

    /**
     * Create an instance of {@link PPBAType }
     * 
     */
    public PPBAType createPPBAType() {
        return new PPBAType();
    }

    /**
     * Create an instance of {@link ChequePaymentType }
     * 
     */
    public ChequePaymentType createChequePaymentType() {
        return new ChequePaymentType();
    }

    /**
     * Create an instance of {@link GroupAndDaysType }
     * 
     */
    public GroupAndDaysType createGroupAndDaysType() {
        return new GroupAndDaysType();
    }

    /**
     * Create an instance of {@link SpecifiedValueType }
     * 
     */
    public SpecifiedValueType createSpecifiedValueType() {
        return new SpecifiedValueType();
    }

}
