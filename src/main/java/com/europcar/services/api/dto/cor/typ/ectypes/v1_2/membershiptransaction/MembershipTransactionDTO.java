
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.membershiptransaction;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.BusinessTargetType;


/**
 * <p>Java class for MembershipTransactionDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MembershipTransactionDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rentalActivity"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="rentalAgreementNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="vehicleCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="numberOfRentalDays" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="checkOutDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="checkInStation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="checkInDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="checkOutStation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="contractId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="driverId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="programCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessTarget" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}BusinessTargetType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MembershipTransactionDTO", propOrder = {
    "rentalActivity"
})
public class MembershipTransactionDTO {

    @XmlElement(required = true)
    protected MembershipTransactionDTO.RentalActivity rentalActivity;
    @XmlAttribute(name = "id")
    protected BigInteger id;
    @XmlAttribute(name = "status")
    protected String status;
    @XmlAttribute(name = "driverId")
    protected String driverId;
    @XmlAttribute(name = "memberNumber")
    protected String memberNumber;
    @XmlAttribute(name = "programCode")
    protected String programCode;
    @XmlAttribute(name = "businessTarget")
    protected BusinessTargetType businessTarget;

    /**
     * Gets the value of the rentalActivity property.
     * 
     * @return
     *     possible object is
     *     {@link MembershipTransactionDTO.RentalActivity }
     *     
     */
    public MembershipTransactionDTO.RentalActivity getRentalActivity() {
        return rentalActivity;
    }

    /**
     * Sets the value of the rentalActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link MembershipTransactionDTO.RentalActivity }
     *     
     */
    public void setRentalActivity(MembershipTransactionDTO.RentalActivity value) {
        this.rentalActivity = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the driverId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverId() {
        return driverId;
    }

    /**
     * Sets the value of the driverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverId(String value) {
        this.driverId = value;
    }

    /**
     * Gets the value of the memberNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberNumber() {
        return memberNumber;
    }

    /**
     * Sets the value of the memberNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberNumber(String value) {
        this.memberNumber = value;
    }

    /**
     * Gets the value of the programCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramCode() {
        return programCode;
    }

    /**
     * Sets the value of the programCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramCode(String value) {
        this.programCode = value;
    }

    /**
     * Gets the value of the businessTarget property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTargetType }
     *     
     */
    public BusinessTargetType getBusinessTarget() {
        return businessTarget;
    }

    /**
     * Sets the value of the businessTarget property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTargetType }
     *     
     */
    public void setBusinessTarget(BusinessTargetType value) {
        this.businessTarget = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="rentalAgreementNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="vehicleCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="numberOfRentalDays" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="checkOutDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="checkInStation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="checkInDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="checkOutStation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="contractId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RentalActivity {

        @XmlAttribute(name = "rentalAgreementNumber")
        protected BigInteger rentalAgreementNumber;
        @XmlAttribute(name = "vehicleCategoryCode")
        protected String vehicleCategoryCode;
        @XmlAttribute(name = "numberOfRentalDays")
        protected BigInteger numberOfRentalDays;
        @XmlAttribute(name = "checkOutDate")
        protected String checkOutDate;
        @XmlAttribute(name = "checkInStation")
        protected String checkInStation;
        @XmlAttribute(name = "checkInDate")
        protected String checkInDate;
        @XmlAttribute(name = "checkOutStation")
        protected String checkOutStation;
        @XmlAttribute(name = "contractId")
        protected String contractId;

        /**
         * Gets the value of the rentalAgreementNumber property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRentalAgreementNumber() {
            return rentalAgreementNumber;
        }

        /**
         * Sets the value of the rentalAgreementNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRentalAgreementNumber(BigInteger value) {
            this.rentalAgreementNumber = value;
        }

        /**
         * Gets the value of the vehicleCategoryCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleCategoryCode() {
            return vehicleCategoryCode;
        }

        /**
         * Sets the value of the vehicleCategoryCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleCategoryCode(String value) {
            this.vehicleCategoryCode = value;
        }

        /**
         * Gets the value of the numberOfRentalDays property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNumberOfRentalDays() {
            return numberOfRentalDays;
        }

        /**
         * Sets the value of the numberOfRentalDays property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNumberOfRentalDays(BigInteger value) {
            this.numberOfRentalDays = value;
        }

        /**
         * Gets the value of the checkOutDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCheckOutDate() {
            return checkOutDate;
        }

        /**
         * Sets the value of the checkOutDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCheckOutDate(String value) {
            this.checkOutDate = value;
        }

        /**
         * Gets the value of the checkInStation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCheckInStation() {
            return checkInStation;
        }

        /**
         * Sets the value of the checkInStation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCheckInStation(String value) {
            this.checkInStation = value;
        }

        /**
         * Gets the value of the checkInDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCheckInDate() {
            return checkInDate;
        }

        /**
         * Sets the value of the checkInDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCheckInDate(String value) {
            this.checkInDate = value;
        }

        /**
         * Gets the value of the checkOutStation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCheckOutStation() {
            return checkOutStation;
        }

        /**
         * Sets the value of the checkOutStation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCheckOutStation(String value) {
            this.checkOutStation = value;
        }

        /**
         * Gets the value of the contractId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractId() {
            return contractId;
        }

        /**
         * Sets the value of the contractId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractId(String value) {
            this.contractId = value;
        }

    }

}
