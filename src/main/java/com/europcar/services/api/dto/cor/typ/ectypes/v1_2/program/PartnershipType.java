
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.program;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartnershipType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartnershipType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="organisationId" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="partnershipTypeLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="multiHolderEnabled" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="roleType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="partnershipTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="partnershipStartDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="partnershipEndDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnershipType")
public class PartnershipType {

    @XmlAttribute(name = "organisationId")
    protected Integer organisationId;
    @XmlAttribute(name = "partnershipTypeLabel")
    protected String partnershipTypeLabel;
    @XmlAttribute(name = "multiHolderEnabled")
    protected String multiHolderEnabled;
    @XmlAttribute(name = "roleType")
    protected String roleType;
    @XmlAttribute(name = "partnershipTypeCode")
    protected String partnershipTypeCode;
    @XmlAttribute(name = "partnershipStartDate")
    protected String partnershipStartDate;
    @XmlAttribute(name = "partnershipEndDate")
    protected String partnershipEndDate;

    /**
     * Gets the value of the organisationId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrganisationId() {
        return organisationId;
    }

    /**
     * Sets the value of the organisationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrganisationId(Integer value) {
        this.organisationId = value;
    }

    /**
     * Gets the value of the partnershipTypeLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnershipTypeLabel() {
        return partnershipTypeLabel;
    }

    /**
     * Sets the value of the partnershipTypeLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnershipTypeLabel(String value) {
        this.partnershipTypeLabel = value;
    }

    /**
     * Gets the value of the multiHolderEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMultiHolderEnabled() {
        return multiHolderEnabled;
    }

    /**
     * Sets the value of the multiHolderEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMultiHolderEnabled(String value) {
        this.multiHolderEnabled = value;
    }

    /**
     * Gets the value of the roleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleType() {
        return roleType;
    }

    /**
     * Sets the value of the roleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleType(String value) {
        this.roleType = value;
    }

    /**
     * Gets the value of the partnershipTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnershipTypeCode() {
        return partnershipTypeCode;
    }

    /**
     * Sets the value of the partnershipTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnershipTypeCode(String value) {
        this.partnershipTypeCode = value;
    }

    /**
     * Gets the value of the partnershipStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnershipStartDate() {
        return partnershipStartDate;
    }

    /**
     * Sets the value of the partnershipStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnershipStartDate(String value) {
        this.partnershipStartDate = value;
    }

    /**
     * Gets the value of the partnershipEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnershipEndDate() {
        return partnershipEndDate;
    }

    /**
     * Sets the value of the partnershipEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnershipEndDate(String value) {
        this.partnershipEndDate = value;
    }

}
