
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.card;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.CardCategoryType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.CardType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.TrackingType;


/**
 * <p>Java class for CardDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CardDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cardType" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}CardType" minOccurs="0"/&gt;
 *         &lt;element name="cardCategory" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}CardCategoryType" minOccurs="0"/&gt;
 *         &lt;element name="dispatch" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/card}DispatchType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tracking" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}TrackingType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="contractId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="expiryDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="driverId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="note" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeCardLife" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardDTO", propOrder = {
    "cardType",
    "cardCategory",
    "dispatch",
    "tracking"
})
public class CardDTO {

    protected CardType cardType;
    protected CardCategoryType cardCategory;
    @XmlElement(nillable = true)
    protected List<DispatchType> dispatch;
    @XmlElement(nillable = true)
    protected List<TrackingType> tracking;
    @XmlAttribute(name = "contractId")
    protected String contractId;
    @XmlAttribute(name = "status")
    protected String status;
    @XmlAttribute(name = "expiryDate")
    protected String expiryDate;
    @XmlAttribute(name = "driverId")
    protected String driverId;
    @XmlAttribute(name = "note")
    protected String note;
    @XmlAttribute(name = "countryCode")
    protected String countryCode;
    @XmlAttribute(name = "chargeCardLife")
    protected String chargeCardLife;

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link CardType }
     *     
     */
    public CardType getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardType }
     *     
     */
    public void setCardType(CardType value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the cardCategory property.
     * 
     * @return
     *     possible object is
     *     {@link CardCategoryType }
     *     
     */
    public CardCategoryType getCardCategory() {
        return cardCategory;
    }

    /**
     * Sets the value of the cardCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardCategoryType }
     *     
     */
    public void setCardCategory(CardCategoryType value) {
        this.cardCategory = value;
    }

    /**
     * Gets the value of the dispatch property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dispatch property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDispatch().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DispatchType }
     * 
     * 
     */
    public List<DispatchType> getDispatch() {
        if (dispatch == null) {
            dispatch = new ArrayList<DispatchType>();
        }
        return this.dispatch;
    }

    /**
     * Gets the value of the tracking property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tracking property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTracking().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TrackingType }
     * 
     * 
     */
    public List<TrackingType> getTracking() {
        if (tracking == null) {
            tracking = new ArrayList<TrackingType>();
        }
        return this.tracking;
    }

    /**
     * Gets the value of the contractId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * Sets the value of the contractId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractId(String value) {
        this.contractId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the driverId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverId() {
        return driverId;
    }

    /**
     * Sets the value of the driverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverId(String value) {
        this.driverId = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the chargeCardLife property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCardLife() {
        return chargeCardLife;
    }

    /**
     * Sets the value of the chargeCardLife property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCardLife(String value) {
        this.chargeCardLife = value;
    }

}
