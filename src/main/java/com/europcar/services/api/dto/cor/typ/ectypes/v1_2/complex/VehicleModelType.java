
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VehicleModelType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VehicleModelType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="vehicleModelId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="vehicleModelDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="vehicleModelPictureUrl" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleModelType")
public class VehicleModelType {

    @XmlAttribute(name = "vehicleModelId")
    protected String vehicleModelId;
    @XmlAttribute(name = "vehicleModelDescription")
    protected String vehicleModelDescription;
    @XmlAttribute(name = "vehicleModelPictureUrl")
    protected String vehicleModelPictureUrl;

    /**
     * Gets the value of the vehicleModelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleModelId() {
        return vehicleModelId;
    }

    /**
     * Sets the value of the vehicleModelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleModelId(String value) {
        this.vehicleModelId = value;
    }

    /**
     * Gets the value of the vehicleModelDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleModelDescription() {
        return vehicleModelDescription;
    }

    /**
     * Sets the value of the vehicleModelDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleModelDescription(String value) {
        this.vehicleModelDescription = value;
    }

    /**
     * Gets the value of the vehicleModelPictureUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleModelPictureUrl() {
        return vehicleModelPictureUrl;
    }

    /**
     * Sets the value of the vehicleModelPictureUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleModelPictureUrl(String value) {
        this.vehicleModelPictureUrl = value;
    }

}
