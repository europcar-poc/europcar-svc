
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactPhoneListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactPhoneListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contactPhone" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ContactPhoneType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactPhoneListType", propOrder = {
    "contactPhone"
})
public class ContactPhoneListType {

    @XmlElement(nillable = true)
    protected List<ContactPhoneType> contactPhone;

    /**
     * Gets the value of the contactPhone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactPhone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactPhone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactPhoneType }
     * 
     * 
     */
    public List<ContactPhoneType> getContactPhone() {
        if (contactPhone == null) {
            contactPhone = new ArrayList<ContactPhoneType>();
        }
        return this.contactPhone;
    }

}
