
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.LoyaltyCommunicationEnumType;


/**
 * <p>Java class for MembershipType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MembershipType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="programCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="expiryDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="communicationMean" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}LoyaltyCommunicationEnumType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MembershipType")
@XmlSeeAlso({
    PartnerLoyaltyProgramMembershipType.class
})
public class MembershipType {

    @XmlAttribute(name = "programCode", required = true)
    protected String programCode;
    @XmlAttribute(name = "memberNumber")
    protected String memberNumber;
    @XmlAttribute(name = "expiryDate")
    protected String expiryDate;
    @XmlAttribute(name = "communicationMean")
    protected LoyaltyCommunicationEnumType communicationMean;

    /**
     * Gets the value of the programCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramCode() {
        return programCode;
    }

    /**
     * Sets the value of the programCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramCode(String value) {
        this.programCode = value;
    }

    /**
     * Gets the value of the memberNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberNumber() {
        return memberNumber;
    }

    /**
     * Sets the value of the memberNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberNumber(String value) {
        this.memberNumber = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the communicationMean property.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyCommunicationEnumType }
     *     
     */
    public LoyaltyCommunicationEnumType getCommunicationMean() {
        return communicationMean;
    }

    /**
     * Sets the value of the communicationMean property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyCommunicationEnumType }
     *     
     */
    public void setCommunicationMean(LoyaltyCommunicationEnumType value) {
        this.communicationMean = value;
    }

}
