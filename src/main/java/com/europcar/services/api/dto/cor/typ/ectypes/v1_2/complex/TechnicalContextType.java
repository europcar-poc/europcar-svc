
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TechnicalContextType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TechnicalContextType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IT_SystemSource" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}IT_SystemType"/&gt;
 *         &lt;element name="serviceVersion" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}VersionType" minOccurs="0"/&gt;
 *         &lt;element name="user" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}UserType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TechnicalContextType", propOrder = {
    "itSystemSource",
    "serviceVersion",
    "user"
})
public class TechnicalContextType {

    @XmlElement(name = "IT_SystemSource", required = true)
    protected ITSystemType itSystemSource;
    protected VersionType serviceVersion;
    protected UserType user;

    /**
     * Gets the value of the itSystemSource property.
     * 
     * @return
     *     possible object is
     *     {@link ITSystemType }
     *     
     */
    public ITSystemType getITSystemSource() {
        return itSystemSource;
    }

    /**
     * Sets the value of the itSystemSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITSystemType }
     *     
     */
    public void setITSystemSource(ITSystemType value) {
        this.itSystemSource = value;
    }

    /**
     * Gets the value of the serviceVersion property.
     * 
     * @return
     *     possible object is
     *     {@link VersionType }
     *     
     */
    public VersionType getServiceVersion() {
        return serviceVersion;
    }

    /**
     * Sets the value of the serviceVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionType }
     *     
     */
    public void setServiceVersion(VersionType value) {
        this.serviceVersion = value;
    }

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link UserType }
     *     
     */
    public UserType getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserType }
     *     
     */
    public void setUser(UserType value) {
        this.user = value;
    }

}
