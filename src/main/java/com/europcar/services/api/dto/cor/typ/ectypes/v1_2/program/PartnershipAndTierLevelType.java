
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.program;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartnershipAndTierLevelType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartnershipAndTierLevelType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnership" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/program}PartnershipType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tierLevelList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/program}TierLevelListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnershipAndTierLevelType", propOrder = {
    "partnership",
    "tierLevelList"
})
public class PartnershipAndTierLevelType {

    @XmlElement(nillable = true)
    protected List<PartnershipType> partnership;
    protected TierLevelListType tierLevelList;

    /**
     * Gets the value of the partnership property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partnership property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartnership().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartnershipType }
     * 
     * 
     */
    public List<PartnershipType> getPartnership() {
        if (partnership == null) {
            partnership = new ArrayList<PartnershipType>();
        }
        return this.partnership;
    }

    /**
     * Gets the value of the tierLevelList property.
     * 
     * @return
     *     possible object is
     *     {@link TierLevelListType }
     *     
     */
    public TierLevelListType getTierLevelList() {
        return tierLevelList;
    }

    /**
     * Sets the value of the tierLevelList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TierLevelListType }
     *     
     */
    public void setTierLevelList(TierLevelListType value) {
        this.tierLevelList = value;
    }

}
