
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BrandingType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BrandingType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mailings" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}MailingListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BrandingType", propOrder = {
    "mailings"
})
public class BrandingType {

    @XmlElement(required = true)
    protected MailingListType mailings;

    /**
     * Gets the value of the mailings property.
     * 
     * @return
     *     possible object is
     *     {@link MailingListType }
     *     
     */
    public MailingListType getMailings() {
        return mailings;
    }

    /**
     * Sets the value of the mailings property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailingListType }
     *     
     */
    public void setMailings(MailingListType value) {
        this.mailings = value;
    }

}
