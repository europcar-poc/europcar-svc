
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.DeliveryCollectionType;


/**
 * <p>Java class for RateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="durationBand" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="durationMin" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="durationMax" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="durationBandName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="durationBandId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="equipmentList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="equipment" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}EquipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="insuranceList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="insurance" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}InsuranceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="surchargeList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="surcharge" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}SurchargeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="rentalData" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}RentalDataType" minOccurs="0"/&gt;
 *         &lt;element name="mileAge" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="mileAgeCap" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="mileAgeCapUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="chargePerUnitForExcessMileAge" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                 &lt;attribute name="mileAgeCapCoverage" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="upgrade" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="extra" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="extraMileAge" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="extraMileAgeCap" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                           &lt;attribute name="extraMileAgeCapUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="extraDay" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="chargeForExtraDayIfLate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                           &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                           &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                           &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="collection" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}DeliveryCollectionType" minOccurs="0"/&gt;
 *         &lt;element name="delivery" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}DeliveryCollectionType" minOccurs="0"/&gt;
 *         &lt;element name="afterHours" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="afterHoursCurrency" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="afterHoursCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="penalty" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                 &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="oneWay" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="charge" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="allowance" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="charge" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="discount" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="discountPercentage" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="baseRate" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="isTaxIncludedInBaseRate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="isFuelIncludedInBaseRate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="isBaseRateSecret" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="isPrepaidRate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateType", propOrder = {
    "durationBand",
    "equipmentList",
    "insuranceList",
    "surchargeList",
    "rentalData",
    "mileAge",
    "upgrade",
    "extra",
    "collection",
    "delivery",
    "afterHours",
    "penalty",
    "oneWay",
    "allowance",
    "discount"
})
public class RateType {

    protected RateType.DurationBand durationBand;
    protected RateType.EquipmentList equipmentList;
    protected RateType.InsuranceList insuranceList;
    protected RateType.SurchargeList surchargeList;
    protected RentalDataType rentalData;
    protected RateType.MileAge mileAge;
    protected RateType.Upgrade upgrade;
    protected RateType.Extra extra;
    protected DeliveryCollectionType collection;
    protected DeliveryCollectionType delivery;
    protected RateType.AfterHours afterHours;
    protected RateType.Penalty penalty;
    protected RateType.OneWay oneWay;
    protected RateType.Allowance allowance;
    protected RateType.Discount discount;
    @XmlAttribute(name = "baseRate")
    protected Double baseRate;
    @XmlAttribute(name = "isTaxIncludedInBaseRate")
    protected Boolean isTaxIncludedInBaseRate;
    @XmlAttribute(name = "isFuelIncludedInBaseRate")
    protected Boolean isFuelIncludedInBaseRate;
    @XmlAttribute(name = "isBaseRateSecret")
    protected Boolean isBaseRateSecret;
    @XmlAttribute(name = "toBeShown")
    protected String toBeShown;
    @XmlAttribute(name = "isActive")
    protected String isActive;
    @XmlAttribute(name = "isPrepaidRate")
    protected Boolean isPrepaidRate;

    /**
     * Gets the value of the durationBand property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.DurationBand }
     *     
     */
    public RateType.DurationBand getDurationBand() {
        return durationBand;
    }

    /**
     * Sets the value of the durationBand property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.DurationBand }
     *     
     */
    public void setDurationBand(RateType.DurationBand value) {
        this.durationBand = value;
    }

    /**
     * Gets the value of the equipmentList property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.EquipmentList }
     *     
     */
    public RateType.EquipmentList getEquipmentList() {
        return equipmentList;
    }

    /**
     * Sets the value of the equipmentList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.EquipmentList }
     *     
     */
    public void setEquipmentList(RateType.EquipmentList value) {
        this.equipmentList = value;
    }

    /**
     * Gets the value of the insuranceList property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.InsuranceList }
     *     
     */
    public RateType.InsuranceList getInsuranceList() {
        return insuranceList;
    }

    /**
     * Sets the value of the insuranceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.InsuranceList }
     *     
     */
    public void setInsuranceList(RateType.InsuranceList value) {
        this.insuranceList = value;
    }

    /**
     * Gets the value of the surchargeList property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.SurchargeList }
     *     
     */
    public RateType.SurchargeList getSurchargeList() {
        return surchargeList;
    }

    /**
     * Sets the value of the surchargeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.SurchargeList }
     *     
     */
    public void setSurchargeList(RateType.SurchargeList value) {
        this.surchargeList = value;
    }

    /**
     * Gets the value of the rentalData property.
     * 
     * @return
     *     possible object is
     *     {@link RentalDataType }
     *     
     */
    public RentalDataType getRentalData() {
        return rentalData;
    }

    /**
     * Sets the value of the rentalData property.
     * 
     * @param value
     *     allowed object is
     *     {@link RentalDataType }
     *     
     */
    public void setRentalData(RentalDataType value) {
        this.rentalData = value;
    }

    /**
     * Gets the value of the mileAge property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.MileAge }
     *     
     */
    public RateType.MileAge getMileAge() {
        return mileAge;
    }

    /**
     * Sets the value of the mileAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.MileAge }
     *     
     */
    public void setMileAge(RateType.MileAge value) {
        this.mileAge = value;
    }

    /**
     * Gets the value of the upgrade property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.Upgrade }
     *     
     */
    public RateType.Upgrade getUpgrade() {
        return upgrade;
    }

    /**
     * Sets the value of the upgrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.Upgrade }
     *     
     */
    public void setUpgrade(RateType.Upgrade value) {
        this.upgrade = value;
    }

    /**
     * Gets the value of the extra property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.Extra }
     *     
     */
    public RateType.Extra getExtra() {
        return extra;
    }

    /**
     * Sets the value of the extra property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.Extra }
     *     
     */
    public void setExtra(RateType.Extra value) {
        this.extra = value;
    }

    /**
     * Gets the value of the collection property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryCollectionType }
     *     
     */
    public DeliveryCollectionType getCollection() {
        return collection;
    }

    /**
     * Sets the value of the collection property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryCollectionType }
     *     
     */
    public void setCollection(DeliveryCollectionType value) {
        this.collection = value;
    }

    /**
     * Gets the value of the delivery property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryCollectionType }
     *     
     */
    public DeliveryCollectionType getDelivery() {
        return delivery;
    }

    /**
     * Sets the value of the delivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryCollectionType }
     *     
     */
    public void setDelivery(DeliveryCollectionType value) {
        this.delivery = value;
    }

    /**
     * Gets the value of the afterHours property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.AfterHours }
     *     
     */
    public RateType.AfterHours getAfterHours() {
        return afterHours;
    }

    /**
     * Sets the value of the afterHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.AfterHours }
     *     
     */
    public void setAfterHours(RateType.AfterHours value) {
        this.afterHours = value;
    }

    /**
     * Gets the value of the penalty property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.Penalty }
     *     
     */
    public RateType.Penalty getPenalty() {
        return penalty;
    }

    /**
     * Sets the value of the penalty property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.Penalty }
     *     
     */
    public void setPenalty(RateType.Penalty value) {
        this.penalty = value;
    }

    /**
     * Gets the value of the oneWay property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.OneWay }
     *     
     */
    public RateType.OneWay getOneWay() {
        return oneWay;
    }

    /**
     * Sets the value of the oneWay property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.OneWay }
     *     
     */
    public void setOneWay(RateType.OneWay value) {
        this.oneWay = value;
    }

    /**
     * Gets the value of the allowance property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.Allowance }
     *     
     */
    public RateType.Allowance getAllowance() {
        return allowance;
    }

    /**
     * Sets the value of the allowance property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.Allowance }
     *     
     */
    public void setAllowance(RateType.Allowance value) {
        this.allowance = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link RateType.Discount }
     *     
     */
    public RateType.Discount getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType.Discount }
     *     
     */
    public void setDiscount(RateType.Discount value) {
        this.discount = value;
    }

    /**
     * Gets the value of the baseRate property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBaseRate() {
        return baseRate;
    }

    /**
     * Sets the value of the baseRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBaseRate(Double value) {
        this.baseRate = value;
    }

    /**
     * Gets the value of the isTaxIncludedInBaseRate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTaxIncludedInBaseRate() {
        return isTaxIncludedInBaseRate;
    }

    /**
     * Sets the value of the isTaxIncludedInBaseRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTaxIncludedInBaseRate(Boolean value) {
        this.isTaxIncludedInBaseRate = value;
    }

    /**
     * Gets the value of the isFuelIncludedInBaseRate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsFuelIncludedInBaseRate() {
        return isFuelIncludedInBaseRate;
    }

    /**
     * Sets the value of the isFuelIncludedInBaseRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsFuelIncludedInBaseRate(Boolean value) {
        this.isFuelIncludedInBaseRate = value;
    }

    /**
     * Gets the value of the isBaseRateSecret property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBaseRateSecret() {
        return isBaseRateSecret;
    }

    /**
     * Sets the value of the isBaseRateSecret property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBaseRateSecret(Boolean value) {
        this.isBaseRateSecret = value;
    }

    /**
     * Gets the value of the toBeShown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToBeShown() {
        return toBeShown;
    }

    /**
     * Sets the value of the toBeShown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToBeShown(String value) {
        this.toBeShown = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the isPrepaidRate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPrepaidRate() {
        return isPrepaidRate;
    }

    /**
     * Sets the value of the isPrepaidRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPrepaidRate(Boolean value) {
        this.isPrepaidRate = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="afterHoursCurrency" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="afterHoursCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AfterHours {

        @XmlAttribute(name = "afterHoursCurrency")
        @XmlSchemaType(name = "anySimpleType")
        protected String afterHoursCurrency;
        @XmlAttribute(name = "isActive")
        protected String isActive;
        @XmlAttribute(name = "toBeShown")
        protected String toBeShown;
        @XmlAttribute(name = "afterHoursCharge")
        protected BigDecimal afterHoursCharge;

        /**
         * Gets the value of the afterHoursCurrency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAfterHoursCurrency() {
            return afterHoursCurrency;
        }

        /**
         * Sets the value of the afterHoursCurrency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAfterHoursCurrency(String value) {
            this.afterHoursCurrency = value;
        }

        /**
         * Gets the value of the isActive property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsActive() {
            return isActive;
        }

        /**
         * Sets the value of the isActive property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsActive(String value) {
            this.isActive = value;
        }

        /**
         * Gets the value of the toBeShown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToBeShown() {
            return toBeShown;
        }

        /**
         * Sets the value of the toBeShown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToBeShown(String value) {
            this.toBeShown = value;
        }

        /**
         * Gets the value of the afterHoursCharge property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAfterHoursCharge() {
            return afterHoursCharge;
        }

        /**
         * Sets the value of the afterHoursCharge property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAfterHoursCharge(BigDecimal value) {
            this.afterHoursCharge = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="charge" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Allowance {

        @XmlAttribute(name = "isActive")
        protected String isActive;
        @XmlAttribute(name = "toBeShown")
        protected String toBeShown;
        @XmlAttribute(name = "charge")
        protected BigInteger charge;

        /**
         * Gets the value of the isActive property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsActive() {
            return isActive;
        }

        /**
         * Sets the value of the isActive property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsActive(String value) {
            this.isActive = value;
        }

        /**
         * Gets the value of the toBeShown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToBeShown() {
            return toBeShown;
        }

        /**
         * Sets the value of the toBeShown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToBeShown(String value) {
            this.toBeShown = value;
        }

        /**
         * Gets the value of the charge property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCharge() {
            return charge;
        }

        /**
         * Sets the value of the charge property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCharge(BigInteger value) {
            this.charge = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="discountPercentage" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Discount {

        @XmlAttribute(name = "isActive")
        protected String isActive;
        @XmlAttribute(name = "toBeShown")
        protected String toBeShown;
        @XmlAttribute(name = "discountPercentage")
        protected Double discountPercentage;

        /**
         * Gets the value of the isActive property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsActive() {
            return isActive;
        }

        /**
         * Sets the value of the isActive property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsActive(String value) {
            this.isActive = value;
        }

        /**
         * Gets the value of the toBeShown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToBeShown() {
            return toBeShown;
        }

        /**
         * Sets the value of the toBeShown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToBeShown(String value) {
            this.toBeShown = value;
        }

        /**
         * Gets the value of the discountPercentage property.
         * 
         * @return
         *     possible object is
         *     {@link Double }
         *     
         */
        public Double getDiscountPercentage() {
            return discountPercentage;
        }

        /**
         * Sets the value of the discountPercentage property.
         * 
         * @param value
         *     allowed object is
         *     {@link Double }
         *     
         */
        public void setDiscountPercentage(Double value) {
            this.discountPercentage = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="durationMin" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="durationMax" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="durationBandName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="durationBandId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DurationBand {

        @XmlAttribute(name = "durationMin")
        protected BigInteger durationMin;
        @XmlAttribute(name = "durationMax")
        protected BigInteger durationMax;
        @XmlAttribute(name = "durationBandName")
        protected String durationBandName;
        @XmlAttribute(name = "durationBandId")
        protected BigInteger durationBandId;
        @XmlAttribute(name = "durationUnit")
        protected String durationUnit;
        @XmlAttribute(name = "effectiveDuration")
        protected BigInteger effectiveDuration;

        /**
         * Gets the value of the durationMin property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDurationMin() {
            return durationMin;
        }

        /**
         * Sets the value of the durationMin property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDurationMin(BigInteger value) {
            this.durationMin = value;
        }

        /**
         * Gets the value of the durationMax property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDurationMax() {
            return durationMax;
        }

        /**
         * Sets the value of the durationMax property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDurationMax(BigInteger value) {
            this.durationMax = value;
        }

        /**
         * Gets the value of the durationBandName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDurationBandName() {
            return durationBandName;
        }

        /**
         * Sets the value of the durationBandName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDurationBandName(String value) {
            this.durationBandName = value;
        }

        /**
         * Gets the value of the durationBandId property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDurationBandId() {
            return durationBandId;
        }

        /**
         * Sets the value of the durationBandId property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDurationBandId(BigInteger value) {
            this.durationBandId = value;
        }

        /**
         * Gets the value of the durationUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDurationUnit() {
            return durationUnit;
        }

        /**
         * Sets the value of the durationUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDurationUnit(String value) {
            this.durationUnit = value;
        }

        /**
         * Gets the value of the effectiveDuration property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getEffectiveDuration() {
            return effectiveDuration;
        }

        /**
         * Sets the value of the effectiveDuration property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setEffectiveDuration(BigInteger value) {
            this.effectiveDuration = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="equipment" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}EquipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "equipment"
    })
    public static class EquipmentList {

        @XmlElement(nillable = true)
        protected List<EquipmentType> equipment;

        /**
         * Gets the value of the equipment property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the equipment property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEquipment().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EquipmentType }
         * 
         * 
         */
        public List<EquipmentType> getEquipment() {
            if (equipment == null) {
                equipment = new ArrayList<EquipmentType>();
            }
            return this.equipment;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="extraMileAge" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="extraMileAgeCap" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *                 &lt;attribute name="extraMileAgeCapUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="extraDay" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="chargeForExtraDayIfLate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *                 &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *                 &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *                 &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "extraMileAge",
        "extraDay"
    })
    public static class Extra {

        protected RateType.Extra.ExtraMileAge extraMileAge;
        protected RateType.Extra.ExtraDay extraDay;

        /**
         * Gets the value of the extraMileAge property.
         * 
         * @return
         *     possible object is
         *     {@link RateType.Extra.ExtraMileAge }
         *     
         */
        public RateType.Extra.ExtraMileAge getExtraMileAge() {
            return extraMileAge;
        }

        /**
         * Sets the value of the extraMileAge property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateType.Extra.ExtraMileAge }
         *     
         */
        public void setExtraMileAge(RateType.Extra.ExtraMileAge value) {
            this.extraMileAge = value;
        }

        /**
         * Gets the value of the extraDay property.
         * 
         * @return
         *     possible object is
         *     {@link RateType.Extra.ExtraDay }
         *     
         */
        public RateType.Extra.ExtraDay getExtraDay() {
            return extraDay;
        }

        /**
         * Sets the value of the extraDay property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateType.Extra.ExtraDay }
         *     
         */
        public void setExtraDay(RateType.Extra.ExtraDay value) {
            this.extraDay = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="chargeForExtraDayIfLate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
         *       &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
         *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
         *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ExtraDay {

            @XmlAttribute(name = "chargeForExtraDayIfLate")
            protected BigDecimal chargeForExtraDayIfLate;
            @XmlAttribute(name = "durationUnit")
            protected String durationUnit;
            @XmlAttribute(name = "effectiveDuration")
            protected BigInteger effectiveDuration;
            @XmlAttribute(name = "amount")
            protected BigDecimal amount;
            @XmlAttribute(name = "isActive")
            protected String isActive;
            @XmlAttribute(name = "toBeShown")
            protected String toBeShown;

            /**
             * Gets the value of the chargeForExtraDayIfLate property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getChargeForExtraDayIfLate() {
                return chargeForExtraDayIfLate;
            }

            /**
             * Sets the value of the chargeForExtraDayIfLate property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setChargeForExtraDayIfLate(BigDecimal value) {
                this.chargeForExtraDayIfLate = value;
            }

            /**
             * Gets the value of the durationUnit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDurationUnit() {
                return durationUnit;
            }

            /**
             * Sets the value of the durationUnit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDurationUnit(String value) {
                this.durationUnit = value;
            }

            /**
             * Gets the value of the effectiveDuration property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getEffectiveDuration() {
                return effectiveDuration;
            }

            /**
             * Sets the value of the effectiveDuration property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setEffectiveDuration(BigInteger value) {
                this.effectiveDuration = value;
            }

            /**
             * Gets the value of the amount property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Sets the value of the amount property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

            /**
             * Gets the value of the isActive property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsActive() {
                return isActive;
            }

            /**
             * Sets the value of the isActive property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsActive(String value) {
                this.isActive = value;
            }

            /**
             * Gets the value of the toBeShown property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getToBeShown() {
                return toBeShown;
            }

            /**
             * Sets the value of the toBeShown property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setToBeShown(String value) {
                this.toBeShown = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="extraMileAgeCap" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
         *       &lt;attribute name="extraMileAgeCapUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ExtraMileAge {

            @XmlAttribute(name = "extraMileAgeCap")
            protected BigInteger extraMileAgeCap;
            @XmlAttribute(name = "extraMileAgeCapUnit")
            protected String extraMileAgeCapUnit;
            @XmlAttribute(name = "durationUnit")
            protected String durationUnit;
            @XmlAttribute(name = "isActive")
            protected String isActive;
            @XmlAttribute(name = "toBeShown")
            protected String toBeShown;

            /**
             * Gets the value of the extraMileAgeCap property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getExtraMileAgeCap() {
                return extraMileAgeCap;
            }

            /**
             * Sets the value of the extraMileAgeCap property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setExtraMileAgeCap(BigInteger value) {
                this.extraMileAgeCap = value;
            }

            /**
             * Gets the value of the extraMileAgeCapUnit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExtraMileAgeCapUnit() {
                return extraMileAgeCapUnit;
            }

            /**
             * Sets the value of the extraMileAgeCapUnit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExtraMileAgeCapUnit(String value) {
                this.extraMileAgeCapUnit = value;
            }

            /**
             * Gets the value of the durationUnit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDurationUnit() {
                return durationUnit;
            }

            /**
             * Sets the value of the durationUnit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDurationUnit(String value) {
                this.durationUnit = value;
            }

            /**
             * Gets the value of the isActive property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsActive() {
                return isActive;
            }

            /**
             * Sets the value of the isActive property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsActive(String value) {
                this.isActive = value;
            }

            /**
             * Gets the value of the toBeShown property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getToBeShown() {
                return toBeShown;
            }

            /**
             * Sets the value of the toBeShown property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setToBeShown(String value) {
                this.toBeShown = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="insurance" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}InsuranceType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "insurance"
    })
    public static class InsuranceList {

        @XmlElement(nillable = true)
        protected List<InsuranceType> insurance;

        /**
         * Gets the value of the insurance property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the insurance property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInsurance().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link InsuranceType }
         * 
         * 
         */
        public List<InsuranceType> getInsurance() {
            if (insurance == null) {
                insurance = new ArrayList<InsuranceType>();
            }
            return this.insurance;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="mileAgeCap" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="mileAgeCapUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="chargePerUnitForExcessMileAge" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *       &lt;attribute name="mileAgeCapCoverage" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MileAge {

        @XmlAttribute(name = "mileAgeCap")
        protected BigInteger mileAgeCap;
        @XmlAttribute(name = "mileAgeCapUnit")
        protected String mileAgeCapUnit;
        @XmlAttribute(name = "chargePerUnitForExcessMileAge")
        protected BigDecimal chargePerUnitForExcessMileAge;
        @XmlAttribute(name = "mileAgeCapCoverage")
        protected String mileAgeCapCoverage;
        @XmlAttribute(name = "isActive")
        protected String isActive;
        @XmlAttribute(name = "toBeShown")
        protected String toBeShown;

        /**
         * Gets the value of the mileAgeCap property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMileAgeCap() {
            return mileAgeCap;
        }

        /**
         * Sets the value of the mileAgeCap property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMileAgeCap(BigInteger value) {
            this.mileAgeCap = value;
        }

        /**
         * Gets the value of the mileAgeCapUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMileAgeCapUnit() {
            return mileAgeCapUnit;
        }

        /**
         * Sets the value of the mileAgeCapUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMileAgeCapUnit(String value) {
            this.mileAgeCapUnit = value;
        }

        /**
         * Gets the value of the chargePerUnitForExcessMileAge property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getChargePerUnitForExcessMileAge() {
            return chargePerUnitForExcessMileAge;
        }

        /**
         * Sets the value of the chargePerUnitForExcessMileAge property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setChargePerUnitForExcessMileAge(BigDecimal value) {
            this.chargePerUnitForExcessMileAge = value;
        }

        /**
         * Gets the value of the mileAgeCapCoverage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMileAgeCapCoverage() {
            return mileAgeCapCoverage;
        }

        /**
         * Sets the value of the mileAgeCapCoverage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMileAgeCapCoverage(String value) {
            this.mileAgeCapCoverage = value;
        }

        /**
         * Gets the value of the isActive property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsActive() {
            return isActive;
        }

        /**
         * Sets the value of the isActive property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsActive(String value) {
            this.isActive = value;
        }

        /**
         * Gets the value of the toBeShown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToBeShown() {
            return toBeShown;
        }

        /**
         * Sets the value of the toBeShown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToBeShown(String value) {
            this.toBeShown = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="charge" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OneWay {

        @XmlAttribute(name = "charge")
        protected BigDecimal charge;
        @XmlAttribute(name = "isActive")
        protected String isActive;
        @XmlAttribute(name = "toBeShown")
        protected String toBeShown;

        /**
         * Gets the value of the charge property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCharge() {
            return charge;
        }

        /**
         * Sets the value of the charge property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCharge(BigDecimal value) {
            this.charge = value;
        }

        /**
         * Gets the value of the isActive property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsActive() {
            return isActive;
        }

        /**
         * Sets the value of the isActive property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsActive(String value) {
            this.isActive = value;
        }

        /**
         * Gets the value of the toBeShown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToBeShown() {
            return toBeShown;
        }

        /**
         * Sets the value of the toBeShown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToBeShown(String value) {
            this.toBeShown = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *       &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Penalty {

        @XmlAttribute(name = "amount")
        protected BigDecimal amount;
        @XmlAttribute(name = "effectiveDuration")
        protected BigInteger effectiveDuration;
        @XmlAttribute(name = "durationUnit")
        protected String durationUnit;
        @XmlAttribute(name = "isActive")
        protected String isActive;
        @XmlAttribute(name = "toBeShown")
        protected String toBeShown;

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

        /**
         * Gets the value of the effectiveDuration property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getEffectiveDuration() {
            return effectiveDuration;
        }

        /**
         * Sets the value of the effectiveDuration property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setEffectiveDuration(BigInteger value) {
            this.effectiveDuration = value;
        }

        /**
         * Gets the value of the durationUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDurationUnit() {
            return durationUnit;
        }

        /**
         * Sets the value of the durationUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDurationUnit(String value) {
            this.durationUnit = value;
        }

        /**
         * Gets the value of the isActive property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsActive() {
            return isActive;
        }

        /**
         * Sets the value of the isActive property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsActive(String value) {
            this.isActive = value;
        }

        /**
         * Gets the value of the toBeShown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToBeShown() {
            return toBeShown;
        }

        /**
         * Sets the value of the toBeShown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToBeShown(String value) {
            this.toBeShown = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="surcharge" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}SurchargeType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "surcharge"
    })
    public static class SurchargeList {

        @XmlElement(nillable = true)
        protected List<SurchargeType> surcharge;

        /**
         * Gets the value of the surcharge property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the surcharge property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSurcharge().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SurchargeType }
         * 
         * 
         */
        public List<SurchargeType> getSurcharge() {
            if (surcharge == null) {
                surcharge = new ArrayList<SurchargeType>();
            }
            return this.surcharge;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="effectiveDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Upgrade {

        @XmlAttribute(name = "amount")
        protected BigInteger amount;
        @XmlAttribute(name = "durationUnit")
        protected String durationUnit;
        @XmlAttribute(name = "effectiveDuration")
        protected BigInteger effectiveDuration;
        @XmlAttribute(name = "isActive")
        protected String isActive;
        @XmlAttribute(name = "toBeShown")
        protected String toBeShown;

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setAmount(BigInteger value) {
            this.amount = value;
        }

        /**
         * Gets the value of the durationUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDurationUnit() {
            return durationUnit;
        }

        /**
         * Sets the value of the durationUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDurationUnit(String value) {
            this.durationUnit = value;
        }

        /**
         * Gets the value of the effectiveDuration property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getEffectiveDuration() {
            return effectiveDuration;
        }

        /**
         * Sets the value of the effectiveDuration property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setEffectiveDuration(BigInteger value) {
            this.effectiveDuration = value;
        }

        /**
         * Gets the value of the isActive property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsActive() {
            return isActive;
        }

        /**
         * Sets the value of the isActive property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsActive(String value) {
            this.isActive = value;
        }

        /**
         * Gets the value of the toBeShown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToBeShown() {
            return toBeShown;
        }

        /**
         * Sets the value of the toBeShown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToBeShown(String value) {
            this.toBeShown = value;
        }

    }

}
