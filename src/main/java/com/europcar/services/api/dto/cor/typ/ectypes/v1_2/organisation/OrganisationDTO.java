
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.organisation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.busacct.BusinessAccountDTO;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.AddressType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ExternalReferenceType;


/**
 * <p>Java class for OrganisationDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrganisationDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="orgRole" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="brand" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="businessAccount" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/busAcct}BusinessAccountDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="contract" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="contractNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="contractType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="contractLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="contractStatus" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="referenceList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="reference" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ExternalReferenceType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="addressList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="address" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="organisationId" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="country" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationDTO", propOrder = {
    "orgRole",
    "brand",
    "businessAccount",
    "contract",
    "referenceList",
    "addressList"
})
public class OrganisationDTO {

    protected String orgRole;
    protected String brand;
    @XmlElement(nillable = true)
    protected List<BusinessAccountDTO> businessAccount;
    @XmlElement(nillable = true)
    protected List<OrganisationDTO.Contract> contract;
    protected OrganisationDTO.ReferenceList referenceList;
    protected OrganisationDTO.AddressList addressList;
    @XmlAttribute(name = "organisationId")
    protected Integer organisationId;
    @XmlAttribute(name = "country")
    protected String country;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "status")
    protected String status;

    /**
     * Gets the value of the orgRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgRole() {
        return orgRole;
    }

    /**
     * Sets the value of the orgRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgRole(String value) {
        this.orgRole = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the businessAccount property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the businessAccount property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusinessAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessAccountDTO }
     * 
     * 
     */
    public List<BusinessAccountDTO> getBusinessAccount() {
        if (businessAccount == null) {
            businessAccount = new ArrayList<BusinessAccountDTO>();
        }
        return this.businessAccount;
    }

    /**
     * Gets the value of the contract property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contract property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContract().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrganisationDTO.Contract }
     * 
     * 
     */
    public List<OrganisationDTO.Contract> getContract() {
        if (contract == null) {
            contract = new ArrayList<OrganisationDTO.Contract>();
        }
        return this.contract;
    }

    /**
     * Gets the value of the referenceList property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationDTO.ReferenceList }
     *     
     */
    public OrganisationDTO.ReferenceList getReferenceList() {
        return referenceList;
    }

    /**
     * Sets the value of the referenceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationDTO.ReferenceList }
     *     
     */
    public void setReferenceList(OrganisationDTO.ReferenceList value) {
        this.referenceList = value;
    }

    /**
     * Gets the value of the addressList property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationDTO.AddressList }
     *     
     */
    public OrganisationDTO.AddressList getAddressList() {
        return addressList;
    }

    /**
     * Sets the value of the addressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationDTO.AddressList }
     *     
     */
    public void setAddressList(OrganisationDTO.AddressList value) {
        this.addressList = value;
    }

    /**
     * Gets the value of the organisationId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrganisationId() {
        return organisationId;
    }

    /**
     * Sets the value of the organisationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrganisationId(Integer value) {
        this.organisationId = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="address" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "address"
    })
    public static class AddressList {

        @XmlElement(nillable = true)
        protected List<AddressType> address;

        /**
         * Gets the value of the address property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the address property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddress().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AddressType }
         * 
         * 
         */
        public List<AddressType> getAddress() {
            if (address == null) {
                address = new ArrayList<AddressType>();
            }
            return this.address;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="contractNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="contractType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="contractLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="contractStatus" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Contract {

        @XmlAttribute(name = "contractNumber")
        protected BigInteger contractNumber;
        @XmlAttribute(name = "contractType")
        protected String contractType;
        @XmlAttribute(name = "contractLabel")
        protected String contractLabel;
        @XmlAttribute(name = "contractStatus")
        protected String contractStatus;

        /**
         * Gets the value of the contractNumber property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getContractNumber() {
            return contractNumber;
        }

        /**
         * Sets the value of the contractNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setContractNumber(BigInteger value) {
            this.contractNumber = value;
        }

        /**
         * Gets the value of the contractType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractType() {
            return contractType;
        }

        /**
         * Sets the value of the contractType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractType(String value) {
            this.contractType = value;
        }

        /**
         * Gets the value of the contractLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractLabel() {
            return contractLabel;
        }

        /**
         * Sets the value of the contractLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractLabel(String value) {
            this.contractLabel = value;
        }

        /**
         * Gets the value of the contractStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractStatus() {
            return contractStatus;
        }

        /**
         * Sets the value of the contractStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractStatus(String value) {
            this.contractStatus = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="reference" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ExternalReferenceType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class ReferenceList {

        @XmlElement(required = true)
        protected ExternalReferenceType reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link ExternalReferenceType }
         *     
         */
        public ExternalReferenceType getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link ExternalReferenceType }
         *     
         */
        public void setReference(ExternalReferenceType value) {
            this.reference = value;
        }

    }

}
