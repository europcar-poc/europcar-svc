
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.AddressEnumType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.ApproachEnumType;


/**
 * <p>Java class for ApproachPointType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApproachPointType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addressType" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}AddressEnumType" minOccurs="0"/&gt;
 *         &lt;element name="mailingAddressType" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" minOccurs="0"/&gt;
 *         &lt;element name="emails" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}EmailListType" minOccurs="0"/&gt;
 *         &lt;element name="phones" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PhoneListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="approachType" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}ApproachEnumType" /&gt;
 *       &lt;attribute name="startDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="marketingStatus" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AdressMarketingStatusType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApproachPointType", propOrder = {
    "addressType",
    "mailingAddressType",
    "emails",
    "phones"
})
public class ApproachPointType {

    @XmlSchemaType(name = "string")
    protected AddressEnumType addressType;
    protected AddressType mailingAddressType;
    protected EmailListType emails;
    protected PhoneListType phones;
    @XmlAttribute(name = "approachType")
    protected ApproachEnumType approachType;
    @XmlAttribute(name = "startDate")
    protected String startDate;
    @XmlAttribute(name = "marketingStatus")
    protected AdressMarketingStatusType marketingStatus;

    /**
     * Gets the value of the addressType property.
     * 
     * @return
     *     possible object is
     *     {@link AddressEnumType }
     *     
     */
    public AddressEnumType getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressEnumType }
     *     
     */
    public void setAddressType(AddressEnumType value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the mailingAddressType property.
     * 
     * @return
     *     possible object is
     *     {@link AddressType }
     *     
     */
    public AddressType getMailingAddressType() {
        return mailingAddressType;
    }

    /**
     * Sets the value of the mailingAddressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressType }
     *     
     */
    public void setMailingAddressType(AddressType value) {
        this.mailingAddressType = value;
    }

    /**
     * Gets the value of the emails property.
     * 
     * @return
     *     possible object is
     *     {@link EmailListType }
     *     
     */
    public EmailListType getEmails() {
        return emails;
    }

    /**
     * Sets the value of the emails property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailListType }
     *     
     */
    public void setEmails(EmailListType value) {
        this.emails = value;
    }

    /**
     * Gets the value of the phones property.
     * 
     * @return
     *     possible object is
     *     {@link PhoneListType }
     *     
     */
    public PhoneListType getPhones() {
        return phones;
    }

    /**
     * Sets the value of the phones property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneListType }
     *     
     */
    public void setPhones(PhoneListType value) {
        this.phones = value;
    }

    /**
     * Gets the value of the approachType property.
     * 
     * @return
     *     possible object is
     *     {@link ApproachEnumType }
     *     
     */
    public ApproachEnumType getApproachType() {
        return approachType;
    }

    /**
     * Sets the value of the approachType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApproachEnumType }
     *     
     */
    public void setApproachType(ApproachEnumType value) {
        this.approachType = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the marketingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link AdressMarketingStatusType }
     *     
     */
    public AdressMarketingStatusType getMarketingStatus() {
        return marketingStatus;
    }

    /**
     * Sets the value of the marketingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdressMarketingStatusType }
     *     
     */
    public void setMarketingStatus(AdressMarketingStatusType value) {
        this.marketingStatus = value;
    }

}
