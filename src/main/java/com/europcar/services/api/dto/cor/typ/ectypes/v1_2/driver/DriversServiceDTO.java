
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DriversServiceDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriversServiceDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="service" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="driverId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *                   &lt;element name="serviceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="serviceDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *                   &lt;element name="preferenceFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="serviceStrValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="sourceSrv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="lastUpdatedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="lastUpdateDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriversServiceDTO", propOrder = {
    "service"
})
public class DriversServiceDTO {

    @XmlElement(nillable = true)
    protected List<DriversServiceDTO.Service> service;

    /**
     * Gets the value of the service property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the service property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DriversServiceDTO.Service }
     * 
     * 
     */
    public List<DriversServiceDTO.Service> getService() {
        if (service == null) {
            service = new ArrayList<DriversServiceDTO.Service>();
        }
        return this.service;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="driverId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
     *         &lt;element name="serviceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="serviceDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
     *         &lt;element name="preferenceFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="serviceStrValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="sourceSrv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="lastUpdatedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="lastUpdateDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "driverId",
        "serviceCode",
        "serviceDescr",
        "serviceID",
        "preferenceFlag",
        "serviceStrValue",
        "sourceSrv",
        "lastUpdatedBy",
        "lastUpdateDate"
    })
    public static class Service {

        protected Long driverId;
        protected String serviceCode;
        protected String serviceDescr;
        protected Long serviceID;
        protected String preferenceFlag;
        protected String serviceStrValue;
        protected String sourceSrv;
        protected String lastUpdatedBy;
        protected String lastUpdateDate;

        /**
         * Gets the value of the driverId property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getDriverId() {
            return driverId;
        }

        /**
         * Sets the value of the driverId property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setDriverId(Long value) {
            this.driverId = value;
        }

        /**
         * Gets the value of the serviceCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceCode() {
            return serviceCode;
        }

        /**
         * Sets the value of the serviceCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceCode(String value) {
            this.serviceCode = value;
        }

        /**
         * Gets the value of the serviceDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceDescr() {
            return serviceDescr;
        }

        /**
         * Sets the value of the serviceDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceDescr(String value) {
            this.serviceDescr = value;
        }

        /**
         * Gets the value of the serviceID property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getServiceID() {
            return serviceID;
        }

        /**
         * Sets the value of the serviceID property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setServiceID(Long value) {
            this.serviceID = value;
        }

        /**
         * Gets the value of the preferenceFlag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreferenceFlag() {
            return preferenceFlag;
        }

        /**
         * Sets the value of the preferenceFlag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreferenceFlag(String value) {
            this.preferenceFlag = value;
        }

        /**
         * Gets the value of the serviceStrValue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceStrValue() {
            return serviceStrValue;
        }

        /**
         * Sets the value of the serviceStrValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceStrValue(String value) {
            this.serviceStrValue = value;
        }

        /**
         * Gets the value of the sourceSrv property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSourceSrv() {
            return sourceSrv;
        }

        /**
         * Sets the value of the sourceSrv property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSourceSrv(String value) {
            this.sourceSrv = value;
        }

        /**
         * Gets the value of the lastUpdatedBy property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLastUpdatedBy() {
            return lastUpdatedBy;
        }

        /**
         * Sets the value of the lastUpdatedBy property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLastUpdatedBy(String value) {
            this.lastUpdatedBy = value;
        }

        /**
         * Gets the value of the lastUpdateDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLastUpdateDate() {
            return lastUpdateDate;
        }

        /**
         * Sets the value of the lastUpdateDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLastUpdateDate(String value) {
            this.lastUpdateDate = value;
        }

    }

}
