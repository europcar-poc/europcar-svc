
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.YNType;


/**
 * <p>Java class for MailingType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MailingType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="brandCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="acceptMailing" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}YNType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MailingType")
public class MailingType {

    @XmlAttribute(name = "brandCode")
    protected String brandCode;
    @XmlAttribute(name = "acceptMailing")
    protected YNType acceptMailing;

    /**
     * Gets the value of the brandCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandCode() {
        return brandCode;
    }

    /**
     * Sets the value of the brandCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandCode(String value) {
        this.brandCode = value;
    }

    /**
     * Gets the value of the acceptMailing property.
     * 
     * @return
     *     possible object is
     *     {@link YNType }
     *     
     */
    public YNType getAcceptMailing() {
        return acceptMailing;
    }

    /**
     * Sets the value of the acceptMailing property.
     * 
     * @param value
     *     allowed object is
     *     {@link YNType }
     *     
     */
    public void setAcceptMailing(YNType value) {
        this.acceptMailing = value;
    }

}
