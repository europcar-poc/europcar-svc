
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.YNType;


/**
 * <p>Java class for ServiceLevelInsurancesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceLevelInsurancesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="TW" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}YNType" /&gt;
 *       &lt;attribute name="CDW" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}YNType" /&gt;
 *       &lt;attribute name="PAI" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}YNType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceLevelInsurancesType")
public class ServiceLevelInsurancesType {

    @XmlAttribute(name = "TW")
    protected YNType tw;
    @XmlAttribute(name = "CDW")
    protected YNType cdw;
    @XmlAttribute(name = "PAI")
    protected YNType pai;

    /**
     * Gets the value of the tw property.
     * 
     * @return
     *     possible object is
     *     {@link YNType }
     *     
     */
    public YNType getTW() {
        return tw;
    }

    /**
     * Sets the value of the tw property.
     * 
     * @param value
     *     allowed object is
     *     {@link YNType }
     *     
     */
    public void setTW(YNType value) {
        this.tw = value;
    }

    /**
     * Gets the value of the cdw property.
     * 
     * @return
     *     possible object is
     *     {@link YNType }
     *     
     */
    public YNType getCDW() {
        return cdw;
    }

    /**
     * Sets the value of the cdw property.
     * 
     * @param value
     *     allowed object is
     *     {@link YNType }
     *     
     */
    public void setCDW(YNType value) {
        this.cdw = value;
    }

    /**
     * Gets the value of the pai property.
     * 
     * @return
     *     possible object is
     *     {@link YNType }
     *     
     */
    public YNType getPAI() {
        return pai;
    }

    /**
     * Sets the value of the pai property.
     * 
     * @param value
     *     allowed object is
     *     {@link YNType }
     *     
     */
    public void setPAI(YNType value) {
        this.pai = value;
    }

}
