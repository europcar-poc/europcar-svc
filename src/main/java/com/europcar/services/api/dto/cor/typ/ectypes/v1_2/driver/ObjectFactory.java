
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DriversServiceDTO }
     * 
     */
    public DriversServiceDTO createDriversServiceDTO() {
        return new DriversServiceDTO();
    }

    /**
     * Create an instance of {@link DriverDTO }
     * 
     */
    public DriverDTO createDriverDTO() {
        return new DriverDTO();
    }

    /**
     * Create an instance of {@link DriversServicType }
     * 
     */
    public DriversServicType createDriversServicType() {
        return new DriversServicType();
    }

    /**
     * Create an instance of {@link LicenseListType }
     * 
     */
    public LicenseListType createLicenseListType() {
        return new LicenseListType();
    }

    /**
     * Create an instance of {@link LicenseType }
     * 
     */
    public LicenseType createLicenseType() {
        return new LicenseType();
    }

    /**
     * Create an instance of {@link LegalIdType }
     * 
     */
    public LegalIdType createLegalIdType() {
        return new LegalIdType();
    }

    /**
     * Create an instance of {@link LegalIdListType }
     * 
     */
    public LegalIdListType createLegalIdListType() {
        return new LegalIdListType();
    }

    /**
     * Create an instance of {@link BrandingType }
     * 
     */
    public BrandingType createBrandingType() {
        return new BrandingType();
    }

    /**
     * Create an instance of {@link MailingListType }
     * 
     */
    public MailingListType createMailingListType() {
        return new MailingListType();
    }

    /**
     * Create an instance of {@link MailingType }
     * 
     */
    public MailingType createMailingType() {
        return new MailingType();
    }

    /**
     * Create an instance of {@link AdditionalDriverDTO }
     * 
     */
    public AdditionalDriverDTO createAdditionalDriverDTO() {
        return new AdditionalDriverDTO();
    }

    /**
     * Create an instance of {@link DriverListType }
     * 
     */
    public DriverListType createDriverListType() {
        return new DriverListType();
    }

    /**
     * Create an instance of {@link DriverProfile }
     * 
     */
    public DriverProfile createDriverProfile() {
        return new DriverProfile();
    }

    /**
     * Create an instance of {@link DriversServiceDTO.Service }
     * 
     */
    public DriversServiceDTO.Service createDriversServiceDTOService() {
        return new DriversServiceDTO.Service();
    }

    /**
     * Create an instance of {@link DriverDTO.CustomerExternalReferenceList }
     * 
     */
    public DriverDTO.CustomerExternalReferenceList createDriverDTOCustomerExternalReferenceList() {
        return new DriverDTO.CustomerExternalReferenceList();
    }

    /**
     * Create an instance of {@link DriverDTO.Preferences }
     * 
     */
    public DriverDTO.Preferences createDriverDTOPreferences() {
        return new DriverDTO.Preferences();
    }

    /**
     * Create an instance of {@link DriversServicType.Service }
     * 
     */
    public DriversServicType.Service createDriversServicTypeService() {
        return new DriversServicType.Service();
    }

}
