
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.technical;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.europcar.services.api.dto.cor.typ.ectypes.v1_2.technical package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TechnicalFault_QNAME = new QName("http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/technical", "technicalFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.europcar.services.api.dto.cor.typ.ectypes.v1_2.technical
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TechnicalFaultDTO }
     * 
     */
    public TechnicalFaultDTO createTechnicalFaultDTO() {
        return new TechnicalFaultDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TechnicalFaultDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/technical", name = "technicalFault")
    public JAXBElement<TechnicalFaultDTO> createTechnicalFault(TechnicalFaultDTO value) {
        return new JAXBElement<TechnicalFaultDTO>(_TechnicalFault_QNAME, TechnicalFaultDTO.class, null, value);
    }

}
