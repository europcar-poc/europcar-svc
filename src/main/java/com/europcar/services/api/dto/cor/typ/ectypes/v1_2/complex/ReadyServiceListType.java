
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReadyServiceListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReadyServiceListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="readyService" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ReadyServiceType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReadyServiceListType", propOrder = {
    "readyService"
})
public class ReadyServiceListType {

    @XmlElement(required = true)
    protected ReadyServiceType readyService;

    /**
     * Gets the value of the readyService property.
     * 
     * @return
     *     possible object is
     *     {@link ReadyServiceType }
     *     
     */
    public ReadyServiceType getReadyService() {
        return readyService;
    }

    /**
     * Sets the value of the readyService property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadyServiceType }
     *     
     */
    public void setReadyService(ReadyServiceType value) {
        this.readyService = value;
    }

}
