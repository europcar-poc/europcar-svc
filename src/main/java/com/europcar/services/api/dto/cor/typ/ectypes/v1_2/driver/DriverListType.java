
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DriverListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="primaryDriver" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriverDTO" minOccurs="0"/&gt;
 *         &lt;element name="additionalDriver" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}AdditionalDriverDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverListType", propOrder = {
    "primaryDriver",
    "additionalDriver"
})
public class DriverListType {

    protected DriverDTO primaryDriver;
    @XmlElement(nillable = true)
    protected List<AdditionalDriverDTO> additionalDriver;

    /**
     * Gets the value of the primaryDriver property.
     * 
     * @return
     *     possible object is
     *     {@link DriverDTO }
     *     
     */
    public DriverDTO getPrimaryDriver() {
        return primaryDriver;
    }

    /**
     * Sets the value of the primaryDriver property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverDTO }
     *     
     */
    public void setPrimaryDriver(DriverDTO value) {
        this.primaryDriver = value;
    }

    /**
     * Gets the value of the additionalDriver property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalDriver property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalDriver().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalDriverDTO }
     * 
     * 
     */
    public List<AdditionalDriverDTO> getAdditionalDriver() {
        if (additionalDriver == null) {
            additionalDriver = new ArrayList<AdditionalDriverDTO>();
        }
        return this.additionalDriver;
    }

}
