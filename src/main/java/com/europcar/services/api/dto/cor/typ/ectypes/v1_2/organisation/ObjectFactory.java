
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.organisation;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.europcar.services.api.dto.cor.typ.ectypes.v1_2.organisation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.europcar.services.api.dto.cor.typ.ectypes.v1_2.organisation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OrganisationDTO }
     * 
     */
    public OrganisationDTO createOrganisationDTO() {
        return new OrganisationDTO();
    }

    /**
     * Create an instance of {@link OrganisationDTO.Contract }
     * 
     */
    public OrganisationDTO.Contract createOrganisationDTOContract() {
        return new OrganisationDTO.Contract();
    }

    /**
     * Create an instance of {@link OrganisationDTO.ReferenceList }
     * 
     */
    public OrganisationDTO.ReferenceList createOrganisationDTOReferenceList() {
        return new OrganisationDTO.ReferenceList();
    }

    /**
     * Create an instance of {@link OrganisationDTO.AddressList }
     * 
     */
    public OrganisationDTO.AddressList createOrganisationDTOAddressList() {
        return new OrganisationDTO.AddressList();
    }

}
