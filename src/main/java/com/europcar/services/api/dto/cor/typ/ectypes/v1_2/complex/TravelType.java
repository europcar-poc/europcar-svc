
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TravelType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TravelType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="checkOut" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}CheckOutInType" minOccurs="0"/&gt;
 *         &lt;element name="checkIn" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}CheckOutInType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TravelType", propOrder = {
    "checkOut",
    "checkIn"
})
public class TravelType {

    protected CheckOutInType checkOut;
    protected CheckOutInType checkIn;

    /**
     * Gets the value of the checkOut property.
     * 
     * @return
     *     possible object is
     *     {@link CheckOutInType }
     *     
     */
    public CheckOutInType getCheckOut() {
        return checkOut;
    }

    /**
     * Sets the value of the checkOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckOutInType }
     *     
     */
    public void setCheckOut(CheckOutInType value) {
        this.checkOut = value;
    }

    /**
     * Gets the value of the checkIn property.
     * 
     * @return
     *     possible object is
     *     {@link CheckOutInType }
     *     
     */
    public CheckOutInType getCheckIn() {
        return checkIn;
    }

    /**
     * Sets the value of the checkIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckOutInType }
     *     
     */
    public void setCheckIn(CheckOutInType value) {
        this.checkIn = value;
    }

}
