
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for F_BusinessAccountBilling complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="F_BusinessAccountBilling"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="creditDaysNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="selfBilling" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "F_BusinessAccountBilling")
public class FBusinessAccountBilling {

    @XmlAttribute(name = "creditDaysNbr")
    protected String creditDaysNbr;
    @XmlAttribute(name = "selfBilling")
    protected String selfBilling;

    /**
     * Gets the value of the creditDaysNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditDaysNbr() {
        return creditDaysNbr;
    }

    /**
     * Sets the value of the creditDaysNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditDaysNbr(String value) {
        this.creditDaysNbr = value;
    }

    /**
     * Gets the value of the selfBilling property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelfBilling() {
        return selfBilling;
    }

    /**
     * Sets the value of the selfBilling property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelfBilling(String value) {
        this.selfBilling = value;
    }

}
