
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.equip.EquipmentDTO;


/**
 * <p>Java class for EquipmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EquipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="selection" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="equipmentCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="equipmentName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="quantity" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="pricePerUnit" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="lossChargeUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="maximumAmountPerRental" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="chargePeriodStartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="equipmentDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="replacementCost" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="isCommunicationDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="communicationDevicePricePerUnit" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="numberOfUnitForChargePeriod" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="currencyConversionTable" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="isInProduct" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="revisionNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="typeLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentType")
@XmlSeeAlso({
    EquipmentDTO.class
})
public class EquipmentType {

    @XmlAttribute(name = "selection")
    protected String selection;
    @XmlAttribute(name = "equipmentCode")
    protected String equipmentCode;
    @XmlAttribute(name = "equipmentName")
    protected String equipmentName;
    @XmlAttribute(name = "quantity")
    protected String quantity;
    @XmlAttribute(name = "pricePerUnit")
    protected BigDecimal pricePerUnit;
    @XmlAttribute(name = "lossChargeUnit")
    protected String lossChargeUnit;
    @XmlAttribute(name = "chargeUnit")
    protected String chargeUnit;
    @XmlAttribute(name = "currencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "chargeType")
    protected String chargeType;
    @XmlAttribute(name = "maximumAmountPerRental")
    protected Double maximumAmountPerRental;
    @XmlAttribute(name = "chargePeriodStartDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar chargePeriodStartDate;
    @XmlAttribute(name = "amount")
    protected Double amount;
    @XmlAttribute(name = "equipmentDescription")
    protected String equipmentDescription;
    @XmlAttribute(name = "replacementCost")
    protected Double replacementCost;
    @XmlAttribute(name = "isCommunicationDevice")
    protected Boolean isCommunicationDevice;
    @XmlAttribute(name = "communicationDevicePricePerUnit")
    protected Double communicationDevicePricePerUnit;
    @XmlAttribute(name = "numberOfUnitForChargePeriod")
    protected BigInteger numberOfUnitForChargePeriod;
    @XmlAttribute(name = "currencyConversionTable")
    protected String currencyConversionTable;
    @XmlAttribute(name = "status")
    protected String status;
    @XmlAttribute(name = "isInProduct")
    protected String isInProduct;
    @XmlAttribute(name = "revisionNumber")
    protected BigInteger revisionNumber;
    @XmlAttribute(name = "typeLabel")
    protected String typeLabel;
    @XmlAttribute(name = "type")
    protected String type;

    /**
     * Gets the value of the selection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelection() {
        return selection;
    }

    /**
     * Sets the value of the selection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelection(String value) {
        this.selection = value;
    }

    /**
     * Gets the value of the equipmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentCode() {
        return equipmentCode;
    }

    /**
     * Sets the value of the equipmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentCode(String value) {
        this.equipmentCode = value;
    }

    /**
     * Gets the value of the equipmentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentName() {
        return equipmentName;
    }

    /**
     * Sets the value of the equipmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentName(String value) {
        this.equipmentName = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity(String value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the pricePerUnit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    /**
     * Sets the value of the pricePerUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPricePerUnit(BigDecimal value) {
        this.pricePerUnit = value;
    }

    /**
     * Gets the value of the lossChargeUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLossChargeUnit() {
        return lossChargeUnit;
    }

    /**
     * Sets the value of the lossChargeUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLossChargeUnit(String value) {
        this.lossChargeUnit = value;
    }

    /**
     * Gets the value of the chargeUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeUnit() {
        return chargeUnit;
    }

    /**
     * Sets the value of the chargeUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeUnit(String value) {
        this.chargeUnit = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the chargeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Sets the value of the chargeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeType(String value) {
        this.chargeType = value;
    }

    /**
     * Gets the value of the maximumAmountPerRental property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMaximumAmountPerRental() {
        return maximumAmountPerRental;
    }

    /**
     * Sets the value of the maximumAmountPerRental property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMaximumAmountPerRental(Double value) {
        this.maximumAmountPerRental = value;
    }

    /**
     * Gets the value of the chargePeriodStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChargePeriodStartDate() {
        return chargePeriodStartDate;
    }

    /**
     * Sets the value of the chargePeriodStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChargePeriodStartDate(XMLGregorianCalendar value) {
        this.chargePeriodStartDate = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAmount(Double value) {
        this.amount = value;
    }

    /**
     * Gets the value of the equipmentDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentDescription() {
        return equipmentDescription;
    }

    /**
     * Sets the value of the equipmentDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentDescription(String value) {
        this.equipmentDescription = value;
    }

    /**
     * Gets the value of the replacementCost property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getReplacementCost() {
        return replacementCost;
    }

    /**
     * Sets the value of the replacementCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setReplacementCost(Double value) {
        this.replacementCost = value;
    }

    /**
     * Gets the value of the isCommunicationDevice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCommunicationDevice() {
        return isCommunicationDevice;
    }

    /**
     * Sets the value of the isCommunicationDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCommunicationDevice(Boolean value) {
        this.isCommunicationDevice = value;
    }

    /**
     * Gets the value of the communicationDevicePricePerUnit property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCommunicationDevicePricePerUnit() {
        return communicationDevicePricePerUnit;
    }

    /**
     * Sets the value of the communicationDevicePricePerUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCommunicationDevicePricePerUnit(Double value) {
        this.communicationDevicePricePerUnit = value;
    }

    /**
     * Gets the value of the numberOfUnitForChargePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfUnitForChargePeriod() {
        return numberOfUnitForChargePeriod;
    }

    /**
     * Sets the value of the numberOfUnitForChargePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfUnitForChargePeriod(BigInteger value) {
        this.numberOfUnitForChargePeriod = value;
    }

    /**
     * Gets the value of the currencyConversionTable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyConversionTable() {
        return currencyConversionTable;
    }

    /**
     * Sets the value of the currencyConversionTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyConversionTable(String value) {
        this.currencyConversionTable = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the isInProduct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInProduct() {
        return isInProduct;
    }

    /**
     * Sets the value of the isInProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInProduct(String value) {
        this.isInProduct = value;
    }

    /**
     * Gets the value of the revisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRevisionNumber() {
        return revisionNumber;
    }

    /**
     * Sets the value of the revisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRevisionNumber(BigInteger value) {
        this.revisionNumber = value;
    }

    /**
     * Gets the value of the typeLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeLabel() {
        return typeLabel;
    }

    /**
     * Sets the value of the typeLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeLabel(String value) {
        this.typeLabel = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
