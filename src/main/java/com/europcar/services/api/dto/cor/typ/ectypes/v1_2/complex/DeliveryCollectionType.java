
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.simple.DelColType;


/**
 * <p>Java class for DeliveryCollectionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeliveryCollectionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="driverDistance" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DistanceType" minOccurs="0"/&gt;
 *         &lt;element name="driverGeoLocation" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="postal" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" minOccurs="0"/&gt;
 *                   &lt;element name="geographical" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}GeographicLocationType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="contactName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="contactPhone" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="chargeApplyCode" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}DelColType" /&gt;
 *                 &lt;attribute name="instructionLine1" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="instructionLine2" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="instructionLine3" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="instructionLine4" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="deliveryCollectionDatetime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="deliveryCollectionStation" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}CheckOutInLocationStringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="movementId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="delColID" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="vehicleReleaseMethod" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="changeOver" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="isReadyForCollection" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeliveryCollectionType", propOrder = {
    "driverDistance",
    "driverGeoLocation",
    "deliveryCollectionStation"
})
public class DeliveryCollectionType {

    protected DistanceType driverDistance;
    protected DeliveryCollectionType.DriverGeoLocation driverGeoLocation;
    protected CheckOutInLocationStringType deliveryCollectionStation;
    @XmlAttribute(name = "movementId")
    @XmlSchemaType(name = "anySimpleType")
    protected String movementId;
    @XmlAttribute(name = "delColID")
    protected BigInteger delColID;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "status")
    protected String status;
    @XmlAttribute(name = "vehicleReleaseMethod")
    protected String vehicleReleaseMethod;
    @XmlAttribute(name = "changeOver")
    protected Boolean changeOver;
    @XmlAttribute(name = "isReadyForCollection")
    protected Boolean isReadyForCollection;

    /**
     * Gets the value of the driverDistance property.
     * 
     * @return
     *     possible object is
     *     {@link DistanceType }
     *     
     */
    public DistanceType getDriverDistance() {
        return driverDistance;
    }

    /**
     * Sets the value of the driverDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link DistanceType }
     *     
     */
    public void setDriverDistance(DistanceType value) {
        this.driverDistance = value;
    }

    /**
     * Gets the value of the driverGeoLocation property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryCollectionType.DriverGeoLocation }
     *     
     */
    public DeliveryCollectionType.DriverGeoLocation getDriverGeoLocation() {
        return driverGeoLocation;
    }

    /**
     * Sets the value of the driverGeoLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryCollectionType.DriverGeoLocation }
     *     
     */
    public void setDriverGeoLocation(DeliveryCollectionType.DriverGeoLocation value) {
        this.driverGeoLocation = value;
    }

    /**
     * Gets the value of the deliveryCollectionStation property.
     * 
     * @return
     *     possible object is
     *     {@link CheckOutInLocationStringType }
     *     
     */
    public CheckOutInLocationStringType getDeliveryCollectionStation() {
        return deliveryCollectionStation;
    }

    /**
     * Sets the value of the deliveryCollectionStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckOutInLocationStringType }
     *     
     */
    public void setDeliveryCollectionStation(CheckOutInLocationStringType value) {
        this.deliveryCollectionStation = value;
    }

    /**
     * Gets the value of the movementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMovementId() {
        return movementId;
    }

    /**
     * Sets the value of the movementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMovementId(String value) {
        this.movementId = value;
    }

    /**
     * Gets the value of the delColID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDelColID() {
        return delColID;
    }

    /**
     * Sets the value of the delColID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDelColID(BigInteger value) {
        this.delColID = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the vehicleReleaseMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleReleaseMethod() {
        return vehicleReleaseMethod;
    }

    /**
     * Sets the value of the vehicleReleaseMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleReleaseMethod(String value) {
        this.vehicleReleaseMethod = value;
    }

    /**
     * Gets the value of the changeOver property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChangeOver() {
        return changeOver;
    }

    /**
     * Sets the value of the changeOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeOver(Boolean value) {
        this.changeOver = value;
    }

    /**
     * Gets the value of the isReadyForCollection property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadyForCollection() {
        return isReadyForCollection;
    }

    /**
     * Sets the value of the isReadyForCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadyForCollection(Boolean value) {
        this.isReadyForCollection = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="postal" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" minOccurs="0"/&gt;
     *         &lt;element name="geographical" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}GeographicLocationType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="contactName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="contactPhone" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="chargeApplyCode" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/simple}DelColType" /&gt;
     *       &lt;attribute name="instructionLine1" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="instructionLine2" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="instructionLine3" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="instructionLine4" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="deliveryCollectionDatetime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "postal",
        "geographical"
    })
    public static class DriverGeoLocation {

        protected AddressType postal;
        protected GeographicLocationType geographical;
        @XmlAttribute(name = "contactName")
        protected String contactName;
        @XmlAttribute(name = "contactPhone")
        protected String contactPhone;
        @XmlAttribute(name = "chargeApplyCode")
        protected DelColType chargeApplyCode;
        @XmlAttribute(name = "instructionLine1")
        protected String instructionLine1;
        @XmlAttribute(name = "instructionLine2")
        protected String instructionLine2;
        @XmlAttribute(name = "instructionLine3")
        protected String instructionLine3;
        @XmlAttribute(name = "instructionLine4")
        protected String instructionLine4;
        @XmlAttribute(name = "deliveryCollectionDatetime")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar deliveryCollectionDatetime;

        /**
         * Gets the value of the postal property.
         * 
         * @return
         *     possible object is
         *     {@link AddressType }
         *     
         */
        public AddressType getPostal() {
            return postal;
        }

        /**
         * Sets the value of the postal property.
         * 
         * @param value
         *     allowed object is
         *     {@link AddressType }
         *     
         */
        public void setPostal(AddressType value) {
            this.postal = value;
        }

        /**
         * Gets the value of the geographical property.
         * 
         * @return
         *     possible object is
         *     {@link GeographicLocationType }
         *     
         */
        public GeographicLocationType getGeographical() {
            return geographical;
        }

        /**
         * Sets the value of the geographical property.
         * 
         * @param value
         *     allowed object is
         *     {@link GeographicLocationType }
         *     
         */
        public void setGeographical(GeographicLocationType value) {
            this.geographical = value;
        }

        /**
         * Gets the value of the contactName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContactName() {
            return contactName;
        }

        /**
         * Sets the value of the contactName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContactName(String value) {
            this.contactName = value;
        }

        /**
         * Gets the value of the contactPhone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContactPhone() {
            return contactPhone;
        }

        /**
         * Sets the value of the contactPhone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContactPhone(String value) {
            this.contactPhone = value;
        }

        /**
         * Gets the value of the chargeApplyCode property.
         * 
         * @return
         *     possible object is
         *     {@link DelColType }
         *     
         */
        public DelColType getChargeApplyCode() {
            return chargeApplyCode;
        }

        /**
         * Sets the value of the chargeApplyCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link DelColType }
         *     
         */
        public void setChargeApplyCode(DelColType value) {
            this.chargeApplyCode = value;
        }

        /**
         * Gets the value of the instructionLine1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstructionLine1() {
            return instructionLine1;
        }

        /**
         * Sets the value of the instructionLine1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstructionLine1(String value) {
            this.instructionLine1 = value;
        }

        /**
         * Gets the value of the instructionLine2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstructionLine2() {
            return instructionLine2;
        }

        /**
         * Sets the value of the instructionLine2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstructionLine2(String value) {
            this.instructionLine2 = value;
        }

        /**
         * Gets the value of the instructionLine3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstructionLine3() {
            return instructionLine3;
        }

        /**
         * Sets the value of the instructionLine3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstructionLine3(String value) {
            this.instructionLine3 = value;
        }

        /**
         * Gets the value of the instructionLine4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstructionLine4() {
            return instructionLine4;
        }

        /**
         * Sets the value of the instructionLine4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstructionLine4(String value) {
            this.instructionLine4 = value;
        }

        /**
         * Gets the value of the deliveryCollectionDatetime property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDeliveryCollectionDatetime() {
            return deliveryCollectionDatetime;
        }

        /**
         * Sets the value of the deliveryCollectionDatetime property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDeliveryCollectionDatetime(XMLGregorianCalendar value) {
            this.deliveryCollectionDatetime = value;
        }

    }

}
