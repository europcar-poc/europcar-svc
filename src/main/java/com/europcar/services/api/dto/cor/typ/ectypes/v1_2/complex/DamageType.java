
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for damageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="damageType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="damageId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="damageTypeId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="damageSeverity" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="rentalAgreementNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="vehicleMovementNumber" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="creationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="creator" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="stationCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="companyProvider" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="numberOfPictureAssigned" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="externalDamageId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="isLinkedToTheRA" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "damageType")
public class DamageType {

    @XmlAttribute(name = "damageId")
    protected String damageId;
    @XmlAttribute(name = "damageTypeId")
    protected String damageTypeId;
    @XmlAttribute(name = "damageSeverity")
    protected BigInteger damageSeverity;
    @XmlAttribute(name = "rentalAgreementNumber")
    protected BigInteger rentalAgreementNumber;
    @XmlAttribute(name = "vehicleMovementNumber")
    protected Integer vehicleMovementNumber;
    @XmlAttribute(name = "creationDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationDateTime;
    @XmlAttribute(name = "creator")
    protected String creator;
    @XmlAttribute(name = "countryCode")
    protected String countryCode;
    @XmlAttribute(name = "stationCode")
    protected String stationCode;
    @XmlAttribute(name = "companyProvider")
    protected String companyProvider;
    @XmlAttribute(name = "numberOfPictureAssigned")
    protected Integer numberOfPictureAssigned;
    @XmlAttribute(name = "externalDamageId")
    protected String externalDamageId;
    @XmlAttribute(name = "isLinkedToTheRA")
    protected Boolean isLinkedToTheRA;

    /**
     * Gets the value of the damageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamageId() {
        return damageId;
    }

    /**
     * Sets the value of the damageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamageId(String value) {
        this.damageId = value;
    }

    /**
     * Gets the value of the damageTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamageTypeId() {
        return damageTypeId;
    }

    /**
     * Sets the value of the damageTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamageTypeId(String value) {
        this.damageTypeId = value;
    }

    /**
     * Gets the value of the damageSeverity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDamageSeverity() {
        return damageSeverity;
    }

    /**
     * Sets the value of the damageSeverity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDamageSeverity(BigInteger value) {
        this.damageSeverity = value;
    }

    /**
     * Gets the value of the rentalAgreementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRentalAgreementNumber() {
        return rentalAgreementNumber;
    }

    /**
     * Sets the value of the rentalAgreementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRentalAgreementNumber(BigInteger value) {
        this.rentalAgreementNumber = value;
    }

    /**
     * Gets the value of the vehicleMovementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVehicleMovementNumber() {
        return vehicleMovementNumber;
    }

    /**
     * Sets the value of the vehicleMovementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVehicleMovementNumber(Integer value) {
        this.vehicleMovementNumber = value;
    }

    /**
     * Gets the value of the creationDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDateTime() {
        return creationDateTime;
    }

    /**
     * Sets the value of the creationDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDateTime(XMLGregorianCalendar value) {
        this.creationDateTime = value;
    }

    /**
     * Gets the value of the creator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Sets the value of the creator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreator(String value) {
        this.creator = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the stationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationCode() {
        return stationCode;
    }

    /**
     * Sets the value of the stationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationCode(String value) {
        this.stationCode = value;
    }

    /**
     * Gets the value of the companyProvider property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyProvider() {
        return companyProvider;
    }

    /**
     * Sets the value of the companyProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyProvider(String value) {
        this.companyProvider = value;
    }

    /**
     * Gets the value of the numberOfPictureAssigned property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfPictureAssigned() {
        return numberOfPictureAssigned;
    }

    /**
     * Sets the value of the numberOfPictureAssigned property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfPictureAssigned(Integer value) {
        this.numberOfPictureAssigned = value;
    }

    /**
     * Gets the value of the externalDamageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalDamageId() {
        return externalDamageId;
    }

    /**
     * Sets the value of the externalDamageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalDamageId(String value) {
        this.externalDamageId = value;
    }

    /**
     * Gets the value of the isLinkedToTheRA property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLinkedToTheRA() {
        return isLinkedToTheRA;
    }

    /**
     * Sets the value of the isLinkedToTheRA property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLinkedToTheRA(Boolean value) {
        this.isLinkedToTheRA = value;
    }

}
