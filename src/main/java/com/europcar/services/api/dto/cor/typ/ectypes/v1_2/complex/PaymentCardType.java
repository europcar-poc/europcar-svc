
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentCardType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentCardType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="paymentCardId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="ranking" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="paymentCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="paymentCardExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="paymentCardHolderFirstName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="paymentCardHolderLastName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="paymentCardVerificationCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="paymentCardIssueNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentCardType", propOrder = {
    "token"
})
@XmlSeeAlso({
    BankCardPaymentType.class
})
public class PaymentCardType {

    @XmlElement(name = "Token")
    protected String token;
    @XmlAttribute(name = "paymentCardId")
    protected BigInteger paymentCardId;
    @XmlAttribute(name = "ranking")
    protected BigInteger ranking;
    @XmlAttribute(name = "paymentCardNumber")
    protected String paymentCardNumber;
    @XmlAttribute(name = "paymentCardExpiryDate")
    protected String paymentCardExpiryDate;
    @XmlAttribute(name = "paymentCardHolderFirstName")
    protected String paymentCardHolderFirstName;
    @XmlAttribute(name = "paymentCardHolderLastName")
    protected String paymentCardHolderLastName;
    @XmlAttribute(name = "paymentCardVerificationCode")
    protected String paymentCardVerificationCode;
    @XmlAttribute(name = "paymentCardIssueNumber")
    protected String paymentCardIssueNumber;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the paymentCardId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPaymentCardId() {
        return paymentCardId;
    }

    /**
     * Sets the value of the paymentCardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPaymentCardId(BigInteger value) {
        this.paymentCardId = value;
    }

    /**
     * Gets the value of the ranking property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRanking() {
        return ranking;
    }

    /**
     * Sets the value of the ranking property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRanking(BigInteger value) {
        this.ranking = value;
    }

    /**
     * Gets the value of the paymentCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCardNumber() {
        return paymentCardNumber;
    }

    /**
     * Sets the value of the paymentCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCardNumber(String value) {
        this.paymentCardNumber = value;
    }

    /**
     * Gets the value of the paymentCardExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCardExpiryDate() {
        return paymentCardExpiryDate;
    }

    /**
     * Sets the value of the paymentCardExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCardExpiryDate(String value) {
        this.paymentCardExpiryDate = value;
    }

    /**
     * Gets the value of the paymentCardHolderFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCardHolderFirstName() {
        return paymentCardHolderFirstName;
    }

    /**
     * Sets the value of the paymentCardHolderFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCardHolderFirstName(String value) {
        this.paymentCardHolderFirstName = value;
    }

    /**
     * Gets the value of the paymentCardHolderLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCardHolderLastName() {
        return paymentCardHolderLastName;
    }

    /**
     * Sets the value of the paymentCardHolderLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCardHolderLastName(String value) {
        this.paymentCardHolderLastName = value;
    }

    /**
     * Gets the value of the paymentCardVerificationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCardVerificationCode() {
        return paymentCardVerificationCode;
    }

    /**
     * Sets the value of the paymentCardVerificationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCardVerificationCode(String value) {
        this.paymentCardVerificationCode = value;
    }

    /**
     * Gets the value of the paymentCardIssueNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCardIssueNumber() {
        return paymentCardIssueNumber;
    }

    /**
     * Sets the value of the paymentCardIssueNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCardIssueNumber(String value) {
        this.paymentCardIssueNumber = value;
    }

}
