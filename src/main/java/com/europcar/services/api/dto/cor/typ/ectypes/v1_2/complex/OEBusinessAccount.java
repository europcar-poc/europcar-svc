
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OE_BusinessAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OE_BusinessAccount"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}F_BusinessAccountID" minOccurs="0"/&gt;
 *         &lt;element name="organisationBookOF" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}F_OrganisationRoleID" minOccurs="0"/&gt;
 *         &lt;element name="organisationHolder" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}F_OrganisationRoleID" minOccurs="0"/&gt;
 *         &lt;element name="billingData" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}F_BusinessAccountBilling" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="brand" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OE_BusinessAccount", propOrder = {
    "id",
    "organisationBookOF",
    "organisationHolder",
    "billingData"
})
public class OEBusinessAccount {

    protected FBusinessAccountID id;
    protected FOrganisationRoleID organisationBookOF;
    protected FOrganisationRoleID organisationHolder;
    protected FBusinessAccountBilling billingData;
    @XmlAttribute(name = "status")
    protected String status;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "Type")
    protected String type;
    @XmlAttribute(name = "brand")
    protected String brand;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link FBusinessAccountID }
     *     
     */
    public FBusinessAccountID getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link FBusinessAccountID }
     *     
     */
    public void setId(FBusinessAccountID value) {
        this.id = value;
    }

    /**
     * Gets the value of the organisationBookOF property.
     * 
     * @return
     *     possible object is
     *     {@link FOrganisationRoleID }
     *     
     */
    public FOrganisationRoleID getOrganisationBookOF() {
        return organisationBookOF;
    }

    /**
     * Sets the value of the organisationBookOF property.
     * 
     * @param value
     *     allowed object is
     *     {@link FOrganisationRoleID }
     *     
     */
    public void setOrganisationBookOF(FOrganisationRoleID value) {
        this.organisationBookOF = value;
    }

    /**
     * Gets the value of the organisationHolder property.
     * 
     * @return
     *     possible object is
     *     {@link FOrganisationRoleID }
     *     
     */
    public FOrganisationRoleID getOrganisationHolder() {
        return organisationHolder;
    }

    /**
     * Sets the value of the organisationHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link FOrganisationRoleID }
     *     
     */
    public void setOrganisationHolder(FOrganisationRoleID value) {
        this.organisationHolder = value;
    }

    /**
     * Gets the value of the billingData property.
     * 
     * @return
     *     possible object is
     *     {@link FBusinessAccountBilling }
     *     
     */
    public FBusinessAccountBilling getBillingData() {
        return billingData;
    }

    /**
     * Sets the value of the billingData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FBusinessAccountBilling }
     *     
     */
    public void setBillingData(FBusinessAccountBilling value) {
        this.billingData = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

}
