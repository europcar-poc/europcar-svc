
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeliveryCollectionType }
     * 
     */
    public DeliveryCollectionType createDeliveryCollectionType() {
        return new DeliveryCollectionType();
    }

    /**
     * Create an instance of {@link MovementType }
     * 
     */
    public MovementType createMovementType() {
        return new MovementType();
    }

    /**
     * Create an instance of {@link CouponCampaignType }
     * 
     */
    public CouponCampaignType createCouponCampaignType() {
        return new CouponCampaignType();
    }

    /**
     * Create an instance of {@link ContactEmailType }
     * 
     */
    public ContactEmailType createContactEmailType() {
        return new ContactEmailType();
    }

    /**
     * Create an instance of {@link CheckOutInType }
     * 
     */
    public CheckOutInType createCheckOutInType() {
        return new CheckOutInType();
    }

    /**
     * Create an instance of {@link RentalDataType }
     * 
     */
    public RentalDataType createRentalDataType() {
        return new RentalDataType();
    }

    /**
     * Create an instance of {@link RentalDataType.DriverList }
     * 
     */
    public RentalDataType.DriverList createRentalDataTypeDriverList() {
        return new RentalDataType.DriverList();
    }

    /**
     * Create an instance of {@link RateType }
     * 
     */
    public RateType createRateType() {
        return new RateType();
    }

    /**
     * Create an instance of {@link RateType.Extra }
     * 
     */
    public RateType.Extra createRateTypeExtra() {
        return new RateType.Extra();
    }

    /**
     * Create an instance of {@link ContactPhoneType }
     * 
     */
    public ContactPhoneType createContactPhoneType() {
        return new ContactPhoneType();
    }

    /**
     * Create an instance of {@link AdrType }
     * 
     */
    public AdrType createAdrType() {
        return new AdrType();
    }

    /**
     * Create an instance of {@link AdrType.PoliceList }
     * 
     */
    public AdrType.PoliceList createAdrTypePoliceList() {
        return new AdrType.PoliceList();
    }

    /**
     * Create an instance of {@link AdrType.PersonInjuredDetail }
     * 
     */
    public AdrType.PersonInjuredDetail createAdrTypePersonInjuredDetail() {
        return new AdrType.PersonInjuredDetail();
    }

    /**
     * Create an instance of {@link AdrType.PersonInjuredDetail.PersonInjuredList }
     * 
     */
    public AdrType.PersonInjuredDetail.PersonInjuredList createAdrTypePersonInjuredDetailPersonInjuredList() {
        return new AdrType.PersonInjuredDetail.PersonInjuredList();
    }

    /**
     * Create an instance of {@link AdrType.ThirdPartyDetail }
     * 
     */
    public AdrType.ThirdPartyDetail createAdrTypeThirdPartyDetail() {
        return new AdrType.ThirdPartyDetail();
    }

    /**
     * Create an instance of {@link AdrType.ThirdPartyDetail.ThirdPartyList }
     * 
     */
    public AdrType.ThirdPartyDetail.ThirdPartyList createAdrTypeThirdPartyDetailThirdPartyList() {
        return new AdrType.ThirdPartyDetail.ThirdPartyList();
    }

    /**
     * Create an instance of {@link AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty }
     * 
     */
    public AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty createAdrTypeThirdPartyDetailThirdPartyListThirdParty() {
        return new AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty();
    }

    /**
     * Create an instance of {@link AdrType.WitnessDetail }
     * 
     */
    public AdrType.WitnessDetail createAdrTypeWitnessDetail() {
        return new AdrType.WitnessDetail();
    }

    /**
     * Create an instance of {@link PaymentTransactionFrag }
     * 
     */
    public PaymentTransactionFrag createPaymentTransactionFrag() {
        return new PaymentTransactionFrag();
    }

    /**
     * Create an instance of {@link PaymentTransactionFrag.DCCexchanges }
     * 
     */
    public PaymentTransactionFrag.DCCexchanges createPaymentTransactionFragDCCexchanges() {
        return new PaymentTransactionFrag.DCCexchanges();
    }

    /**
     * Create an instance of {@link DamageDetailType }
     * 
     */
    public DamageDetailType createDamageDetailType() {
        return new DamageDetailType();
    }

    /**
     * Create an instance of {@link DamageDetailType.ADRList }
     * 
     */
    public DamageDetailType.ADRList createDamageDetailTypeADRList() {
        return new DamageDetailType.ADRList();
    }

    /**
     * Create an instance of {@link PaymentMOPFrag }
     * 
     */
    public PaymentMOPFrag createPaymentMOPFrag() {
        return new PaymentMOPFrag();
    }

    /**
     * Create an instance of {@link AccountType }
     * 
     */
    public AccountType createAccountType() {
        return new AccountType();
    }

    /**
     * Create an instance of {@link ProfileType }
     * 
     */
    public ProfileType createProfileType() {
        return new ProfileType();
    }

    /**
     * Create an instance of {@link RequestContextType }
     * 
     */
    public RequestContextType createRequestContextType() {
        return new RequestContextType();
    }

    /**
     * Create an instance of {@link TechnicalContextType }
     * 
     */
    public TechnicalContextType createTechnicalContextType() {
        return new TechnicalContextType();
    }

    /**
     * Create an instance of {@link ITSystemType }
     * 
     */
    public ITSystemType createITSystemType() {
        return new ITSystemType();
    }

    /**
     * Create an instance of {@link VersionType }
     * 
     */
    public VersionType createVersionType() {
        return new VersionType();
    }

    /**
     * Create an instance of {@link UserType }
     * 
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link BusinessContextType }
     * 
     */
    public BusinessContextType createBusinessContextType() {
        return new BusinessContextType();
    }

    /**
     * Create an instance of {@link LocalisationType }
     * 
     */
    public LocalisationType createLocalisationType() {
        return new LocalisationType();
    }

    /**
     * Create an instance of {@link ConventionType }
     * 
     */
    public ConventionType createConventionType() {
        return new ConventionType();
    }

    /**
     * Create an instance of {@link LanguageType }
     * 
     */
    public LanguageType createLanguageType() {
        return new LanguageType();
    }

    /**
     * Create an instance of {@link CurrencyType }
     * 
     */
    public CurrencyType createCurrencyType() {
        return new CurrencyType();
    }

    /**
     * Create an instance of {@link ChannelType }
     * 
     */
    public ChannelType createChannelType() {
        return new ChannelType();
    }

    /**
     * Create an instance of {@link BookingChannelType }
     * 
     */
    public BookingChannelType createBookingChannelType() {
        return new BookingChannelType();
    }

    /**
     * Create an instance of {@link BrandType }
     * 
     */
    public BrandType createBrandType() {
        return new BrandType();
    }

    /**
     * Create an instance of {@link RequestSenderType }
     * 
     */
    public RequestSenderType createRequestSenderType() {
        return new RequestSenderType();
    }

    /**
     * Create an instance of {@link ActivityContextType }
     * 
     */
    public ActivityContextType createActivityContextType() {
        return new ActivityContextType();
    }

    /**
     * Create an instance of {@link ApproachPointListType }
     * 
     */
    public ApproachPointListType createApproachPointListType() {
        return new ApproachPointListType();
    }

    /**
     * Create an instance of {@link ApproachPointType }
     * 
     */
    public ApproachPointType createApproachPointType() {
        return new ApproachPointType();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link EmailListType }
     * 
     */
    public EmailListType createEmailListType() {
        return new EmailListType();
    }

    /**
     * Create an instance of {@link EmailType }
     * 
     */
    public EmailType createEmailType() {
        return new EmailType();
    }

    /**
     * Create an instance of {@link PhoneListType }
     * 
     */
    public PhoneListType createPhoneListType() {
        return new PhoneListType();
    }

    /**
     * Create an instance of {@link PhoneType }
     * 
     */
    public PhoneType createPhoneType() {
        return new PhoneType();
    }

    /**
     * Create an instance of {@link DocumentType }
     * 
     */
    public DocumentType createDocumentType() {
        return new DocumentType();
    }

    /**
     * Create an instance of {@link LicenseType }
     * 
     */
    public LicenseType createLicenseType() {
        return new LicenseType();
    }

    /**
     * Create an instance of {@link ReadyServiceListType }
     * 
     */
    public ReadyServiceListType createReadyServiceListType() {
        return new ReadyServiceListType();
    }

    /**
     * Create an instance of {@link ReadyServiceType }
     * 
     */
    public ReadyServiceType createReadyServiceType() {
        return new ReadyServiceType();
    }

    /**
     * Create an instance of {@link ServiceLevelType }
     * 
     */
    public ServiceLevelType createServiceLevelType() {
        return new ServiceLevelType();
    }

    /**
     * Create an instance of {@link ServiceLevelInsurancesType }
     * 
     */
    public ServiceLevelInsurancesType createServiceLevelInsurancesType() {
        return new ServiceLevelInsurancesType();
    }

    /**
     * Create an instance of {@link OrganisationExternalReferenceType }
     * 
     */
    public OrganisationExternalReferenceType createOrganisationExternalReferenceType() {
        return new OrganisationExternalReferenceType();
    }

    /**
     * Create an instance of {@link ExternalReferenceType }
     * 
     */
    public ExternalReferenceType createExternalReferenceType() {
        return new ExternalReferenceType();
    }

    /**
     * Create an instance of {@link TrackingType }
     * 
     */
    public TrackingType createTrackingType() {
        return new TrackingType();
    }

    /**
     * Create an instance of {@link CardType }
     * 
     */
    public CardType createCardType() {
        return new CardType();
    }

    /**
     * Create an instance of {@link CardCategoryType }
     * 
     */
    public CardCategoryType createCardCategoryType() {
        return new CardCategoryType();
    }

    /**
     * Create an instance of {@link IdCardType }
     * 
     */
    public IdCardType createIdCardType() {
        return new IdCardType();
    }

    /**
     * Create an instance of {@link ChargeCardType }
     * 
     */
    public ChargeCardType createChargeCardType() {
        return new ChargeCardType();
    }

    /**
     * Create an instance of {@link ProgramType }
     * 
     */
    public ProgramType createProgramType() {
        return new ProgramType();
    }

    /**
     * Create an instance of {@link MemberType }
     * 
     */
    public MemberType createMemberType() {
        return new MemberType();
    }

    /**
     * Create an instance of {@link CounterType }
     * 
     */
    public CounterType createCounterType() {
        return new CounterType();
    }

    /**
     * Create an instance of {@link AccountHistoryType }
     * 
     */
    public AccountHistoryType createAccountHistoryType() {
        return new AccountHistoryType();
    }

    /**
     * Create an instance of {@link UpdateRecordType }
     * 
     */
    public UpdateRecordType createUpdateRecordType() {
        return new UpdateRecordType();
    }

    /**
     * Create an instance of {@link ErrorMessageListType }
     * 
     */
    public ErrorMessageListType createErrorMessageListType() {
        return new ErrorMessageListType();
    }

    /**
     * Create an instance of {@link ErrorMessageType }
     * 
     */
    public ErrorMessageType createErrorMessageType() {
        return new ErrorMessageType();
    }

    /**
     * Create an instance of {@link MessageType }
     * 
     */
    public MessageType createMessageType() {
        return new MessageType();
    }

    /**
     * Create an instance of {@link WarningMessageType }
     * 
     */
    public WarningMessageType createWarningMessageType() {
        return new WarningMessageType();
    }

    /**
     * Create an instance of {@link ResponseContextType }
     * 
     */
    public ResponseContextType createResponseContextType() {
        return new ResponseContextType();
    }

    /**
     * Create an instance of {@link WarningMessageListType }
     * 
     */
    public WarningMessageListType createWarningMessageListType() {
        return new WarningMessageListType();
    }

    /**
     * Create an instance of {@link FOrganisationRoleID }
     * 
     */
    public FOrganisationRoleID createFOrganisationRoleID() {
        return new FOrganisationRoleID();
    }

    /**
     * Create an instance of {@link FOrganisationID }
     * 
     */
    public FOrganisationID createFOrganisationID() {
        return new FOrganisationID();
    }

    /**
     * Create an instance of {@link FBusinessAccountBilling }
     * 
     */
    public FBusinessAccountBilling createFBusinessAccountBilling() {
        return new FBusinessAccountBilling();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link InfosType }
     * 
     */
    public InfosType createInfosType() {
        return new InfosType();
    }

    /**
     * Create an instance of {@link PaymentDetailsFrag }
     * 
     */
    public PaymentDetailsFrag createPaymentDetailsFrag() {
        return new PaymentDetailsFrag();
    }

    /**
     * Create an instance of {@link PaymentRentalFrag }
     * 
     */
    public PaymentRentalFrag createPaymentRentalFrag() {
        return new PaymentRentalFrag();
    }

    /**
     * Create an instance of {@link GwyTimeZoneType }
     * 
     */
    public GwyTimeZoneType createGwyTimeZoneType() {
        return new GwyTimeZoneType();
    }

    /**
     * Create an instance of {@link PaymentAmountType }
     * 
     */
    public PaymentAmountType createPaymentAmountType() {
        return new PaymentAmountType();
    }

    /**
     * Create an instance of {@link PaymentMeanFrag }
     * 
     */
    public PaymentMeanFrag createPaymentMeanFrag() {
        return new PaymentMeanFrag();
    }

    /**
     * Create an instance of {@link FBusinessAccountID }
     * 
     */
    public FBusinessAccountID createFBusinessAccountID() {
        return new FBusinessAccountID();
    }

    /**
     * Create an instance of {@link PaymentIdFrag }
     * 
     */
    public PaymentIdFrag createPaymentIdFrag() {
        return new PaymentIdFrag();
    }

    /**
     * Create an instance of {@link OEBusinessAccount }
     * 
     */
    public OEBusinessAccount createOEBusinessAccount() {
        return new OEBusinessAccount();
    }

    /**
     * Create an instance of {@link FDriverID }
     * 
     */
    public FDriverID createFDriverID() {
        return new FDriverID();
    }

    /**
     * Create an instance of {@link TerminalDetailType }
     * 
     */
    public TerminalDetailType createTerminalDetailType() {
        return new TerminalDetailType();
    }

    /**
     * Create an instance of {@link Note }
     * 
     */
    public Note createNote() {
        return new Note();
    }

    /**
     * Create an instance of {@link BankCardPaymentType }
     * 
     */
    public BankCardPaymentType createBankCardPaymentType() {
        return new BankCardPaymentType();
    }

    /**
     * Create an instance of {@link PaymentCardType }
     * 
     */
    public PaymentCardType createPaymentCardType() {
        return new PaymentCardType();
    }

    /**
     * Create an instance of {@link ChequePaymentType }
     * 
     */
    public ChequePaymentType createChequePaymentType() {
        return new ChequePaymentType();
    }

    /**
     * Create an instance of {@link VoucherPaymentType }
     * 
     */
    public VoucherPaymentType createVoucherPaymentType() {
        return new VoucherPaymentType();
    }

    /**
     * Create an instance of {@link ChargeCardPaymentType }
     * 
     */
    public ChargeCardPaymentType createChargeCardPaymentType() {
        return new ChargeCardPaymentType();
    }

    /**
     * Create an instance of {@link ContactPhoneListType }
     * 
     */
    public ContactPhoneListType createContactPhoneListType() {
        return new ContactPhoneListType();
    }

    /**
     * Create an instance of {@link EquipmentType }
     * 
     */
    public EquipmentType createEquipmentType() {
        return new EquipmentType();
    }

    /**
     * Create an instance of {@link InsuranceType }
     * 
     */
    public InsuranceType createInsuranceType() {
        return new InsuranceType();
    }

    /**
     * Create an instance of {@link SurchargeType }
     * 
     */
    public SurchargeType createSurchargeType() {
        return new SurchargeType();
    }

    /**
     * Create an instance of {@link TravelType }
     * 
     */
    public TravelType createTravelType() {
        return new TravelType();
    }

    /**
     * Create an instance of {@link DistanceType }
     * 
     */
    public DistanceType createDistanceType() {
        return new DistanceType();
    }

    /**
     * Create an instance of {@link GeographicLocationType }
     * 
     */
    public GeographicLocationType createGeographicLocationType() {
        return new GeographicLocationType();
    }

    /**
     * Create an instance of {@link CheckOutInLocationStringType }
     * 
     */
    public CheckOutInLocationStringType createCheckOutInLocationStringType() {
        return new CheckOutInLocationStringType();
    }

    /**
     * Create an instance of {@link VehicleCategoryType }
     * 
     */
    public VehicleCategoryType createVehicleCategoryType() {
        return new VehicleCategoryType();
    }

    /**
     * Create an instance of {@link DriverType }
     * 
     */
    public DriverType createDriverType() {
        return new DriverType();
    }

    /**
     * Create an instance of {@link MeanOfPaymentDetailsType }
     * 
     */
    public MeanOfPaymentDetailsType createMeanOfPaymentDetailsType() {
        return new MeanOfPaymentDetailsType();
    }

    /**
     * Create an instance of {@link BHPOPaymentType }
     * 
     */
    public BHPOPaymentType createBHPOPaymentType() {
        return new BHPOPaymentType();
    }

    /**
     * Create an instance of {@link CommunicationDeviceType }
     * 
     */
    public CommunicationDeviceType createCommunicationDeviceType() {
        return new CommunicationDeviceType();
    }

    /**
     * Create an instance of {@link CardListType }
     * 
     */
    public CardListType createCardListType() {
        return new CardListType();
    }

    /**
     * Create an instance of {@link LoyaltyProgramMembershipListType }
     * 
     */
    public LoyaltyProgramMembershipListType createLoyaltyProgramMembershipListType() {
        return new LoyaltyProgramMembershipListType();
    }

    /**
     * Create an instance of {@link LoyaltyProgramType }
     * 
     */
    public LoyaltyProgramType createLoyaltyProgramType() {
        return new LoyaltyProgramType();
    }

    /**
     * Create an instance of {@link CryptObjectType }
     * 
     */
    public CryptObjectType createCryptObjectType() {
        return new CryptObjectType();
    }

    /**
     * Create an instance of {@link PeriodType }
     * 
     */
    public PeriodType createPeriodType() {
        return new PeriodType();
    }

    /**
     * Create an instance of {@link TechnicalFault }
     * 
     */
    public TechnicalFault createTechnicalFault() {
        return new TechnicalFault();
    }

    /**
     * Create an instance of {@link ContactEmailListType }
     * 
     */
    public ContactEmailListType createContactEmailListType() {
        return new ContactEmailListType();
    }

    /**
     * Create an instance of {@link VehiclePatternListType }
     * 
     */
    public VehiclePatternListType createVehiclePatternListType() {
        return new VehiclePatternListType();
    }

    /**
     * Create an instance of {@link DetailsType }
     * 
     */
    public DetailsType createDetailsType() {
        return new DetailsType();
    }

    /**
     * Create an instance of {@link PartnerLoyaltyProgramMembershipType }
     * 
     */
    public PartnerLoyaltyProgramMembershipType createPartnerLoyaltyProgramMembershipType() {
        return new PartnerLoyaltyProgramMembershipType();
    }

    /**
     * Create an instance of {@link MembershipType }
     * 
     */
    public MembershipType createMembershipType() {
        return new MembershipType();
    }

    /**
     * Create an instance of {@link DamageType }
     * 
     */
    public DamageType createDamageType() {
        return new DamageType();
    }

    /**
     * Create an instance of {@link ExternalReferenceListType }
     * 
     */
    public ExternalReferenceListType createExternalReferenceListType() {
        return new ExternalReferenceListType();
    }

    /**
     * Create an instance of {@link MarketSegmentType }
     * 
     */
    public MarketSegmentType createMarketSegmentType() {
        return new MarketSegmentType();
    }

    /**
     * Create an instance of {@link CheckOutInLocationType }
     * 
     */
    public CheckOutInLocationType createCheckOutInLocationType() {
        return new CheckOutInLocationType();
    }

    /**
     * Create an instance of {@link CouponType }
     * 
     */
    public CouponType createCouponType() {
        return new CouponType();
    }

    /**
     * Create an instance of {@link PartnerLoyaltyProgramMembershipListType }
     * 
     */
    public PartnerLoyaltyProgramMembershipListType createPartnerLoyaltyProgramMembershipListType() {
        return new PartnerLoyaltyProgramMembershipListType();
    }

    /**
     * Create an instance of {@link ArchiveType }
     * 
     */
    public ArchiveType createArchiveType() {
        return new ArchiveType();
    }

    /**
     * Create an instance of {@link VehicleModelType }
     * 
     */
    public VehicleModelType createVehicleModelType() {
        return new VehicleModelType();
    }

    /**
     * Create an instance of {@link Notes }
     * 
     */
    public Notes createNotes() {
        return new Notes();
    }

    /**
     * Create an instance of {@link VehicleType }
     * 
     */
    public VehicleType createVehicleType() {
        return new VehicleType();
    }

    /**
     * Create an instance of {@link ProductType }
     * 
     */
    public ProductType createProductType() {
        return new ProductType();
    }

    /**
     * Create an instance of {@link ContractualConditionsType }
     * 
     */
    public ContractualConditionsType createContractualConditionsType() {
        return new ContractualConditionsType();
    }

    /**
     * Create an instance of {@link CouponLinkType }
     * 
     */
    public CouponLinkType createCouponLinkType() {
        return new CouponLinkType();
    }

    /**
     * Create an instance of {@link DefectType }
     * 
     */
    public DefectType createDefectType() {
        return new DefectType();
    }

    /**
     * Create an instance of {@link ChargeLineType }
     * 
     */
    public ChargeLineType createChargeLineType() {
        return new ChargeLineType();
    }

    /**
     * Create an instance of {@link DeliveryCollectionType.DriverGeoLocation }
     * 
     */
    public DeliveryCollectionType.DriverGeoLocation createDeliveryCollectionTypeDriverGeoLocation() {
        return new DeliveryCollectionType.DriverGeoLocation();
    }

    /**
     * Create an instance of {@link MovementType.Distance }
     * 
     */
    public MovementType.Distance createMovementTypeDistance() {
        return new MovementType.Distance();
    }

    /**
     * Create an instance of {@link CouponCampaignType.Brands }
     * 
     */
    public CouponCampaignType.Brands createCouponCampaignTypeBrands() {
        return new CouponCampaignType.Brands();
    }

    /**
     * Create an instance of {@link CouponCampaignType.CheckoutCountries }
     * 
     */
    public CouponCampaignType.CheckoutCountries createCouponCampaignTypeCheckoutCountries() {
        return new CouponCampaignType.CheckoutCountries();
    }

    /**
     * Create an instance of {@link ContactEmailType.EmailPreferences }
     * 
     */
    public ContactEmailType.EmailPreferences createContactEmailTypeEmailPreferences() {
        return new ContactEmailType.EmailPreferences();
    }

    /**
     * Create an instance of {@link CheckOutInType.DriverGeoLocation }
     * 
     */
    public CheckOutInType.DriverGeoLocation createCheckOutInTypeDriverGeoLocation() {
        return new CheckOutInType.DriverGeoLocation();
    }

    /**
     * Create an instance of {@link RentalDataType.Reservation }
     * 
     */
    public RentalDataType.Reservation createRentalDataTypeReservation() {
        return new RentalDataType.Reservation();
    }

    /**
     * Create an instance of {@link RentalDataType.RentalAgreement }
     * 
     */
    public RentalDataType.RentalAgreement createRentalDataTypeRentalAgreement() {
        return new RentalDataType.RentalAgreement();
    }

    /**
     * Create an instance of {@link RentalDataType.MeanOfPaymentList }
     * 
     */
    public RentalDataType.MeanOfPaymentList createRentalDataTypeMeanOfPaymentList() {
        return new RentalDataType.MeanOfPaymentList();
    }

    /**
     * Create an instance of {@link RentalDataType.DriverList.Driver }
     * 
     */
    public RentalDataType.DriverList.Driver createRentalDataTypeDriverListDriver() {
        return new RentalDataType.DriverList.Driver();
    }

    /**
     * Create an instance of {@link RateType.DurationBand }
     * 
     */
    public RateType.DurationBand createRateTypeDurationBand() {
        return new RateType.DurationBand();
    }

    /**
     * Create an instance of {@link RateType.EquipmentList }
     * 
     */
    public RateType.EquipmentList createRateTypeEquipmentList() {
        return new RateType.EquipmentList();
    }

    /**
     * Create an instance of {@link RateType.InsuranceList }
     * 
     */
    public RateType.InsuranceList createRateTypeInsuranceList() {
        return new RateType.InsuranceList();
    }

    /**
     * Create an instance of {@link RateType.SurchargeList }
     * 
     */
    public RateType.SurchargeList createRateTypeSurchargeList() {
        return new RateType.SurchargeList();
    }

    /**
     * Create an instance of {@link RateType.MileAge }
     * 
     */
    public RateType.MileAge createRateTypeMileAge() {
        return new RateType.MileAge();
    }

    /**
     * Create an instance of {@link RateType.Upgrade }
     * 
     */
    public RateType.Upgrade createRateTypeUpgrade() {
        return new RateType.Upgrade();
    }

    /**
     * Create an instance of {@link RateType.AfterHours }
     * 
     */
    public RateType.AfterHours createRateTypeAfterHours() {
        return new RateType.AfterHours();
    }

    /**
     * Create an instance of {@link RateType.Penalty }
     * 
     */
    public RateType.Penalty createRateTypePenalty() {
        return new RateType.Penalty();
    }

    /**
     * Create an instance of {@link RateType.OneWay }
     * 
     */
    public RateType.OneWay createRateTypeOneWay() {
        return new RateType.OneWay();
    }

    /**
     * Create an instance of {@link RateType.Allowance }
     * 
     */
    public RateType.Allowance createRateTypeAllowance() {
        return new RateType.Allowance();
    }

    /**
     * Create an instance of {@link RateType.Discount }
     * 
     */
    public RateType.Discount createRateTypeDiscount() {
        return new RateType.Discount();
    }

    /**
     * Create an instance of {@link RateType.Extra.ExtraMileAge }
     * 
     */
    public RateType.Extra.ExtraMileAge createRateTypeExtraExtraMileAge() {
        return new RateType.Extra.ExtraMileAge();
    }

    /**
     * Create an instance of {@link RateType.Extra.ExtraDay }
     * 
     */
    public RateType.Extra.ExtraDay createRateTypeExtraExtraDay() {
        return new RateType.Extra.ExtraDay();
    }

    /**
     * Create an instance of {@link ContactPhoneType.PhonePreferences }
     * 
     */
    public ContactPhoneType.PhonePreferences createContactPhoneTypePhonePreferences() {
        return new ContactPhoneType.PhonePreferences();
    }

    /**
     * Create an instance of {@link AdrType.Document }
     * 
     */
    public AdrType.Document createAdrTypeDocument() {
        return new AdrType.Document();
    }

    /**
     * Create an instance of {@link AdrType.Vehicle }
     * 
     */
    public AdrType.Vehicle createAdrTypeVehicle() {
        return new AdrType.Vehicle();
    }

    /**
     * Create an instance of {@link AdrType.PoliceList.Police }
     * 
     */
    public AdrType.PoliceList.Police createAdrTypePoliceListPolice() {
        return new AdrType.PoliceList.Police();
    }

    /**
     * Create an instance of {@link AdrType.PersonInjuredDetail.PersonInjuredList.PersonInjured }
     * 
     */
    public AdrType.PersonInjuredDetail.PersonInjuredList.PersonInjured createAdrTypePersonInjuredDetailPersonInjuredListPersonInjured() {
        return new AdrType.PersonInjuredDetail.PersonInjuredList.PersonInjured();
    }

    /**
     * Create an instance of {@link AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty.Vehicle }
     * 
     */
    public AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty.Vehicle createAdrTypeThirdPartyDetailThirdPartyListThirdPartyVehicle() {
        return new AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty.Vehicle();
    }

    /**
     * Create an instance of {@link AdrType.WitnessDetail.WitnessList }
     * 
     */
    public AdrType.WitnessDetail.WitnessList createAdrTypeWitnessDetailWitnessList() {
        return new AdrType.WitnessDetail.WitnessList();
    }

    /**
     * Create an instance of {@link PaymentTransactionFrag.Authorization }
     * 
     */
    public PaymentTransactionFrag.Authorization createPaymentTransactionFragAuthorization() {
        return new PaymentTransactionFrag.Authorization();
    }

    /**
     * Create an instance of {@link PaymentTransactionFrag.Amounts }
     * 
     */
    public PaymentTransactionFrag.Amounts createPaymentTransactionFragAmounts() {
        return new PaymentTransactionFrag.Amounts();
    }

    /**
     * Create an instance of {@link PaymentTransactionFrag.BillingTreatment }
     * 
     */
    public PaymentTransactionFrag.BillingTreatment createPaymentTransactionFragBillingTreatment() {
        return new PaymentTransactionFrag.BillingTreatment();
    }

    /**
     * Create an instance of {@link PaymentTransactionFrag.DCCexchanges.Exchange }
     * 
     */
    public PaymentTransactionFrag.DCCexchanges.Exchange createPaymentTransactionFragDCCexchangesExchange() {
        return new PaymentTransactionFrag.DCCexchanges.Exchange();
    }

    /**
     * Create an instance of {@link DamageDetailType.Qualification }
     * 
     */
    public DamageDetailType.Qualification createDamageDetailTypeQualification() {
        return new DamageDetailType.Qualification();
    }

    /**
     * Create an instance of {@link DamageDetailType.Provider }
     * 
     */
    public DamageDetailType.Provider createDamageDetailTypeProvider() {
        return new DamageDetailType.Provider();
    }

    /**
     * Create an instance of {@link DamageDetailType.Repairing }
     * 
     */
    public DamageDetailType.Repairing createDamageDetailTypeRepairing() {
        return new DamageDetailType.Repairing();
    }

    /**
     * Create an instance of {@link DamageDetailType.ADRList.ADR }
     * 
     */
    public DamageDetailType.ADRList.ADR createDamageDetailTypeADRListADR() {
        return new DamageDetailType.ADRList.ADR();
    }

    /**
     * Create an instance of {@link PaymentMOPFrag.Details }
     * 
     */
    public PaymentMOPFrag.Details createPaymentMOPFragDetails() {
        return new PaymentMOPFrag.Details();
    }

    /**
     * Create an instance of {@link AccountType.RequestedCardCategory }
     * 
     */
    public AccountType.RequestedCardCategory createAccountTypeRequestedCardCategory() {
        return new AccountType.RequestedCardCategory();
    }

    /**
     * Create an instance of {@link ProfileType.Right }
     * 
     */
    public ProfileType.Right createProfileTypeRight() {
        return new ProfileType.Right();
    }

}
