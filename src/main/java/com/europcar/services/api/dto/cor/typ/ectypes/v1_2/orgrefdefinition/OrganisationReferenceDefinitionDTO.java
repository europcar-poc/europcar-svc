
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.orgrefdefinition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ExternalReferenceType;


/**
 * <p>Java class for OrganisationReferenceDefinitionDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrganisationReferenceDefinitionDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="referenceDefinition" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ExternalReferenceType"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="organisationId" use="required" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationReferenceDefinitionDTO", propOrder = {
    "referenceDefinition"
})
public class OrganisationReferenceDefinitionDTO {

    @XmlElement(required = true)
    protected ExternalReferenceType referenceDefinition;
    @XmlAttribute(name = "organisationId", required = true)
    protected int organisationId;

    /**
     * Gets the value of the referenceDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalReferenceType }
     *     
     */
    public ExternalReferenceType getReferenceDefinition() {
        return referenceDefinition;
    }

    /**
     * Sets the value of the referenceDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalReferenceType }
     *     
     */
    public void setReferenceDefinition(ExternalReferenceType value) {
        this.referenceDefinition = value;
    }

    /**
     * Gets the value of the organisationId property.
     * 
     */
    public int getOrganisationId() {
        return organisationId;
    }

    /**
     * Sets the value of the organisationId property.
     * 
     */
    public void setOrganisationId(int value) {
        this.organisationId = value;
    }

}
