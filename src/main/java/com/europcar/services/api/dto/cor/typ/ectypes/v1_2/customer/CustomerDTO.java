
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.customer;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.card.CardDTO;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver.DriverDTO;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.membership.MembershipDTO;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.membershiptransaction.MembershipTransactionDTO;


/**
 * <p>Java class for CustomerDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="driver" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriverDTO" minOccurs="0"/&gt;
 *         &lt;element name="card" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/card}CardDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="membership" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/membership}MembershipDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="membershipTransaction" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/membershiptransaction}MembershipTransactionDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDTO", propOrder = {
    "driver",
    "card",
    "membership",
    "membershipTransaction"
})
public class CustomerDTO {

    protected DriverDTO driver;
    @XmlElement(nillable = true)
    protected List<CardDTO> card;
    @XmlElement(nillable = true)
    protected List<MembershipDTO> membership;
    @XmlElement(nillable = true)
    protected List<MembershipTransactionDTO> membershipTransaction;

    /**
     * Gets the value of the driver property.
     * 
     * @return
     *     possible object is
     *     {@link DriverDTO }
     *     
     */
    public DriverDTO getDriver() {
        return driver;
    }

    /**
     * Sets the value of the driver property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverDTO }
     *     
     */
    public void setDriver(DriverDTO value) {
        this.driver = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the card property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCard().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CardDTO }
     * 
     * 
     */
    public List<CardDTO> getCard() {
        if (card == null) {
            card = new ArrayList<CardDTO>();
        }
        return this.card;
    }

    /**
     * Gets the value of the membership property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the membership property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMembership().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MembershipDTO }
     * 
     * 
     */
    public List<MembershipDTO> getMembership() {
        if (membership == null) {
            membership = new ArrayList<MembershipDTO>();
        }
        return this.membership;
    }

    /**
     * Gets the value of the membershipTransaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the membershipTransaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMembershipTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MembershipTransactionDTO }
     * 
     * 
     */
    public List<MembershipTransactionDTO> getMembershipTransaction() {
        if (membershipTransaction == null) {
            membershipTransaction = new ArrayList<MembershipTransactionDTO>();
        }
        return this.membershipTransaction;
    }

}
