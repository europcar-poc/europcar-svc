
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MailingListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MailingListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mailing" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}MailingType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MailingListType", propOrder = {
    "mailing"
})
public class MailingListType {

    @XmlElement(required = true)
    protected List<MailingType> mailing;

    /**
     * Gets the value of the mailing property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mailing property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMailing().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MailingType }
     * 
     * 
     */
    public List<MailingType> getMailing() {
        if (mailing == null) {
            mailing = new ArrayList<MailingType>();
        }
        return this.mailing;
    }

}
