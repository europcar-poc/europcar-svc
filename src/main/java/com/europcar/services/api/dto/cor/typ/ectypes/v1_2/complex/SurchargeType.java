
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SurchargeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SurchargeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="surchargeCode" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="surchargeName" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="surchargeDescription" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="currencyConversionTable" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargePeriodStartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="numberOfUnitForChargePeriod" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="surchargeAmount" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SurchargeType")
public class SurchargeType {

    @XmlAttribute(name = "surchargeCode")
    @XmlSchemaType(name = "anySimpleType")
    protected String surchargeCode;
    @XmlAttribute(name = "surchargeName")
    @XmlSchemaType(name = "anySimpleType")
    protected String surchargeName;
    @XmlAttribute(name = "surchargeDescription")
    @XmlSchemaType(name = "anySimpleType")
    protected String surchargeDescription;
    @XmlAttribute(name = "currencyConversionTable")
    protected String currencyConversionTable;
    @XmlAttribute(name = "chargeType")
    protected String chargeType;
    @XmlAttribute(name = "chargeUnit")
    protected String chargeUnit;
    @XmlAttribute(name = "chargePeriodStartDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar chargePeriodStartDate;
    @XmlAttribute(name = "numberOfUnitForChargePeriod")
    protected BigInteger numberOfUnitForChargePeriod;
    @XmlAttribute(name = "currencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "surchargeAmount")
    protected Double surchargeAmount;

    /**
     * Gets the value of the surchargeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurchargeCode() {
        return surchargeCode;
    }

    /**
     * Sets the value of the surchargeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurchargeCode(String value) {
        this.surchargeCode = value;
    }

    /**
     * Gets the value of the surchargeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurchargeName() {
        return surchargeName;
    }

    /**
     * Sets the value of the surchargeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurchargeName(String value) {
        this.surchargeName = value;
    }

    /**
     * Gets the value of the surchargeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurchargeDescription() {
        return surchargeDescription;
    }

    /**
     * Sets the value of the surchargeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurchargeDescription(String value) {
        this.surchargeDescription = value;
    }

    /**
     * Gets the value of the currencyConversionTable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyConversionTable() {
        return currencyConversionTable;
    }

    /**
     * Sets the value of the currencyConversionTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyConversionTable(String value) {
        this.currencyConversionTable = value;
    }

    /**
     * Gets the value of the chargeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Sets the value of the chargeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeType(String value) {
        this.chargeType = value;
    }

    /**
     * Gets the value of the chargeUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeUnit() {
        return chargeUnit;
    }

    /**
     * Sets the value of the chargeUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeUnit(String value) {
        this.chargeUnit = value;
    }

    /**
     * Gets the value of the chargePeriodStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChargePeriodStartDate() {
        return chargePeriodStartDate;
    }

    /**
     * Sets the value of the chargePeriodStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChargePeriodStartDate(XMLGregorianCalendar value) {
        this.chargePeriodStartDate = value;
    }

    /**
     * Gets the value of the numberOfUnitForChargePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfUnitForChargePeriod() {
        return numberOfUnitForChargePeriod;
    }

    /**
     * Sets the value of the numberOfUnitForChargePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfUnitForChargePeriod(BigInteger value) {
        this.numberOfUnitForChargePeriod = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the surchargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSurchargeAmount() {
        return surchargeAmount;
    }

    /**
     * Sets the value of the surchargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSurchargeAmount(Double value) {
        this.surchargeAmount = value;
    }

}
