
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CryptObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CryptObjectType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="cryptId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="cryptData" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CryptObjectType")
public class CryptObjectType {

    @XmlAttribute(name = "cryptId")
    protected BigInteger cryptId;
    @XmlAttribute(name = "cryptData")
    protected String cryptData;

    /**
     * Gets the value of the cryptId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCryptId() {
        return cryptId;
    }

    /**
     * Sets the value of the cryptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCryptId(BigInteger value) {
        this.cryptId = value;
    }

    /**
     * Gets the value of the cryptData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCryptData() {
        return cryptData;
    }

    /**
     * Sets the value of the cryptData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCryptData(String value) {
        this.cryptData = value;
    }

}
