
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LegalIdListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegalIdListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="legalId" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}LegalIdType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegalIdListType", propOrder = {
    "legalId"
})
public class LegalIdListType {

    @XmlElement(required = true)
    protected List<LegalIdType> legalId;

    /**
     * Gets the value of the legalId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the legalId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLegalId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LegalIdType }
     * 
     * 
     */
    public List<LegalIdType> getLegalId() {
        if (legalId == null) {
            legalId = new ArrayList<LegalIdType>();
        }
        return this.legalId;
    }

}
