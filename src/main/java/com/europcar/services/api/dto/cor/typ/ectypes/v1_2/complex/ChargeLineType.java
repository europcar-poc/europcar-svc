
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ChargeLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeLineType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="chargeLineId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="chargeCategory" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="VAT_ExcludedChargeAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="applicatedVAT" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="VAT_ReferenceCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeStatus" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="isActive" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="toBeShown" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="taxCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="taxStartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="auxiliaryKey" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="isAllocatedToRenter" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="number" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="chargeBasis" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="percentage" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="chargeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="originalCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="payer" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="payerType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="technicalOperation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="amountWithTax" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeLineType")
public class ChargeLineType {

    @XmlAttribute(name = "chargeLineId")
    protected BigInteger chargeLineId;
    @XmlAttribute(name = "chargeCategory")
    protected String chargeCategory;
    @XmlAttribute(name = "chargeType")
    protected String chargeType;
    @XmlAttribute(name = "chargeTypeDescription")
    protected String chargeTypeDescription;
    @XmlAttribute(name = "VAT_ExcludedChargeAmount")
    protected BigDecimal vatExcludedChargeAmount;
    @XmlAttribute(name = "applicatedVAT")
    protected String applicatedVAT;
    @XmlAttribute(name = "VAT_ReferenceCode")
    protected String vatReferenceCode;
    @XmlAttribute(name = "chargeStatus")
    protected String chargeStatus;
    @XmlAttribute(name = "isActive")
    protected String isActive;
    @XmlAttribute(name = "toBeShown")
    protected String toBeShown;
    @XmlAttribute(name = "taxCode")
    protected String taxCode;
    @XmlAttribute(name = "taxStartDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar taxStartDate;
    @XmlAttribute(name = "auxiliaryKey")
    protected String auxiliaryKey;
    @XmlAttribute(name = "isAllocatedToRenter")
    protected String isAllocatedToRenter;
    @XmlAttribute(name = "unitCode")
    protected String unitCode;
    @XmlAttribute(name = "number")
    protected BigInteger number;
    @XmlAttribute(name = "chargeBasis")
    protected BigDecimal chargeBasis;
    @XmlAttribute(name = "percentage")
    protected BigDecimal percentage;
    @XmlAttribute(name = "chargeRate")
    protected BigDecimal chargeRate;
    @XmlAttribute(name = "originalCharge")
    protected BigDecimal originalCharge;
    @XmlAttribute(name = "payer")
    protected BigInteger payer;
    @XmlAttribute(name = "payerType")
    protected String payerType;
    @XmlAttribute(name = "technicalOperation")
    protected String technicalOperation;
    @XmlAttribute(name = "amountWithTax")
    protected String amountWithTax;

    /**
     * Gets the value of the chargeLineId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getChargeLineId() {
        return chargeLineId;
    }

    /**
     * Sets the value of the chargeLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setChargeLineId(BigInteger value) {
        this.chargeLineId = value;
    }

    /**
     * Gets the value of the chargeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCategory() {
        return chargeCategory;
    }

    /**
     * Sets the value of the chargeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCategory(String value) {
        this.chargeCategory = value;
    }

    /**
     * Gets the value of the chargeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Sets the value of the chargeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeType(String value) {
        this.chargeType = value;
    }

    /**
     * Gets the value of the chargeTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeTypeDescription() {
        return chargeTypeDescription;
    }

    /**
     * Sets the value of the chargeTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeTypeDescription(String value) {
        this.chargeTypeDescription = value;
    }

    /**
     * Gets the value of the vatExcludedChargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVATExcludedChargeAmount() {
        return vatExcludedChargeAmount;
    }

    /**
     * Sets the value of the vatExcludedChargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVATExcludedChargeAmount(BigDecimal value) {
        this.vatExcludedChargeAmount = value;
    }

    /**
     * Gets the value of the applicatedVAT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicatedVAT() {
        return applicatedVAT;
    }

    /**
     * Sets the value of the applicatedVAT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicatedVAT(String value) {
        this.applicatedVAT = value;
    }

    /**
     * Gets the value of the vatReferenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATReferenceCode() {
        return vatReferenceCode;
    }

    /**
     * Sets the value of the vatReferenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATReferenceCode(String value) {
        this.vatReferenceCode = value;
    }

    /**
     * Gets the value of the chargeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeStatus() {
        return chargeStatus;
    }

    /**
     * Sets the value of the chargeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeStatus(String value) {
        this.chargeStatus = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the toBeShown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToBeShown() {
        return toBeShown;
    }

    /**
     * Sets the value of the toBeShown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToBeShown(String value) {
        this.toBeShown = value;
    }

    /**
     * Gets the value of the taxCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCode() {
        return taxCode;
    }

    /**
     * Sets the value of the taxCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCode(String value) {
        this.taxCode = value;
    }

    /**
     * Gets the value of the taxStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaxStartDate() {
        return taxStartDate;
    }

    /**
     * Sets the value of the taxStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaxStartDate(XMLGregorianCalendar value) {
        this.taxStartDate = value;
    }

    /**
     * Gets the value of the auxiliaryKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuxiliaryKey() {
        return auxiliaryKey;
    }

    /**
     * Sets the value of the auxiliaryKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuxiliaryKey(String value) {
        this.auxiliaryKey = value;
    }

    /**
     * Gets the value of the isAllocatedToRenter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllocatedToRenter() {
        return isAllocatedToRenter;
    }

    /**
     * Sets the value of the isAllocatedToRenter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllocatedToRenter(String value) {
        this.isAllocatedToRenter = value;
    }

    /**
     * Gets the value of the unitCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitCode() {
        return unitCode;
    }

    /**
     * Sets the value of the unitCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitCode(String value) {
        this.unitCode = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumber(BigInteger value) {
        this.number = value;
    }

    /**
     * Gets the value of the chargeBasis property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargeBasis() {
        return chargeBasis;
    }

    /**
     * Sets the value of the chargeBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargeBasis(BigDecimal value) {
        this.chargeBasis = value;
    }

    /**
     * Gets the value of the percentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentage() {
        return percentage;
    }

    /**
     * Sets the value of the percentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentage(BigDecimal value) {
        this.percentage = value;
    }

    /**
     * Gets the value of the chargeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargeRate() {
        return chargeRate;
    }

    /**
     * Sets the value of the chargeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargeRate(BigDecimal value) {
        this.chargeRate = value;
    }

    /**
     * Gets the value of the originalCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOriginalCharge() {
        return originalCharge;
    }

    /**
     * Sets the value of the originalCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOriginalCharge(BigDecimal value) {
        this.originalCharge = value;
    }

    /**
     * Gets the value of the payer property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPayer() {
        return payer;
    }

    /**
     * Sets the value of the payer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPayer(BigInteger value) {
        this.payer = value;
    }

    /**
     * Gets the value of the payerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayerType() {
        return payerType;
    }

    /**
     * Sets the value of the payerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayerType(String value) {
        this.payerType = value;
    }

    /**
     * Gets the value of the technicalOperation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicalOperation() {
        return technicalOperation;
    }

    /**
     * Sets the value of the technicalOperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicalOperation(String value) {
        this.technicalOperation = value;
    }

    /**
     * Gets the value of the amountWithTax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmountWithTax() {
        return amountWithTax;
    }

    /**
     * Sets the value of the amountWithTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmountWithTax(String value) {
        this.amountWithTax = value;
    }

}
