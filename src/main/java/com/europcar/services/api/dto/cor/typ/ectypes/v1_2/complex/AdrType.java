
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for adrType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="adrType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="document" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="location" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}infosType" minOccurs="0"/&gt;
 *                   &lt;element name="circonstancy" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}infosType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="accidentType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="damageSpotter" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="accidentDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *                 &lt;attribute name="europeanStatement" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="firstCall" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="witnessDetail" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="witnessList" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="witness" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="witnessInvolved" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="thirdPartyDetail" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="thirdPartyList" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="thirdParty" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="vehicle" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                               &lt;/sequence&gt;
 *                                               &lt;attribute name="vehicleRegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                               &lt;attribute name="vehicleModel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                               &lt;attribute name="vehicleColor" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                               &lt;attribute name="insuranceCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                               &lt;attribute name="insuranceNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                               &lt;attribute name="damage" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="thirdPartyInvolved" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="personInjuredDetail" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="personInjuredList" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="personInjured" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="description" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}infosType" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="personInjuredInvolved" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="policeList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="police" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="OfficerId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                           &lt;attribute name="officerName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="place" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="vehicle" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="immediateRepair" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="tracking" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}TrackingType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ADRId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="vehicleUnitNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="movementID" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="driverId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="driverStaff" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="source" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adrType", propOrder = {
    "document",
    "witnessDetail",
    "thirdPartyDetail",
    "personInjuredDetail",
    "policeList",
    "vehicle",
    "tracking"
})
public class AdrType {

    protected AdrType.Document document;
    protected AdrType.WitnessDetail witnessDetail;
    protected AdrType.ThirdPartyDetail thirdPartyDetail;
    protected AdrType.PersonInjuredDetail personInjuredDetail;
    protected AdrType.PoliceList policeList;
    protected AdrType.Vehicle vehicle;
    protected TrackingType tracking;
    @XmlAttribute(name = "ADRId")
    protected BigInteger adrId;
    @XmlAttribute(name = "vehicleUnitNumber")
    protected BigInteger vehicleUnitNumber;
    @XmlAttribute(name = "movementID")
    protected BigInteger movementID;
    @XmlAttribute(name = "driverId")
    protected String driverId;
    @XmlAttribute(name = "driverStaff")
    protected String driverStaff;
    @XmlAttribute(name = "status")
    protected String status;
    @XmlAttribute(name = "source")
    protected String source;

    /**
     * Gets the value of the document property.
     * 
     * @return
     *     possible object is
     *     {@link AdrType.Document }
     *     
     */
    public AdrType.Document getDocument() {
        return document;
    }

    /**
     * Sets the value of the document property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdrType.Document }
     *     
     */
    public void setDocument(AdrType.Document value) {
        this.document = value;
    }

    /**
     * Gets the value of the witnessDetail property.
     * 
     * @return
     *     possible object is
     *     {@link AdrType.WitnessDetail }
     *     
     */
    public AdrType.WitnessDetail getWitnessDetail() {
        return witnessDetail;
    }

    /**
     * Sets the value of the witnessDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdrType.WitnessDetail }
     *     
     */
    public void setWitnessDetail(AdrType.WitnessDetail value) {
        this.witnessDetail = value;
    }

    /**
     * Gets the value of the thirdPartyDetail property.
     * 
     * @return
     *     possible object is
     *     {@link AdrType.ThirdPartyDetail }
     *     
     */
    public AdrType.ThirdPartyDetail getThirdPartyDetail() {
        return thirdPartyDetail;
    }

    /**
     * Sets the value of the thirdPartyDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdrType.ThirdPartyDetail }
     *     
     */
    public void setThirdPartyDetail(AdrType.ThirdPartyDetail value) {
        this.thirdPartyDetail = value;
    }

    /**
     * Gets the value of the personInjuredDetail property.
     * 
     * @return
     *     possible object is
     *     {@link AdrType.PersonInjuredDetail }
     *     
     */
    public AdrType.PersonInjuredDetail getPersonInjuredDetail() {
        return personInjuredDetail;
    }

    /**
     * Sets the value of the personInjuredDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdrType.PersonInjuredDetail }
     *     
     */
    public void setPersonInjuredDetail(AdrType.PersonInjuredDetail value) {
        this.personInjuredDetail = value;
    }

    /**
     * Gets the value of the policeList property.
     * 
     * @return
     *     possible object is
     *     {@link AdrType.PoliceList }
     *     
     */
    public AdrType.PoliceList getPoliceList() {
        return policeList;
    }

    /**
     * Sets the value of the policeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdrType.PoliceList }
     *     
     */
    public void setPoliceList(AdrType.PoliceList value) {
        this.policeList = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link AdrType.Vehicle }
     *     
     */
    public AdrType.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdrType.Vehicle }
     *     
     */
    public void setVehicle(AdrType.Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the tracking property.
     * 
     * @return
     *     possible object is
     *     {@link TrackingType }
     *     
     */
    public TrackingType getTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrackingType }
     *     
     */
    public void setTracking(TrackingType value) {
        this.tracking = value;
    }

    /**
     * Gets the value of the adrId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getADRId() {
        return adrId;
    }

    /**
     * Sets the value of the adrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setADRId(BigInteger value) {
        this.adrId = value;
    }

    /**
     * Gets the value of the vehicleUnitNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVehicleUnitNumber() {
        return vehicleUnitNumber;
    }

    /**
     * Sets the value of the vehicleUnitNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVehicleUnitNumber(BigInteger value) {
        this.vehicleUnitNumber = value;
    }

    /**
     * Gets the value of the movementID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMovementID() {
        return movementID;
    }

    /**
     * Sets the value of the movementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMovementID(BigInteger value) {
        this.movementID = value;
    }

    /**
     * Gets the value of the driverId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverId() {
        return driverId;
    }

    /**
     * Sets the value of the driverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverId(String value) {
        this.driverId = value;
    }

    /**
     * Gets the value of the driverStaff property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverStaff() {
        return driverStaff;
    }

    /**
     * Sets the value of the driverStaff property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverStaff(String value) {
        this.driverStaff = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="location" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}infosType" minOccurs="0"/&gt;
     *         &lt;element name="circonstancy" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}infosType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="accidentType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="damageSpotter" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="accidentDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
     *       &lt;attribute name="europeanStatement" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="firstCall" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location",
        "circonstancy"
    })
    public static class Document {

        protected InfosType location;
        protected InfosType circonstancy;
        @XmlAttribute(name = "accidentType")
        protected String accidentType;
        @XmlAttribute(name = "damageSpotter")
        protected String damageSpotter;
        @XmlAttribute(name = "accidentDateTime")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar accidentDateTime;
        @XmlAttribute(name = "europeanStatement")
        protected Boolean europeanStatement;
        @XmlAttribute(name = "firstCall")
        protected Boolean firstCall;

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link InfosType }
         *     
         */
        public InfosType getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link InfosType }
         *     
         */
        public void setLocation(InfosType value) {
            this.location = value;
        }

        /**
         * Gets the value of the circonstancy property.
         * 
         * @return
         *     possible object is
         *     {@link InfosType }
         *     
         */
        public InfosType getCirconstancy() {
            return circonstancy;
        }

        /**
         * Sets the value of the circonstancy property.
         * 
         * @param value
         *     allowed object is
         *     {@link InfosType }
         *     
         */
        public void setCirconstancy(InfosType value) {
            this.circonstancy = value;
        }

        /**
         * Gets the value of the accidentType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccidentType() {
            return accidentType;
        }

        /**
         * Sets the value of the accidentType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccidentType(String value) {
            this.accidentType = value;
        }

        /**
         * Gets the value of the damageSpotter property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDamageSpotter() {
            return damageSpotter;
        }

        /**
         * Sets the value of the damageSpotter property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDamageSpotter(String value) {
            this.damageSpotter = value;
        }

        /**
         * Gets the value of the accidentDateTime property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getAccidentDateTime() {
            return accidentDateTime;
        }

        /**
         * Sets the value of the accidentDateTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setAccidentDateTime(XMLGregorianCalendar value) {
            this.accidentDateTime = value;
        }

        /**
         * Gets the value of the europeanStatement property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isEuropeanStatement() {
            return europeanStatement;
        }

        /**
         * Sets the value of the europeanStatement property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setEuropeanStatement(Boolean value) {
            this.europeanStatement = value;
        }

        /**
         * Gets the value of the firstCall property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isFirstCall() {
            return firstCall;
        }

        /**
         * Sets the value of the firstCall property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setFirstCall(Boolean value) {
            this.firstCall = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="personInjuredList" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="personInjured" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="description" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}infosType" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="personInjuredInvolved" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personInjuredList"
    })
    public static class PersonInjuredDetail {

        protected AdrType.PersonInjuredDetail.PersonInjuredList personInjuredList;
        @XmlAttribute(name = "personInjuredInvolved")
        protected Boolean personInjuredInvolved;

        /**
         * Gets the value of the personInjuredList property.
         * 
         * @return
         *     possible object is
         *     {@link AdrType.PersonInjuredDetail.PersonInjuredList }
         *     
         */
        public AdrType.PersonInjuredDetail.PersonInjuredList getPersonInjuredList() {
            return personInjuredList;
        }

        /**
         * Sets the value of the personInjuredList property.
         * 
         * @param value
         *     allowed object is
         *     {@link AdrType.PersonInjuredDetail.PersonInjuredList }
         *     
         */
        public void setPersonInjuredList(AdrType.PersonInjuredDetail.PersonInjuredList value) {
            this.personInjuredList = value;
        }

        /**
         * Gets the value of the personInjuredInvolved property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPersonInjuredInvolved() {
            return personInjuredInvolved;
        }

        /**
         * Sets the value of the personInjuredInvolved property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPersonInjuredInvolved(Boolean value) {
            this.personInjuredInvolved = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="personInjured" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="description" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}infosType" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/extension&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "personInjured"
        })
        public static class PersonInjuredList {

            @XmlElement(nillable = true)
            protected List<AdrType.PersonInjuredDetail.PersonInjuredList.PersonInjured> personInjured;

            /**
             * Gets the value of the personInjured property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the personInjured property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPersonInjured().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AdrType.PersonInjuredDetail.PersonInjuredList.PersonInjured }
             * 
             * 
             */
            public List<AdrType.PersonInjuredDetail.PersonInjuredList.PersonInjured> getPersonInjured() {
                if (personInjured == null) {
                    personInjured = new ArrayList<AdrType.PersonInjuredDetail.PersonInjuredList.PersonInjured>();
                }
                return this.personInjured;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="description" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}infosType" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/extension&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "description"
            })
            public static class PersonInjured
                extends PersonType
            {

                protected InfosType description;

                /**
                 * Gets the value of the description property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link InfosType }
                 *     
                 */
                public InfosType getDescription() {
                    return description;
                }

                /**
                 * Sets the value of the description property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link InfosType }
                 *     
                 */
                public void setDescription(InfosType value) {
                    this.description = value;
                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="police" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="OfficerId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *                 &lt;attribute name="officerName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="place" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "police"
    })
    public static class PoliceList {

        @XmlElement(nillable = true)
        protected List<AdrType.PoliceList.Police> police;

        /**
         * Gets the value of the police property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the police property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPolice().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AdrType.PoliceList.Police }
         * 
         * 
         */
        public List<AdrType.PoliceList.Police> getPolice() {
            if (police == null) {
                police = new ArrayList<AdrType.PoliceList.Police>();
            }
            return this.police;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="OfficerId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
         *       &lt;attribute name="officerName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="place" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Police {

            @XmlAttribute(name = "OfficerId")
            @XmlSchemaType(name = "anySimpleType")
            protected String officerId;
            @XmlAttribute(name = "officerName")
            protected String officerName;
            @XmlAttribute(name = "place")
            protected String place;

            /**
             * Gets the value of the officerId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOfficerId() {
                return officerId;
            }

            /**
             * Sets the value of the officerId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOfficerId(String value) {
                this.officerId = value;
            }

            /**
             * Gets the value of the officerName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOfficerName() {
                return officerName;
            }

            /**
             * Sets the value of the officerName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOfficerName(String value) {
                this.officerName = value;
            }

            /**
             * Gets the value of the place property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlace() {
                return place;
            }

            /**
             * Sets the value of the place property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlace(String value) {
                this.place = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="thirdPartyList" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="thirdParty" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="vehicle" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                     &lt;/sequence&gt;
     *                                     &lt;attribute name="vehicleRegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                     &lt;attribute name="vehicleModel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                     &lt;attribute name="vehicleColor" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                     &lt;attribute name="insuranceCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                     &lt;attribute name="insuranceNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                     &lt;attribute name="damage" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="thirdPartyInvolved" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "thirdPartyList"
    })
    public static class ThirdPartyDetail {

        protected AdrType.ThirdPartyDetail.ThirdPartyList thirdPartyList;
        @XmlAttribute(name = "thirdPartyInvolved")
        protected String thirdPartyInvolved;

        /**
         * Gets the value of the thirdPartyList property.
         * 
         * @return
         *     possible object is
         *     {@link AdrType.ThirdPartyDetail.ThirdPartyList }
         *     
         */
        public AdrType.ThirdPartyDetail.ThirdPartyList getThirdPartyList() {
            return thirdPartyList;
        }

        /**
         * Sets the value of the thirdPartyList property.
         * 
         * @param value
         *     allowed object is
         *     {@link AdrType.ThirdPartyDetail.ThirdPartyList }
         *     
         */
        public void setThirdPartyList(AdrType.ThirdPartyDetail.ThirdPartyList value) {
            this.thirdPartyList = value;
        }

        /**
         * Gets the value of the thirdPartyInvolved property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getThirdPartyInvolved() {
            return thirdPartyInvolved;
        }

        /**
         * Sets the value of the thirdPartyInvolved property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setThirdPartyInvolved(String value) {
            this.thirdPartyInvolved = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="thirdParty" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="vehicle" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                           &lt;/sequence&gt;
         *                           &lt;attribute name="vehicleRegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                           &lt;attribute name="vehicleModel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                           &lt;attribute name="vehicleColor" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                           &lt;attribute name="insuranceCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                           &lt;attribute name="insuranceNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                           &lt;attribute name="damage" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/extension&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "thirdParty"
        })
        public static class ThirdPartyList {

            @XmlElement(nillable = true)
            protected List<AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty> thirdParty;

            /**
             * Gets the value of the thirdParty property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the thirdParty property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getThirdParty().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty }
             * 
             * 
             */
            public List<AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty> getThirdParty() {
                if (thirdParty == null) {
                    thirdParty = new ArrayList<AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty>();
                }
                return this.thirdParty;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="vehicle" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                 &lt;/sequence&gt;
             *                 &lt;attribute name="vehicleRegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                 &lt;attribute name="vehicleModel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                 &lt;attribute name="vehicleColor" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                 &lt;attribute name="insuranceCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                 &lt;attribute name="insuranceNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                 &lt;attribute name="damage" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/extension&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "vehicle"
            })
            public static class ThirdParty
                extends PersonType
            {

                protected AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty.Vehicle vehicle;

                /**
                 * Gets the value of the vehicle property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty.Vehicle }
                 *     
                 */
                public AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty.Vehicle getVehicle() {
                    return vehicle;
                }

                /**
                 * Sets the value of the vehicle property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty.Vehicle }
                 *     
                 */
                public void setVehicle(AdrType.ThirdPartyDetail.ThirdPartyList.ThirdParty.Vehicle value) {
                    this.vehicle = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *       &lt;/sequence&gt;
                 *       &lt;attribute name="vehicleRegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *       &lt;attribute name="vehicleModel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *       &lt;attribute name="vehicleColor" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *       &lt;attribute name="insuranceCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *       &lt;attribute name="insuranceNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *       &lt;attribute name="damage" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Vehicle {

                    @XmlAttribute(name = "vehicleRegistrationNumber")
                    protected String vehicleRegistrationNumber;
                    @XmlAttribute(name = "vehicleModel")
                    protected String vehicleModel;
                    @XmlAttribute(name = "vehicleColor")
                    protected String vehicleColor;
                    @XmlAttribute(name = "insuranceCompanyName")
                    protected String insuranceCompanyName;
                    @XmlAttribute(name = "insuranceNumber")
                    protected String insuranceNumber;
                    @XmlAttribute(name = "damage")
                    protected Boolean damage;

                    /**
                     * Gets the value of the vehicleRegistrationNumber property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getVehicleRegistrationNumber() {
                        return vehicleRegistrationNumber;
                    }

                    /**
                     * Sets the value of the vehicleRegistrationNumber property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setVehicleRegistrationNumber(String value) {
                        this.vehicleRegistrationNumber = value;
                    }

                    /**
                     * Gets the value of the vehicleModel property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getVehicleModel() {
                        return vehicleModel;
                    }

                    /**
                     * Sets the value of the vehicleModel property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setVehicleModel(String value) {
                        this.vehicleModel = value;
                    }

                    /**
                     * Gets the value of the vehicleColor property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getVehicleColor() {
                        return vehicleColor;
                    }

                    /**
                     * Sets the value of the vehicleColor property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setVehicleColor(String value) {
                        this.vehicleColor = value;
                    }

                    /**
                     * Gets the value of the insuranceCompanyName property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getInsuranceCompanyName() {
                        return insuranceCompanyName;
                    }

                    /**
                     * Sets the value of the insuranceCompanyName property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setInsuranceCompanyName(String value) {
                        this.insuranceCompanyName = value;
                    }

                    /**
                     * Gets the value of the insuranceNumber property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getInsuranceNumber() {
                        return insuranceNumber;
                    }

                    /**
                     * Sets the value of the insuranceNumber property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setInsuranceNumber(String value) {
                        this.insuranceNumber = value;
                    }

                    /**
                     * Gets the value of the damage property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isDamage() {
                        return damage;
                    }

                    /**
                     * Sets the value of the damage property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setDamage(Boolean value) {
                        this.damage = value;
                    }

                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="immediateRepair" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Vehicle {

        @XmlAttribute(name = "immediateRepair")
        protected Boolean immediateRepair;

        /**
         * Gets the value of the immediateRepair property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isImmediateRepair() {
            return immediateRepair;
        }

        /**
         * Sets the value of the immediateRepair property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setImmediateRepair(Boolean value) {
            this.immediateRepair = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="witnessList" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="witness" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="witnessInvolved" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "witnessList"
    })
    public static class WitnessDetail {

        protected AdrType.WitnessDetail.WitnessList witnessList;
        @XmlAttribute(name = "witnessInvolved")
        protected Boolean witnessInvolved;

        /**
         * Gets the value of the witnessList property.
         * 
         * @return
         *     possible object is
         *     {@link AdrType.WitnessDetail.WitnessList }
         *     
         */
        public AdrType.WitnessDetail.WitnessList getWitnessList() {
            return witnessList;
        }

        /**
         * Sets the value of the witnessList property.
         * 
         * @param value
         *     allowed object is
         *     {@link AdrType.WitnessDetail.WitnessList }
         *     
         */
        public void setWitnessList(AdrType.WitnessDetail.WitnessList value) {
            this.witnessList = value;
        }

        /**
         * Gets the value of the witnessInvolved property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isWitnessInvolved() {
            return witnessInvolved;
        }

        /**
         * Sets the value of the witnessInvolved property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setWitnessInvolved(Boolean value) {
            this.witnessInvolved = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="witness" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}personType" maxOccurs="unbounded" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "witness"
        })
        public static class WitnessList {

            @XmlElement(nillable = true)
            protected List<PersonType> witness;

            /**
             * Gets the value of the witness property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the witness property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getWitness().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link PersonType }
             * 
             * 
             */
            public List<PersonType> getWitness() {
                if (witness == null) {
                    witness = new ArrayList<PersonType>();
                }
                return this.witness;
            }

        }

    }

}
