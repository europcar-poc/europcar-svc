
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargeCardPaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeCardPaymentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ChargeCardExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessAccountId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="ChargeCardholderFirstName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ChargeCardholderLastName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chargeCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeCardPaymentType")
public class ChargeCardPaymentType {

    @XmlAttribute(name = "ChargeCardExpiryDate")
    protected String chargeCardExpiryDate;
    @XmlAttribute(name = "businessAccountId")
    protected BigInteger businessAccountId;
    @XmlAttribute(name = "ChargeCardholderFirstName")
    protected String chargeCardholderFirstName;
    @XmlAttribute(name = "ChargeCardholderLastName")
    protected String chargeCardholderLastName;
    @XmlAttribute(name = "chargeCardNumber")
    protected String chargeCardNumber;

    /**
     * Gets the value of the chargeCardExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCardExpiryDate() {
        return chargeCardExpiryDate;
    }

    /**
     * Sets the value of the chargeCardExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCardExpiryDate(String value) {
        this.chargeCardExpiryDate = value;
    }

    /**
     * Gets the value of the businessAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBusinessAccountId() {
        return businessAccountId;
    }

    /**
     * Sets the value of the businessAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBusinessAccountId(BigInteger value) {
        this.businessAccountId = value;
    }

    /**
     * Gets the value of the chargeCardholderFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCardholderFirstName() {
        return chargeCardholderFirstName;
    }

    /**
     * Sets the value of the chargeCardholderFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCardholderFirstName(String value) {
        this.chargeCardholderFirstName = value;
    }

    /**
     * Gets the value of the chargeCardholderLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCardholderLastName() {
        return chargeCardholderLastName;
    }

    /**
     * Sets the value of the chargeCardholderLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCardholderLastName(String value) {
        this.chargeCardholderLastName = value;
    }

    /**
     * Gets the value of the chargeCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCardNumber() {
        return chargeCardNumber;
    }

    /**
     * Sets the value of the chargeCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCardNumber(String value) {
        this.chargeCardNumber = value;
    }

}
