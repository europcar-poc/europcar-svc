
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.equip;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.EquipmentType;


/**
 * <p>Java class for EquipmentDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EquipmentDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}EquipmentType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentDTO")
public class EquipmentDTO
    extends EquipmentType
{


}
