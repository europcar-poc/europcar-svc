
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactEmailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactEmailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="email" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}EmailType"/&gt;
 *         &lt;element name="emailPreferences"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="mainEmail" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactEmailType", propOrder = {
    "email",
    "emailPreferences"
})
public class ContactEmailType {

    @XmlElement(required = true)
    protected EmailType email;
    @XmlElement(required = true)
    protected ContactEmailType.EmailPreferences emailPreferences;

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link EmailType }
     *     
     */
    public EmailType getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailType }
     *     
     */
    public void setEmail(EmailType value) {
        this.email = value;
    }

    /**
     * Gets the value of the emailPreferences property.
     * 
     * @return
     *     possible object is
     *     {@link ContactEmailType.EmailPreferences }
     *     
     */
    public ContactEmailType.EmailPreferences getEmailPreferences() {
        return emailPreferences;
    }

    /**
     * Sets the value of the emailPreferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactEmailType.EmailPreferences }
     *     
     */
    public void setEmailPreferences(ContactEmailType.EmailPreferences value) {
        this.emailPreferences = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="mainEmail" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class EmailPreferences {

        @XmlAttribute(name = "mainEmail")
        protected String mainEmail;

        /**
         * Gets the value of the mainEmail property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMainEmail() {
            return mainEmail;
        }

        /**
         * Sets the value of the mainEmail property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMainEmail(String value) {
            this.mainEmail = value;
        }

    }

}
