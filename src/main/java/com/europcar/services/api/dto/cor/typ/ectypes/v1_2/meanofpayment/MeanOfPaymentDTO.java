
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.meanofpayment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MeanOfPaymentDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeanOfPaymentDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bankCard" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/meanofpayment}BankCardPaymentType" minOccurs="0"/&gt;
 *         &lt;element name="chargeCard" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/meanofpayment}ChargeCardPaymentType" minOccurs="0"/&gt;
 *         &lt;element name="BHPO" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/meanofpayment}BHPOPaymentType" minOccurs="0"/&gt;
 *         &lt;element name="voucher" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/meanofpayment}VoucherPaymentType" minOccurs="0"/&gt;
 *         &lt;element name="PPBA" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/meanofpayment}PPBAType" minOccurs="0"/&gt;
 *         &lt;element name="cheque" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/meanofpayment}ChequePaymentType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MOPType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="MOPCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="secondaryMOPType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="secondaryMOPCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeanOfPaymentDTO", propOrder = {
    "bankCard",
    "chargeCard",
    "bhpo",
    "voucher",
    "ppba",
    "cheque"
})
public class MeanOfPaymentDTO {

    protected BankCardPaymentType bankCard;
    protected ChargeCardPaymentType chargeCard;
    @XmlElement(name = "BHPO")
    protected BHPOPaymentType bhpo;
    protected VoucherPaymentType voucher;
    @XmlElement(name = "PPBA")
    protected PPBAType ppba;
    protected ChequePaymentType cheque;
    @XmlAttribute(name = "MOPType")
    protected String mopType;
    @XmlAttribute(name = "MOPCode")
    protected String mopCode;
    @XmlAttribute(name = "secondaryMOPType")
    protected String secondaryMOPType;
    @XmlAttribute(name = "secondaryMOPCode")
    protected String secondaryMOPCode;

    /**
     * Gets the value of the bankCard property.
     * 
     * @return
     *     possible object is
     *     {@link BankCardPaymentType }
     *     
     */
    public BankCardPaymentType getBankCard() {
        return bankCard;
    }

    /**
     * Sets the value of the bankCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankCardPaymentType }
     *     
     */
    public void setBankCard(BankCardPaymentType value) {
        this.bankCard = value;
    }

    /**
     * Gets the value of the chargeCard property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeCardPaymentType }
     *     
     */
    public ChargeCardPaymentType getChargeCard() {
        return chargeCard;
    }

    /**
     * Sets the value of the chargeCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeCardPaymentType }
     *     
     */
    public void setChargeCard(ChargeCardPaymentType value) {
        this.chargeCard = value;
    }

    /**
     * Gets the value of the bhpo property.
     * 
     * @return
     *     possible object is
     *     {@link BHPOPaymentType }
     *     
     */
    public BHPOPaymentType getBHPO() {
        return bhpo;
    }

    /**
     * Sets the value of the bhpo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BHPOPaymentType }
     *     
     */
    public void setBHPO(BHPOPaymentType value) {
        this.bhpo = value;
    }

    /**
     * Gets the value of the voucher property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherPaymentType }
     *     
     */
    public VoucherPaymentType getVoucher() {
        return voucher;
    }

    /**
     * Sets the value of the voucher property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherPaymentType }
     *     
     */
    public void setVoucher(VoucherPaymentType value) {
        this.voucher = value;
    }

    /**
     * Gets the value of the ppba property.
     * 
     * @return
     *     possible object is
     *     {@link PPBAType }
     *     
     */
    public PPBAType getPPBA() {
        return ppba;
    }

    /**
     * Sets the value of the ppba property.
     * 
     * @param value
     *     allowed object is
     *     {@link PPBAType }
     *     
     */
    public void setPPBA(PPBAType value) {
        this.ppba = value;
    }

    /**
     * Gets the value of the cheque property.
     * 
     * @return
     *     possible object is
     *     {@link ChequePaymentType }
     *     
     */
    public ChequePaymentType getCheque() {
        return cheque;
    }

    /**
     * Sets the value of the cheque property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChequePaymentType }
     *     
     */
    public void setCheque(ChequePaymentType value) {
        this.cheque = value;
    }

    /**
     * Gets the value of the mopType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMOPType() {
        return mopType;
    }

    /**
     * Sets the value of the mopType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMOPType(String value) {
        this.mopType = value;
    }

    /**
     * Gets the value of the mopCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMOPCode() {
        return mopCode;
    }

    /**
     * Sets the value of the mopCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMOPCode(String value) {
        this.mopCode = value;
    }

    /**
     * Gets the value of the secondaryMOPType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryMOPType() {
        return secondaryMOPType;
    }

    /**
     * Sets the value of the secondaryMOPType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryMOPType(String value) {
        this.secondaryMOPType = value;
    }

    /**
     * Gets the value of the secondaryMOPCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryMOPCode() {
        return secondaryMOPCode;
    }

    /**
     * Sets the value of the secondaryMOPCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryMOPCode(String value) {
        this.secondaryMOPCode = value;
    }

}
