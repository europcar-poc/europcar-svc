
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentTransaction_frag complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentTransaction_frag"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="authorization" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                 &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="number" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="isManual" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DCCexchanges" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="exchange" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                           &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                           &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="conversionServiceId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="fee" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                           &lt;attribute name="autherizedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                           &lt;attribute name="isCustomerAccepted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="amounts"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="renting" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentAmountType" minOccurs="0"/&gt;
 *                   &lt;element name="station" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentAmountType" minOccurs="0"/&gt;
 *                   &lt;element name="localStation" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentAmountType" minOccurs="0"/&gt;
 *                   &lt;element name="booking" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentAmountType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="billingTreatment"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="batchId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="batchProcId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="batchCardId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentTransaction_frag", propOrder = {
    "authorization",
    "dcCexchanges",
    "amounts",
    "billingTreatment"
})
public class PaymentTransactionFrag {

    protected PaymentTransactionFrag.Authorization authorization;
    @XmlElement(name = "DCCexchanges")
    protected PaymentTransactionFrag.DCCexchanges dcCexchanges;
    @XmlElement(required = true)
    protected PaymentTransactionFrag.Amounts amounts;
    @XmlElement(required = true)
    protected PaymentTransactionFrag.BillingTreatment billingTreatment;

    /**
     * Gets the value of the authorization property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTransactionFrag.Authorization }
     *     
     */
    public PaymentTransactionFrag.Authorization getAuthorization() {
        return authorization;
    }

    /**
     * Sets the value of the authorization property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTransactionFrag.Authorization }
     *     
     */
    public void setAuthorization(PaymentTransactionFrag.Authorization value) {
        this.authorization = value;
    }

    /**
     * Gets the value of the dcCexchanges property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTransactionFrag.DCCexchanges }
     *     
     */
    public PaymentTransactionFrag.DCCexchanges getDCCexchanges() {
        return dcCexchanges;
    }

    /**
     * Sets the value of the dcCexchanges property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTransactionFrag.DCCexchanges }
     *     
     */
    public void setDCCexchanges(PaymentTransactionFrag.DCCexchanges value) {
        this.dcCexchanges = value;
    }

    /**
     * Gets the value of the amounts property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTransactionFrag.Amounts }
     *     
     */
    public PaymentTransactionFrag.Amounts getAmounts() {
        return amounts;
    }

    /**
     * Sets the value of the amounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTransactionFrag.Amounts }
     *     
     */
    public void setAmounts(PaymentTransactionFrag.Amounts value) {
        this.amounts = value;
    }

    /**
     * Gets the value of the billingTreatment property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTransactionFrag.BillingTreatment }
     *     
     */
    public PaymentTransactionFrag.BillingTreatment getBillingTreatment() {
        return billingTreatment;
    }

    /**
     * Sets the value of the billingTreatment property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTransactionFrag.BillingTreatment }
     *     
     */
    public void setBillingTreatment(PaymentTransactionFrag.BillingTreatment value) {
        this.billingTreatment = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="renting" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentAmountType" minOccurs="0"/&gt;
     *         &lt;element name="station" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentAmountType" minOccurs="0"/&gt;
     *         &lt;element name="localStation" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentAmountType" minOccurs="0"/&gt;
     *         &lt;element name="booking" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PaymentAmountType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "renting",
        "station",
        "localStation",
        "booking"
    })
    public static class Amounts {

        protected PaymentAmountType renting;
        protected PaymentAmountType station;
        protected PaymentAmountType localStation;
        protected PaymentAmountType booking;

        /**
         * Gets the value of the renting property.
         * 
         * @return
         *     possible object is
         *     {@link PaymentAmountType }
         *     
         */
        public PaymentAmountType getRenting() {
            return renting;
        }

        /**
         * Sets the value of the renting property.
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentAmountType }
         *     
         */
        public void setRenting(PaymentAmountType value) {
            this.renting = value;
        }

        /**
         * Gets the value of the station property.
         * 
         * @return
         *     possible object is
         *     {@link PaymentAmountType }
         *     
         */
        public PaymentAmountType getStation() {
            return station;
        }

        /**
         * Sets the value of the station property.
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentAmountType }
         *     
         */
        public void setStation(PaymentAmountType value) {
            this.station = value;
        }

        /**
         * Gets the value of the localStation property.
         * 
         * @return
         *     possible object is
         *     {@link PaymentAmountType }
         *     
         */
        public PaymentAmountType getLocalStation() {
            return localStation;
        }

        /**
         * Sets the value of the localStation property.
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentAmountType }
         *     
         */
        public void setLocalStation(PaymentAmountType value) {
            this.localStation = value;
        }

        /**
         * Gets the value of the booking property.
         * 
         * @return
         *     possible object is
         *     {@link PaymentAmountType }
         *     
         */
        public PaymentAmountType getBooking() {
            return booking;
        }

        /**
         * Sets the value of the booking property.
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentAmountType }
         *     
         */
        public void setBooking(PaymentAmountType value) {
            this.booking = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *       &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="number" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="isManual" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Authorization {

        @XmlAttribute(name = "amount")
        protected BigDecimal amount;
        @XmlAttribute(name = "currency")
        protected String currency;
        @XmlAttribute(name = "number")
        protected String number;
        @XmlAttribute(name = "isManual")
        protected Boolean isManual;

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

        /**
         * Gets the value of the currency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrency() {
            return currency;
        }

        /**
         * Sets the value of the currency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrency(String value) {
            this.currency = value;
        }

        /**
         * Gets the value of the number property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumber() {
            return number;
        }

        /**
         * Sets the value of the number property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumber(String value) {
            this.number = value;
        }

        /**
         * Gets the value of the isManual property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsManual() {
            return isManual;
        }

        /**
         * Sets the value of the isManual property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsManual(Boolean value) {
            this.isManual = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="batchId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="batchProcId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="batchCardId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BillingTreatment {

        @XmlAttribute(name = "batchId")
        protected BigInteger batchId;
        @XmlAttribute(name = "batchProcId")
        protected BigInteger batchProcId;
        @XmlAttribute(name = "batchCardId")
        protected BigInteger batchCardId;

        /**
         * Gets the value of the batchId property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getBatchId() {
            return batchId;
        }

        /**
         * Sets the value of the batchId property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setBatchId(BigInteger value) {
            this.batchId = value;
        }

        /**
         * Gets the value of the batchProcId property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getBatchProcId() {
            return batchProcId;
        }

        /**
         * Sets the value of the batchProcId property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setBatchProcId(BigInteger value) {
            this.batchProcId = value;
        }

        /**
         * Gets the value of the batchCardId property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getBatchCardId() {
            return batchCardId;
        }

        /**
         * Sets the value of the batchCardId property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setBatchCardId(BigInteger value) {
            this.batchCardId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="exchange" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *                 &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *                 &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="conversionServiceId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="fee" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *                 &lt;attribute name="autherizedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *                 &lt;attribute name="isCustomerAccepted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "exchange"
    })
    public static class DCCexchanges {

        @XmlElement(required = true)
        protected List<PaymentTransactionFrag.DCCexchanges.Exchange> exchange;

        /**
         * Gets the value of the exchange property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the exchange property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getExchange().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PaymentTransactionFrag.DCCexchanges.Exchange }
         * 
         * 
         */
        public List<PaymentTransactionFrag.DCCexchanges.Exchange> getExchange() {
            if (exchange == null) {
                exchange = new ArrayList<PaymentTransactionFrag.DCCexchanges.Exchange>();
            }
            return this.exchange;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
         *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
         *       &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="conversionServiceId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="fee" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
         *       &lt;attribute name="autherizedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
         *       &lt;attribute name="isCustomerAccepted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Exchange {

            @XmlAttribute(name = "type")
            protected String type;
            @XmlAttribute(name = "rate")
            protected BigDecimal rate;
            @XmlAttribute(name = "amount")
            protected BigDecimal amount;
            @XmlAttribute(name = "currency")
            protected String currency;
            @XmlAttribute(name = "conversionServiceId")
            protected String conversionServiceId;
            @XmlAttribute(name = "fee")
            protected BigDecimal fee;
            @XmlAttribute(name = "autherizedAmount")
            protected BigDecimal autherizedAmount;
            @XmlAttribute(name = "isCustomerAccepted")
            protected Boolean isCustomerAccepted;

            /**
             * Gets the value of the type property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Sets the value of the type property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Gets the value of the rate property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getRate() {
                return rate;
            }

            /**
             * Sets the value of the rate property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setRate(BigDecimal value) {
                this.rate = value;
            }

            /**
             * Gets the value of the amount property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Sets the value of the amount property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

            /**
             * Gets the value of the currency property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrency() {
                return currency;
            }

            /**
             * Sets the value of the currency property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrency(String value) {
                this.currency = value;
            }

            /**
             * Gets the value of the conversionServiceId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getConversionServiceId() {
                return conversionServiceId;
            }

            /**
             * Sets the value of the conversionServiceId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setConversionServiceId(String value) {
                this.conversionServiceId = value;
            }

            /**
             * Gets the value of the fee property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getFee() {
                return fee;
            }

            /**
             * Sets the value of the fee property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setFee(BigDecimal value) {
                this.fee = value;
            }

            /**
             * Gets the value of the autherizedAmount property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAutherizedAmount() {
                return autherizedAmount;
            }

            /**
             * Sets the value of the autherizedAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAutherizedAmount(BigDecimal value) {
                this.autherizedAmount = value;
            }

            /**
             * Gets the value of the isCustomerAccepted property.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isIsCustomerAccepted() {
                return isCustomerAccepted;
            }

            /**
             * Sets the value of the isCustomerAccepted property.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setIsCustomerAccepted(Boolean value) {
                this.isCustomerAccepted = value;
            }

        }

    }

}
