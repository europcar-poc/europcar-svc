
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestContextType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestContextType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="technicalContext" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}TechnicalContextType" minOccurs="0"/&gt;
 *         &lt;element name="businessContext" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}BusinessContextType" minOccurs="0"/&gt;
 *         &lt;element name="activityContext" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ActivityContextType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestContextType", propOrder = {
    "technicalContext",
    "businessContext",
    "activityContext"
})
public class RequestContextType {

    protected TechnicalContextType technicalContext;
    protected BusinessContextType businessContext;
    protected ActivityContextType activityContext;

    /**
     * Gets the value of the technicalContext property.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalContextType }
     *     
     */
    public TechnicalContextType getTechnicalContext() {
        return technicalContext;
    }

    /**
     * Sets the value of the technicalContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalContextType }
     *     
     */
    public void setTechnicalContext(TechnicalContextType value) {
        this.technicalContext = value;
    }

    /**
     * Gets the value of the businessContext property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessContextType }
     *     
     */
    public BusinessContextType getBusinessContext() {
        return businessContext;
    }

    /**
     * Sets the value of the businessContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessContextType }
     *     
     */
    public void setBusinessContext(BusinessContextType value) {
        this.businessContext = value;
    }

    /**
     * Gets the value of the activityContext property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityContextType }
     *     
     */
    public ActivityContextType getActivityContext() {
        return activityContext;
    }

    /**
     * Sets the value of the activityContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityContextType }
     *     
     */
    public void setActivityContext(ActivityContextType value) {
        this.activityContext = value;
    }

}
