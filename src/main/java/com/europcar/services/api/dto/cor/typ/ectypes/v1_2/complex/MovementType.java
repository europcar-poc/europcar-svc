
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MovementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MovementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="driver" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DriverType" minOccurs="0"/&gt;
 *         &lt;element name="distance" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="mileageUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="mileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="movementID" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="unitNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="rentalAgreementNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="vehicleCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="movementType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="revisionNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="vehicleModelDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="franchiseeDocNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="registrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="chassisNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="reservationStationCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MovementType", propOrder = {
    "driver",
    "distance"
})
public class MovementType {

    protected DriverType driver;
    protected MovementType.Distance distance;
    @XmlAttribute(name = "movementID")
    protected BigInteger movementID;
    @XmlAttribute(name = "unitNumber")
    protected BigInteger unitNumber;
    @XmlAttribute(name = "rentalAgreementNumber")
    protected BigInteger rentalAgreementNumber;
    @XmlAttribute(name = "vehicleCategoryCode")
    protected String vehicleCategoryCode;
    @XmlAttribute(name = "status")
    protected String status;
    @XmlAttribute(name = "movementType")
    protected String movementType;
    @XmlAttribute(name = "revisionNumber")
    protected BigInteger revisionNumber;
    @XmlAttribute(name = "vehicleModelDescription")
    protected String vehicleModelDescription;
    @XmlAttribute(name = "franchiseeDocNumber")
    protected String franchiseeDocNumber;
    @XmlAttribute(name = "registrationNumber")
    protected String registrationNumber;
    @XmlAttribute(name = "chassisNumber")
    protected String chassisNumber;
    @XmlAttribute(name = "reservationStationCode")
    protected String reservationStationCode;

    /**
     * Gets the value of the driver property.
     * 
     * @return
     *     possible object is
     *     {@link DriverType }
     *     
     */
    public DriverType getDriver() {
        return driver;
    }

    /**
     * Sets the value of the driver property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverType }
     *     
     */
    public void setDriver(DriverType value) {
        this.driver = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     * @return
     *     possible object is
     *     {@link MovementType.Distance }
     *     
     */
    public MovementType.Distance getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     * @param value
     *     allowed object is
     *     {@link MovementType.Distance }
     *     
     */
    public void setDistance(MovementType.Distance value) {
        this.distance = value;
    }

    /**
     * Gets the value of the movementID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMovementID() {
        return movementID;
    }

    /**
     * Sets the value of the movementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMovementID(BigInteger value) {
        this.movementID = value;
    }

    /**
     * Gets the value of the unitNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnitNumber() {
        return unitNumber;
    }

    /**
     * Sets the value of the unitNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnitNumber(BigInteger value) {
        this.unitNumber = value;
    }

    /**
     * Gets the value of the rentalAgreementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRentalAgreementNumber() {
        return rentalAgreementNumber;
    }

    /**
     * Sets the value of the rentalAgreementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRentalAgreementNumber(BigInteger value) {
        this.rentalAgreementNumber = value;
    }

    /**
     * Gets the value of the vehicleCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleCategoryCode() {
        return vehicleCategoryCode;
    }

    /**
     * Sets the value of the vehicleCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleCategoryCode(String value) {
        this.vehicleCategoryCode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the movementType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * Sets the value of the movementType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMovementType(String value) {
        this.movementType = value;
    }

    /**
     * Gets the value of the revisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRevisionNumber() {
        return revisionNumber;
    }

    /**
     * Sets the value of the revisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRevisionNumber(BigInteger value) {
        this.revisionNumber = value;
    }

    /**
     * Gets the value of the vehicleModelDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleModelDescription() {
        return vehicleModelDescription;
    }

    /**
     * Sets the value of the vehicleModelDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleModelDescription(String value) {
        this.vehicleModelDescription = value;
    }

    /**
     * Gets the value of the franchiseeDocNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFranchiseeDocNumber() {
        return franchiseeDocNumber;
    }

    /**
     * Sets the value of the franchiseeDocNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFranchiseeDocNumber(String value) {
        this.franchiseeDocNumber = value;
    }

    /**
     * Gets the value of the registrationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    /**
     * Sets the value of the registrationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationNumber(String value) {
        this.registrationNumber = value;
    }

    /**
     * Gets the value of the chassisNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisNumber() {
        return chassisNumber;
    }

    /**
     * Sets the value of the chassisNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisNumber(String value) {
        this.chassisNumber = value;
    }

    /**
     * Gets the value of the reservationStationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationStationCode() {
        return reservationStationCode;
    }

    /**
     * Sets the value of the reservationStationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationStationCode(String value) {
        this.reservationStationCode = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="mileageUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="mileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Distance {

        @XmlAttribute(name = "mileageUnit")
        protected String mileageUnit;
        @XmlAttribute(name = "mileage")
        protected BigInteger mileage;

        /**
         * Gets the value of the mileageUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMileageUnit() {
            return mileageUnit;
        }

        /**
         * Sets the value of the mileageUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMileageUnit(String value) {
            this.mileageUnit = value;
        }

        /**
         * Gets the value of the mileage property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMileage() {
            return mileage;
        }

        /**
         * Sets the value of the mileage property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMileage(BigInteger value) {
            this.mileage = value;
        }

    }

}
