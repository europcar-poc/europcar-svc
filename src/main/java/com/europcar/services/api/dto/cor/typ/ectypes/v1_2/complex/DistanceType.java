
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DistanceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DistanceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="byRoad" type="{http://www.w3.org/2001/XMLSchema}float" /&gt;
 *       &lt;attribute name="byCrowFlies" type="{http://www.w3.org/2001/XMLSchema}float" /&gt;
 *       &lt;attribute name="mileageUnit" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="mileageUnitLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="mileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="numberOfHours" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="isInTown" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DistanceType")
public class DistanceType {

    @XmlAttribute(name = "byRoad")
    protected Float byRoad;
    @XmlAttribute(name = "byCrowFlies")
    protected Float byCrowFlies;
    @XmlAttribute(name = "mileageUnit")
    protected String mileageUnit;
    @XmlAttribute(name = "mileageUnitLabel")
    protected String mileageUnitLabel;
    @XmlAttribute(name = "mileage")
    protected BigInteger mileage;
    @XmlAttribute(name = "numberOfHours")
    protected BigInteger numberOfHours;
    @XmlAttribute(name = "isInTown")
    protected Boolean isInTown;

    /**
     * Gets the value of the byRoad property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getByRoad() {
        return byRoad;
    }

    /**
     * Sets the value of the byRoad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setByRoad(Float value) {
        this.byRoad = value;
    }

    /**
     * Gets the value of the byCrowFlies property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getByCrowFlies() {
        return byCrowFlies;
    }

    /**
     * Sets the value of the byCrowFlies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setByCrowFlies(Float value) {
        this.byCrowFlies = value;
    }

    /**
     * Gets the value of the mileageUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMileageUnit() {
        return mileageUnit;
    }

    /**
     * Sets the value of the mileageUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMileageUnit(String value) {
        this.mileageUnit = value;
    }

    /**
     * Gets the value of the mileageUnitLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMileageUnitLabel() {
        return mileageUnitLabel;
    }

    /**
     * Sets the value of the mileageUnitLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMileageUnitLabel(String value) {
        this.mileageUnitLabel = value;
    }

    /**
     * Gets the value of the mileage property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMileage() {
        return mileage;
    }

    /**
     * Sets the value of the mileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMileage(BigInteger value) {
        this.mileage = value;
    }

    /**
     * Gets the value of the numberOfHours property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfHours() {
        return numberOfHours;
    }

    /**
     * Sets the value of the numberOfHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfHours(BigInteger value) {
        this.numberOfHours = value;
    }

    /**
     * Gets the value of the isInTown property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsInTown() {
        return isInTown;
    }

    /**
     * Sets the value of the isInTown property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsInTown(Boolean value) {
        this.isInTown = value;
    }

}
