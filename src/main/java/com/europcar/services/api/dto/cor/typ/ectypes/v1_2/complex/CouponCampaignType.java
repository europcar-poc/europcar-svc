
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CouponCampaignType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CouponCampaignType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="brands" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="brand" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}BrandType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="checkoutCountries" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="checkoutCountry" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="sequenceId" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="issuingCountry" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="startCheckoutDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="endCheckoutDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="vehicleCategory" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="startReservationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="endReservationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="minimumRentalDays" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="maxPercentOfTRE" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CouponCampaignType", propOrder = {
    "brands",
    "checkoutCountries"
})
public class CouponCampaignType {

    protected CouponCampaignType.Brands brands;
    protected CouponCampaignType.CheckoutCountries checkoutCountries;
    @XmlAttribute(name = "sequenceId")
    protected BigInteger sequenceId;
    @XmlAttribute(name = "code")
    protected String code;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "issuingCountry")
    protected String issuingCountry;
    @XmlAttribute(name = "startCheckoutDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startCheckoutDate;
    @XmlAttribute(name = "endCheckoutDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endCheckoutDate;
    @XmlAttribute(name = "vehicleCategory")
    protected String vehicleCategory;
    @XmlAttribute(name = "startReservationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startReservationDate;
    @XmlAttribute(name = "endReservationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endReservationDate;
    @XmlAttribute(name = "minimumRentalDays")
    protected BigInteger minimumRentalDays;
    @XmlAttribute(name = "maxPercentOfTRE")
    protected BigInteger maxPercentOfTRE;
    @XmlAttribute(name = "amount")
    protected Double amount;
    @XmlAttribute(name = "currencyCode")
    protected String currencyCode;

    /**
     * Gets the value of the brands property.
     * 
     * @return
     *     possible object is
     *     {@link CouponCampaignType.Brands }
     *     
     */
    public CouponCampaignType.Brands getBrands() {
        return brands;
    }

    /**
     * Sets the value of the brands property.
     * 
     * @param value
     *     allowed object is
     *     {@link CouponCampaignType.Brands }
     *     
     */
    public void setBrands(CouponCampaignType.Brands value) {
        this.brands = value;
    }

    /**
     * Gets the value of the checkoutCountries property.
     * 
     * @return
     *     possible object is
     *     {@link CouponCampaignType.CheckoutCountries }
     *     
     */
    public CouponCampaignType.CheckoutCountries getCheckoutCountries() {
        return checkoutCountries;
    }

    /**
     * Sets the value of the checkoutCountries property.
     * 
     * @param value
     *     allowed object is
     *     {@link CouponCampaignType.CheckoutCountries }
     *     
     */
    public void setCheckoutCountries(CouponCampaignType.CheckoutCountries value) {
        this.checkoutCountries = value;
    }

    /**
     * Gets the value of the sequenceId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceId() {
        return sequenceId;
    }

    /**
     * Sets the value of the sequenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceId(BigInteger value) {
        this.sequenceId = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the issuingCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCountry() {
        return issuingCountry;
    }

    /**
     * Sets the value of the issuingCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCountry(String value) {
        this.issuingCountry = value;
    }

    /**
     * Gets the value of the startCheckoutDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartCheckoutDate() {
        return startCheckoutDate;
    }

    /**
     * Sets the value of the startCheckoutDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartCheckoutDate(XMLGregorianCalendar value) {
        this.startCheckoutDate = value;
    }

    /**
     * Gets the value of the endCheckoutDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndCheckoutDate() {
        return endCheckoutDate;
    }

    /**
     * Sets the value of the endCheckoutDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndCheckoutDate(XMLGregorianCalendar value) {
        this.endCheckoutDate = value;
    }

    /**
     * Gets the value of the vehicleCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleCategory() {
        return vehicleCategory;
    }

    /**
     * Sets the value of the vehicleCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleCategory(String value) {
        this.vehicleCategory = value;
    }

    /**
     * Gets the value of the startReservationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartReservationDate() {
        return startReservationDate;
    }

    /**
     * Sets the value of the startReservationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartReservationDate(XMLGregorianCalendar value) {
        this.startReservationDate = value;
    }

    /**
     * Gets the value of the endReservationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndReservationDate() {
        return endReservationDate;
    }

    /**
     * Sets the value of the endReservationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndReservationDate(XMLGregorianCalendar value) {
        this.endReservationDate = value;
    }

    /**
     * Gets the value of the minimumRentalDays property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumRentalDays() {
        return minimumRentalDays;
    }

    /**
     * Sets the value of the minimumRentalDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumRentalDays(BigInteger value) {
        this.minimumRentalDays = value;
    }

    /**
     * Gets the value of the maxPercentOfTRE property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxPercentOfTRE() {
        return maxPercentOfTRE;
    }

    /**
     * Sets the value of the maxPercentOfTRE property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxPercentOfTRE(BigInteger value) {
        this.maxPercentOfTRE = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAmount(Double value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="brand" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}BrandType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "brand"
    })
    public static class Brands {

        @XmlElement(required = true)
        protected List<BrandType> brand;

        /**
         * Gets the value of the brand property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the brand property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBrand().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BrandType }
         * 
         * 
         */
        public List<BrandType> getBrand() {
            if (brand == null) {
                brand = new ArrayList<BrandType>();
            }
            return this.brand;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="checkoutCountry" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "checkoutCountry"
    })
    public static class CheckoutCountries {

        @XmlElement(required = true)
        protected List<String> checkoutCountry;

        /**
         * Gets the value of the checkoutCountry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the checkoutCountry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCheckoutCountry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getCheckoutCountry() {
            if (checkoutCountry == null) {
                checkoutCountry = new ArrayList<String>();
            }
            return this.checkoutCountry;
        }

    }

}
