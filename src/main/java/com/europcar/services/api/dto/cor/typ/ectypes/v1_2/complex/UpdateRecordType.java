
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateRecordType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateRecordType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="counter" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}CounterType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tracking" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}TrackingType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="changeReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="changeReasonDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="previousTierLevel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="nextTierLevel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="forcedTierLevelCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateRecordType", propOrder = {
    "counter",
    "tracking"
})
public class UpdateRecordType {

    @XmlElement(nillable = true)
    protected List<CounterType> counter;
    protected TrackingType tracking;
    @XmlAttribute(name = "changeReasonCode")
    protected String changeReasonCode;
    @XmlAttribute(name = "changeReasonDescription")
    protected String changeReasonDescription;
    @XmlAttribute(name = "previousTierLevel")
    protected String previousTierLevel;
    @XmlAttribute(name = "nextTierLevel")
    protected String nextTierLevel;
    @XmlAttribute(name = "forcedTierLevelCode")
    protected String forcedTierLevelCode;

    /**
     * Gets the value of the counter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the counter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCounter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CounterType }
     * 
     * 
     */
    public List<CounterType> getCounter() {
        if (counter == null) {
            counter = new ArrayList<CounterType>();
        }
        return this.counter;
    }

    /**
     * Gets the value of the tracking property.
     * 
     * @return
     *     possible object is
     *     {@link TrackingType }
     *     
     */
    public TrackingType getTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrackingType }
     *     
     */
    public void setTracking(TrackingType value) {
        this.tracking = value;
    }

    /**
     * Gets the value of the changeReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeReasonCode() {
        return changeReasonCode;
    }

    /**
     * Sets the value of the changeReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeReasonCode(String value) {
        this.changeReasonCode = value;
    }

    /**
     * Gets the value of the changeReasonDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeReasonDescription() {
        return changeReasonDescription;
    }

    /**
     * Sets the value of the changeReasonDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeReasonDescription(String value) {
        this.changeReasonDescription = value;
    }

    /**
     * Gets the value of the previousTierLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousTierLevel() {
        return previousTierLevel;
    }

    /**
     * Sets the value of the previousTierLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousTierLevel(String value) {
        this.previousTierLevel = value;
    }

    /**
     * Gets the value of the nextTierLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextTierLevel() {
        return nextTierLevel;
    }

    /**
     * Sets the value of the nextTierLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextTierLevel(String value) {
        this.nextTierLevel = value;
    }

    /**
     * Gets the value of the forcedTierLevelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForcedTierLevelCode() {
        return forcedTierLevelCode;
    }

    /**
     * Sets the value of the forcedTierLevelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForcedTierLevelCode(String value) {
        this.forcedTierLevelCode = value;
    }

}
