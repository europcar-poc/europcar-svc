
package com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RentalDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RentalDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="travel" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}TravelType" minOccurs="0"/&gt;
 *         &lt;element name="vehicleCategory" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}VehicleCategoryType" minOccurs="0"/&gt;
 *         &lt;element name="reservation" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="reservationStation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="reservationCountry" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="reservationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *                 &lt;attribute name="reservationCurrency" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="rentalAgreement" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="rentalDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="applicableDateForVAT" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="driverList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="driver" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="legalDocument" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DocumentType" minOccurs="0"/&gt;
 *                             &lt;element name="details" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DriverType" minOccurs="0"/&gt;
 *                             &lt;element name="address" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" minOccurs="0"/&gt;
 *                             &lt;element name="phone" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PhoneType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="isMain" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="isRenter" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="isAdditional" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="meanOfPaymentList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="meanOfPayment" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}MeanOfPaymentDetailsType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="contractId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="businessAccountId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="brand" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RentalDataType", propOrder = {
    "travel",
    "vehicleCategory",
    "reservation",
    "rentalAgreement",
    "driverList",
    "meanOfPaymentList"
})
public class RentalDataType {

    protected TravelType travel;
    protected VehicleCategoryType vehicleCategory;
    protected RentalDataType.Reservation reservation;
    protected RentalDataType.RentalAgreement rentalAgreement;
    protected RentalDataType.DriverList driverList;
    protected RentalDataType.MeanOfPaymentList meanOfPaymentList;
    @XmlAttribute(name = "contractId")
    protected String contractId;
    @XmlAttribute(name = "businessAccountId")
    protected String businessAccountId;
    @XmlAttribute(name = "brand")
    protected String brand;

    /**
     * Gets the value of the travel property.
     * 
     * @return
     *     possible object is
     *     {@link TravelType }
     *     
     */
    public TravelType getTravel() {
        return travel;
    }

    /**
     * Sets the value of the travel property.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelType }
     *     
     */
    public void setTravel(TravelType value) {
        this.travel = value;
    }

    /**
     * Gets the value of the vehicleCategory property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleCategoryType }
     *     
     */
    public VehicleCategoryType getVehicleCategory() {
        return vehicleCategory;
    }

    /**
     * Sets the value of the vehicleCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleCategoryType }
     *     
     */
    public void setVehicleCategory(VehicleCategoryType value) {
        this.vehicleCategory = value;
    }

    /**
     * Gets the value of the reservation property.
     * 
     * @return
     *     possible object is
     *     {@link RentalDataType.Reservation }
     *     
     */
    public RentalDataType.Reservation getReservation() {
        return reservation;
    }

    /**
     * Sets the value of the reservation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RentalDataType.Reservation }
     *     
     */
    public void setReservation(RentalDataType.Reservation value) {
        this.reservation = value;
    }

    /**
     * Gets the value of the rentalAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link RentalDataType.RentalAgreement }
     *     
     */
    public RentalDataType.RentalAgreement getRentalAgreement() {
        return rentalAgreement;
    }

    /**
     * Sets the value of the rentalAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link RentalDataType.RentalAgreement }
     *     
     */
    public void setRentalAgreement(RentalDataType.RentalAgreement value) {
        this.rentalAgreement = value;
    }

    /**
     * Gets the value of the driverList property.
     * 
     * @return
     *     possible object is
     *     {@link RentalDataType.DriverList }
     *     
     */
    public RentalDataType.DriverList getDriverList() {
        return driverList;
    }

    /**
     * Sets the value of the driverList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RentalDataType.DriverList }
     *     
     */
    public void setDriverList(RentalDataType.DriverList value) {
        this.driverList = value;
    }

    /**
     * Gets the value of the meanOfPaymentList property.
     * 
     * @return
     *     possible object is
     *     {@link RentalDataType.MeanOfPaymentList }
     *     
     */
    public RentalDataType.MeanOfPaymentList getMeanOfPaymentList() {
        return meanOfPaymentList;
    }

    /**
     * Sets the value of the meanOfPaymentList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RentalDataType.MeanOfPaymentList }
     *     
     */
    public void setMeanOfPaymentList(RentalDataType.MeanOfPaymentList value) {
        this.meanOfPaymentList = value;
    }

    /**
     * Gets the value of the contractId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * Sets the value of the contractId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractId(String value) {
        this.contractId = value;
    }

    /**
     * Gets the value of the businessAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessAccountId() {
        return businessAccountId;
    }

    /**
     * Sets the value of the businessAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessAccountId(String value) {
        this.businessAccountId = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="driver" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="legalDocument" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DocumentType" minOccurs="0"/&gt;
     *                   &lt;element name="details" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DriverType" minOccurs="0"/&gt;
     *                   &lt;element name="address" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" minOccurs="0"/&gt;
     *                   &lt;element name="phone" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PhoneType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="isMain" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="isRenter" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="isAdditional" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "driver"
    })
    public static class DriverList {

        @XmlElement(nillable = true)
        protected List<RentalDataType.DriverList.Driver> driver;

        /**
         * Gets the value of the driver property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the driver property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDriver().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RentalDataType.DriverList.Driver }
         * 
         * 
         */
        public List<RentalDataType.DriverList.Driver> getDriver() {
            if (driver == null) {
                driver = new ArrayList<RentalDataType.DriverList.Driver>();
            }
            return this.driver;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="legalDocument" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DocumentType" minOccurs="0"/&gt;
         *         &lt;element name="details" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}DriverType" minOccurs="0"/&gt;
         *         &lt;element name="address" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}AddressType" minOccurs="0"/&gt;
         *         &lt;element name="phone" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}PhoneType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="isMain" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="isRenter" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="isAdditional" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "legalDocument",
            "details",
            "address",
            "phone"
        })
        public static class Driver {

            protected DocumentType legalDocument;
            protected DriverType details;
            protected AddressType address;
            protected PhoneType phone;
            @XmlAttribute(name = "isMain")
            protected Boolean isMain;
            @XmlAttribute(name = "isRenter")
            protected Boolean isRenter;
            @XmlAttribute(name = "isAdditional")
            protected Boolean isAdditional;

            /**
             * Gets the value of the legalDocument property.
             * 
             * @return
             *     possible object is
             *     {@link DocumentType }
             *     
             */
            public DocumentType getLegalDocument() {
                return legalDocument;
            }

            /**
             * Sets the value of the legalDocument property.
             * 
             * @param value
             *     allowed object is
             *     {@link DocumentType }
             *     
             */
            public void setLegalDocument(DocumentType value) {
                this.legalDocument = value;
            }

            /**
             * Gets the value of the details property.
             * 
             * @return
             *     possible object is
             *     {@link DriverType }
             *     
             */
            public DriverType getDetails() {
                return details;
            }

            /**
             * Sets the value of the details property.
             * 
             * @param value
             *     allowed object is
             *     {@link DriverType }
             *     
             */
            public void setDetails(DriverType value) {
                this.details = value;
            }

            /**
             * Gets the value of the address property.
             * 
             * @return
             *     possible object is
             *     {@link AddressType }
             *     
             */
            public AddressType getAddress() {
                return address;
            }

            /**
             * Sets the value of the address property.
             * 
             * @param value
             *     allowed object is
             *     {@link AddressType }
             *     
             */
            public void setAddress(AddressType value) {
                this.address = value;
            }

            /**
             * Gets the value of the phone property.
             * 
             * @return
             *     possible object is
             *     {@link PhoneType }
             *     
             */
            public PhoneType getPhone() {
                return phone;
            }

            /**
             * Sets the value of the phone property.
             * 
             * @param value
             *     allowed object is
             *     {@link PhoneType }
             *     
             */
            public void setPhone(PhoneType value) {
                this.phone = value;
            }

            /**
             * Gets the value of the isMain property.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isIsMain() {
                return isMain;
            }

            /**
             * Sets the value of the isMain property.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setIsMain(Boolean value) {
                this.isMain = value;
            }

            /**
             * Gets the value of the isRenter property.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isIsRenter() {
                return isRenter;
            }

            /**
             * Sets the value of the isRenter property.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setIsRenter(Boolean value) {
                this.isRenter = value;
            }

            /**
             * Gets the value of the isAdditional property.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isIsAdditional() {
                return isAdditional;
            }

            /**
             * Sets the value of the isAdditional property.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setIsAdditional(Boolean value) {
                this.isAdditional = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="meanOfPayment" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}MeanOfPaymentDetailsType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "meanOfPayment"
    })
    public static class MeanOfPaymentList {

        @XmlElement(nillable = true)
        protected List<MeanOfPaymentDetailsType> meanOfPayment;

        /**
         * Gets the value of the meanOfPayment property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the meanOfPayment property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMeanOfPayment().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MeanOfPaymentDetailsType }
         * 
         * 
         */
        public List<MeanOfPaymentDetailsType> getMeanOfPayment() {
            if (meanOfPayment == null) {
                meanOfPayment = new ArrayList<MeanOfPaymentDetailsType>();
            }
            return this.meanOfPayment;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="rentalDuration" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="applicableDateForVAT" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RentalAgreement {

        @XmlAttribute(name = "rentalDuration")
        protected BigInteger rentalDuration;
        @XmlAttribute(name = "applicableDateForVAT")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar applicableDateForVAT;

        /**
         * Gets the value of the rentalDuration property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRentalDuration() {
            return rentalDuration;
        }

        /**
         * Sets the value of the rentalDuration property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRentalDuration(BigInteger value) {
            this.rentalDuration = value;
        }

        /**
         * Gets the value of the applicableDateForVAT property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getApplicableDateForVAT() {
            return applicableDateForVAT;
        }

        /**
         * Sets the value of the applicableDateForVAT property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setApplicableDateForVAT(XMLGregorianCalendar value) {
            this.applicableDateForVAT = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="reservationStation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="reservationCountry" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="reservationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
     *       &lt;attribute name="reservationCurrency" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Reservation {

        @XmlAttribute(name = "reservationStation")
        protected String reservationStation;
        @XmlAttribute(name = "reservationCountry")
        protected String reservationCountry;
        @XmlAttribute(name = "reservationDate")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar reservationDate;
        @XmlAttribute(name = "reservationCurrency")
        protected String reservationCurrency;

        /**
         * Gets the value of the reservationStation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservationStation() {
            return reservationStation;
        }

        /**
         * Sets the value of the reservationStation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservationStation(String value) {
            this.reservationStation = value;
        }

        /**
         * Gets the value of the reservationCountry property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservationCountry() {
            return reservationCountry;
        }

        /**
         * Sets the value of the reservationCountry property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservationCountry(String value) {
            this.reservationCountry = value;
        }

        /**
         * Gets the value of the reservationDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getReservationDate() {
            return reservationDate;
        }

        /**
         * Sets the value of the reservationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setReservationDate(XMLGregorianCalendar value) {
            this.reservationDate = value;
        }

        /**
         * Gets the value of the reservationCurrency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservationCurrency() {
            return reservationCurrency;
        }

        /**
         * Sets the value of the reservationCurrency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservationCurrency(String value) {
            this.reservationCurrency = value;
        }

    }

}
