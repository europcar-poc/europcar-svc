
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ErrorMessageListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ResponseContextType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.WarningMessageListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.customer.CustomerDTO;


/**
 * <p>Java class for ViewCustomerDetailsRSType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ViewCustomerDetailsRSType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="errorList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ErrorMessageListType" minOccurs="0"/&gt;
 *         &lt;element name="responseContext" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ResponseContextType" minOccurs="0"/&gt;
 *         &lt;element name="warningList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}WarningMessageListType" minOccurs="0"/&gt;
 *         &lt;element name="response" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customer" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/customer}CustomerDTO" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ViewCustomerDetailsRSType", propOrder = {
    "errorList",
    "responseContext",
    "warningList",
    "response"
})
public class ViewCustomerDetailsRSType {

    protected ErrorMessageListType errorList;
    protected ResponseContextType responseContext;
    protected WarningMessageListType warningList;
    protected ViewCustomerDetailsRSType.Response response;

    /**
     * Gets the value of the errorList property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorMessageListType }
     *     
     */
    public ErrorMessageListType getErrorList() {
        return errorList;
    }

    /**
     * Sets the value of the errorList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorMessageListType }
     *     
     */
    public void setErrorList(ErrorMessageListType value) {
        this.errorList = value;
    }

    /**
     * Gets the value of the responseContext property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseContextType }
     *     
     */
    public ResponseContextType getResponseContext() {
        return responseContext;
    }

    /**
     * Sets the value of the responseContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseContextType }
     *     
     */
    public void setResponseContext(ResponseContextType value) {
        this.responseContext = value;
    }

    /**
     * Gets the value of the warningList property.
     * 
     * @return
     *     possible object is
     *     {@link WarningMessageListType }
     *     
     */
    public WarningMessageListType getWarningList() {
        return warningList;
    }

    /**
     * Sets the value of the warningList property.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningMessageListType }
     *     
     */
    public void setWarningList(WarningMessageListType value) {
        this.warningList = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ViewCustomerDetailsRSType.Response }
     *     
     */
    public ViewCustomerDetailsRSType.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ViewCustomerDetailsRSType.Response }
     *     
     */
    public void setResponse(ViewCustomerDetailsRSType.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="customer" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/customer}CustomerDTO" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customer"
    })
    public static class Response {

        protected CustomerDTO customer;

        /**
         * Gets the value of the customer property.
         * 
         * @return
         *     possible object is
         *     {@link CustomerDTO }
         *     
         */
        public CustomerDTO getCustomer() {
            return customer;
        }

        /**
         * Sets the value of the customer property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustomerDTO }
         *     
         */
        public void setCustomer(CustomerDTO value) {
            this.customer = value;
        }

    }

}
