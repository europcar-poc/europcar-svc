
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.europcar.services.api.mkt.cust.v1_0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AmendCustomerRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "amendCustomerRQ");
    private final static QName _AmendCustomerRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "amendCustomerRS");
    private final static QName _CoBrandPrivilegeRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "coBrandPrivilegeRQ");
    private final static QName _EnrollCustomerRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "enrollCustomerRQ");
    private final static QName _EnrollCustomerRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "enrollCustomerRS");
    private final static QName _EnrollCustomerToPrivilegeRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "enrollCustomerToPrivilegeRQ");
    private final static QName _EnrollCustomerToPrivilegeRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "enrollCustomerToPrivilegeRS");
    private final static QName _RegisterCustomerRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "registerCustomerRQ");
    private final static QName _RegisterCustomerRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "registerCustomerRS");
    private final static QName _SearchCustomerRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "searchCustomerRQ");
    private final static QName _SearchCustomerRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "searchCustomerRS");
    private final static QName _SearchDriverRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "searchDriverRQ");
    private final static QName _SearchDriverRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "searchDriverRS");
    private final static QName _SetDriverServiceRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "setDriverServiceRQ");
    private final static QName _SetDriverServiceRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "setDriverServiceRS");
    private final static QName _ViewCustomerDetailsRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "viewCustomerDetailsRQ");
    private final static QName _ViewCustomerDetailsRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "viewCustomerDetailsRS");
    private final static QName _ViewCustomerRQ_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "viewCustomerRQ");
    private final static QName _ViewCustomerRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "viewCustomerRS");
    private final static QName _CoBrandPrivilegeRS_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "CoBrandPrivilegeRS");
    private final static QName _Driverid_QNAME = new QName("http://services.europcar.com/api/mkt/cust/v1_0", "driverid");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.europcar.services.api.mkt.cust.v1_0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CoBrandPrivilegeRSType }
     * 
     */
    public CoBrandPrivilegeRSType createCoBrandPrivilegeRSType() {
        return new CoBrandPrivilegeRSType();
    }

    /**
     * Create an instance of {@link ViewCustomerRSType }
     * 
     */
    public ViewCustomerRSType createViewCustomerRSType() {
        return new ViewCustomerRSType();
    }

    /**
     * Create an instance of {@link ViewCustomerRQType }
     * 
     */
    public ViewCustomerRQType createViewCustomerRQType() {
        return new ViewCustomerRQType();
    }

    /**
     * Create an instance of {@link ViewCustomerRQType.Request }
     * 
     */
    public ViewCustomerRQType.Request createViewCustomerRQTypeRequest() {
        return new ViewCustomerRQType.Request();
    }

    /**
     * Create an instance of {@link ViewCustomerDetailsRSType }
     * 
     */
    public ViewCustomerDetailsRSType createViewCustomerDetailsRSType() {
        return new ViewCustomerDetailsRSType();
    }

    /**
     * Create an instance of {@link ViewCustomerDetailsRQType }
     * 
     */
    public ViewCustomerDetailsRQType createViewCustomerDetailsRQType() {
        return new ViewCustomerDetailsRQType();
    }

    /**
     * Create an instance of {@link ViewCustomerDetailsRQType.Request }
     * 
     */
    public ViewCustomerDetailsRQType.Request createViewCustomerDetailsRQTypeRequest() {
        return new ViewCustomerDetailsRQType.Request();
    }

    /**
     * Create an instance of {@link SetDriverServiceRSType }
     * 
     */
    public SetDriverServiceRSType createSetDriverServiceRSType() {
        return new SetDriverServiceRSType();
    }

    /**
     * Create an instance of {@link SetDriverServiceRQType }
     * 
     */
    public SetDriverServiceRQType createSetDriverServiceRQType() {
        return new SetDriverServiceRQType();
    }

    /**
     * Create an instance of {@link SearchDriverRSType }
     * 
     */
    public SearchDriverRSType createSearchDriverRSType() {
        return new SearchDriverRSType();
    }

    /**
     * Create an instance of {@link SearchDriverRQType }
     * 
     */
    public SearchDriverRQType createSearchDriverRQType() {
        return new SearchDriverRQType();
    }

    /**
     * Create an instance of {@link SearchDriverRQType.Request }
     * 
     */
    public SearchDriverRQType.Request createSearchDriverRQTypeRequest() {
        return new SearchDriverRQType.Request();
    }

    /**
     * Create an instance of {@link SearchCustomerRSType }
     * 
     */
    public SearchCustomerRSType createSearchCustomerRSType() {
        return new SearchCustomerRSType();
    }

    /**
     * Create an instance of {@link SearchCustomerRQType }
     * 
     */
    public SearchCustomerRQType createSearchCustomerRQType() {
        return new SearchCustomerRQType();
    }

    /**
     * Create an instance of {@link SearchCustomerRQType.Request }
     * 
     */
    public SearchCustomerRQType.Request createSearchCustomerRQTypeRequest() {
        return new SearchCustomerRQType.Request();
    }

    /**
     * Create an instance of {@link RegisterCustomerRSType }
     * 
     */
    public RegisterCustomerRSType createRegisterCustomerRSType() {
        return new RegisterCustomerRSType();
    }

    /**
     * Create an instance of {@link RegisterCustomerRQType }
     * 
     */
    public RegisterCustomerRQType createRegisterCustomerRQType() {
        return new RegisterCustomerRQType();
    }

    /**
     * Create an instance of {@link RegisterCustomerRQType.Request }
     * 
     */
    public RegisterCustomerRQType.Request createRegisterCustomerRQTypeRequest() {
        return new RegisterCustomerRQType.Request();
    }

    /**
     * Create an instance of {@link EnrollCustomerToPrivilegeRSType }
     * 
     */
    public EnrollCustomerToPrivilegeRSType createEnrollCustomerToPrivilegeRSType() {
        return new EnrollCustomerToPrivilegeRSType();
    }

    /**
     * Create an instance of {@link EnrollCustomerToPrivilegeRQType }
     * 
     */
    public EnrollCustomerToPrivilegeRQType createEnrollCustomerToPrivilegeRQType() {
        return new EnrollCustomerToPrivilegeRQType();
    }

    /**
     * Create an instance of {@link EnrollCustomerRSType }
     * 
     */
    public EnrollCustomerRSType createEnrollCustomerRSType() {
        return new EnrollCustomerRSType();
    }

    /**
     * Create an instance of {@link EnrollCustomerRQType }
     * 
     */
    public EnrollCustomerRQType createEnrollCustomerRQType() {
        return new EnrollCustomerRQType();
    }

    /**
     * Create an instance of {@link EnrollCustomerRQType.Request }
     * 
     */
    public EnrollCustomerRQType.Request createEnrollCustomerRQTypeRequest() {
        return new EnrollCustomerRQType.Request();
    }

    /**
     * Create an instance of {@link CoBrandPrivilegeRQType }
     * 
     */
    public CoBrandPrivilegeRQType createCoBrandPrivilegeRQType() {
        return new CoBrandPrivilegeRQType();
    }

    /**
     * Create an instance of {@link CoBrandPrivilegeRQType.Request }
     * 
     */
    public CoBrandPrivilegeRQType.Request createCoBrandPrivilegeRQTypeRequest() {
        return new CoBrandPrivilegeRQType.Request();
    }

    /**
     * Create an instance of {@link AmendCustomerRSType }
     * 
     */
    public AmendCustomerRSType createAmendCustomerRSType() {
        return new AmendCustomerRSType();
    }

    /**
     * Create an instance of {@link AmendCustomerRQType }
     * 
     */
    public AmendCustomerRQType createAmendCustomerRQType() {
        return new AmendCustomerRQType();
    }

    /**
     * Create an instance of {@link AmendCustomerRQType.Request }
     * 
     */
    public AmendCustomerRQType.Request createAmendCustomerRQTypeRequest() {
        return new AmendCustomerRQType.Request();
    }

    /**
     * Create an instance of {@link SearchCriterionForDriverType }
     * 
     */
    public SearchCriterionForDriverType createSearchCriterionForDriverType() {
        return new SearchCriterionForDriverType();
    }

    /**
     * Create an instance of {@link ReturnedFragmentForDriverType }
     * 
     */
    public ReturnedFragmentForDriverType createReturnedFragmentForDriverType() {
        return new ReturnedFragmentForDriverType();
    }

    /**
     * Create an instance of {@link CoBrandPrivilegeRSType.Response }
     * 
     */
    public CoBrandPrivilegeRSType.Response createCoBrandPrivilegeRSTypeResponse() {
        return new CoBrandPrivilegeRSType.Response();
    }

    /**
     * Create an instance of {@link ViewCustomerRSType.Response }
     * 
     */
    public ViewCustomerRSType.Response createViewCustomerRSTypeResponse() {
        return new ViewCustomerRSType.Response();
    }

    /**
     * Create an instance of {@link ViewCustomerRQType.Request.OptionList }
     * 
     */
    public ViewCustomerRQType.Request.OptionList createViewCustomerRQTypeRequestOptionList() {
        return new ViewCustomerRQType.Request.OptionList();
    }

    /**
     * Create an instance of {@link ViewCustomerDetailsRSType.Response }
     * 
     */
    public ViewCustomerDetailsRSType.Response createViewCustomerDetailsRSTypeResponse() {
        return new ViewCustomerDetailsRSType.Response();
    }

    /**
     * Create an instance of {@link ViewCustomerDetailsRQType.Request.OptionList }
     * 
     */
    public ViewCustomerDetailsRQType.Request.OptionList createViewCustomerDetailsRQTypeRequestOptionList() {
        return new ViewCustomerDetailsRQType.Request.OptionList();
    }

    /**
     * Create an instance of {@link SetDriverServiceRSType.Response }
     * 
     */
    public SetDriverServiceRSType.Response createSetDriverServiceRSTypeResponse() {
        return new SetDriverServiceRSType.Response();
    }

    /**
     * Create an instance of {@link SetDriverServiceRQType.Request }
     * 
     */
    public SetDriverServiceRQType.Request createSetDriverServiceRQTypeRequest() {
        return new SetDriverServiceRQType.Request();
    }

    /**
     * Create an instance of {@link SearchDriverRSType.Response }
     * 
     */
    public SearchDriverRSType.Response createSearchDriverRSTypeResponse() {
        return new SearchDriverRSType.Response();
    }

    /**
     * Create an instance of {@link SearchDriverRQType.Request.SearchCriteria }
     * 
     */
    public SearchDriverRQType.Request.SearchCriteria createSearchDriverRQTypeRequestSearchCriteria() {
        return new SearchDriverRQType.Request.SearchCriteria();
    }

    /**
     * Create an instance of {@link SearchDriverRQType.Request.ReturnedFragments }
     * 
     */
    public SearchDriverRQType.Request.ReturnedFragments createSearchDriverRQTypeRequestReturnedFragments() {
        return new SearchDriverRQType.Request.ReturnedFragments();
    }

    /**
     * Create an instance of {@link SearchCustomerRSType.Response }
     * 
     */
    public SearchCustomerRSType.Response createSearchCustomerRSTypeResponse() {
        return new SearchCustomerRSType.Response();
    }

    /**
     * Create an instance of {@link SearchCustomerRQType.Request.OptionList }
     * 
     */
    public SearchCustomerRQType.Request.OptionList createSearchCustomerRQTypeRequestOptionList() {
        return new SearchCustomerRQType.Request.OptionList();
    }

    /**
     * Create an instance of {@link RegisterCustomerRSType.Response }
     * 
     */
    public RegisterCustomerRSType.Response createRegisterCustomerRSTypeResponse() {
        return new RegisterCustomerRSType.Response();
    }

    /**
     * Create an instance of {@link RegisterCustomerRQType.Request.OptionList }
     * 
     */
    public RegisterCustomerRQType.Request.OptionList createRegisterCustomerRQTypeRequestOptionList() {
        return new RegisterCustomerRQType.Request.OptionList();
    }

    /**
     * Create an instance of {@link EnrollCustomerToPrivilegeRSType.Response }
     * 
     */
    public EnrollCustomerToPrivilegeRSType.Response createEnrollCustomerToPrivilegeRSTypeResponse() {
        return new EnrollCustomerToPrivilegeRSType.Response();
    }

    /**
     * Create an instance of {@link EnrollCustomerToPrivilegeRQType.Request }
     * 
     */
    public EnrollCustomerToPrivilegeRQType.Request createEnrollCustomerToPrivilegeRQTypeRequest() {
        return new EnrollCustomerToPrivilegeRQType.Request();
    }

    /**
     * Create an instance of {@link EnrollCustomerRSType.Response }
     * 
     */
    public EnrollCustomerRSType.Response createEnrollCustomerRSTypeResponse() {
        return new EnrollCustomerRSType.Response();
    }

    /**
     * Create an instance of {@link EnrollCustomerRQType.Request.OptionList }
     * 
     */
    public EnrollCustomerRQType.Request.OptionList createEnrollCustomerRQTypeRequestOptionList() {
        return new EnrollCustomerRQType.Request.OptionList();
    }

    /**
     * Create an instance of {@link CoBrandPrivilegeRQType.Request.OptionList }
     * 
     */
    public CoBrandPrivilegeRQType.Request.OptionList createCoBrandPrivilegeRQTypeRequestOptionList() {
        return new CoBrandPrivilegeRQType.Request.OptionList();
    }

    /**
     * Create an instance of {@link AmendCustomerRSType.Response }
     * 
     */
    public AmendCustomerRSType.Response createAmendCustomerRSTypeResponse() {
        return new AmendCustomerRSType.Response();
    }

    /**
     * Create an instance of {@link AmendCustomerRQType.Request.OptionList }
     * 
     */
    public AmendCustomerRQType.Request.OptionList createAmendCustomerRQTypeRequestOptionList() {
        return new AmendCustomerRQType.Request.OptionList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmendCustomerRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "amendCustomerRQ")
    public JAXBElement<AmendCustomerRQType> createAmendCustomerRQ(AmendCustomerRQType value) {
        return new JAXBElement<AmendCustomerRQType>(_AmendCustomerRQ_QNAME, AmendCustomerRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmendCustomerRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "amendCustomerRS")
    public JAXBElement<AmendCustomerRSType> createAmendCustomerRS(AmendCustomerRSType value) {
        return new JAXBElement<AmendCustomerRSType>(_AmendCustomerRS_QNAME, AmendCustomerRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoBrandPrivilegeRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "coBrandPrivilegeRQ")
    public JAXBElement<CoBrandPrivilegeRQType> createCoBrandPrivilegeRQ(CoBrandPrivilegeRQType value) {
        return new JAXBElement<CoBrandPrivilegeRQType>(_CoBrandPrivilegeRQ_QNAME, CoBrandPrivilegeRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnrollCustomerRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "enrollCustomerRQ")
    public JAXBElement<EnrollCustomerRQType> createEnrollCustomerRQ(EnrollCustomerRQType value) {
        return new JAXBElement<EnrollCustomerRQType>(_EnrollCustomerRQ_QNAME, EnrollCustomerRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnrollCustomerRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "enrollCustomerRS")
    public JAXBElement<EnrollCustomerRSType> createEnrollCustomerRS(EnrollCustomerRSType value) {
        return new JAXBElement<EnrollCustomerRSType>(_EnrollCustomerRS_QNAME, EnrollCustomerRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnrollCustomerToPrivilegeRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "enrollCustomerToPrivilegeRQ")
    public JAXBElement<EnrollCustomerToPrivilegeRQType> createEnrollCustomerToPrivilegeRQ(EnrollCustomerToPrivilegeRQType value) {
        return new JAXBElement<EnrollCustomerToPrivilegeRQType>(_EnrollCustomerToPrivilegeRQ_QNAME, EnrollCustomerToPrivilegeRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnrollCustomerToPrivilegeRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "enrollCustomerToPrivilegeRS")
    public JAXBElement<EnrollCustomerToPrivilegeRSType> createEnrollCustomerToPrivilegeRS(EnrollCustomerToPrivilegeRSType value) {
        return new JAXBElement<EnrollCustomerToPrivilegeRSType>(_EnrollCustomerToPrivilegeRS_QNAME, EnrollCustomerToPrivilegeRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterCustomerRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "registerCustomerRQ")
    public JAXBElement<RegisterCustomerRQType> createRegisterCustomerRQ(RegisterCustomerRQType value) {
        return new JAXBElement<RegisterCustomerRQType>(_RegisterCustomerRQ_QNAME, RegisterCustomerRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterCustomerRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "registerCustomerRS")
    public JAXBElement<RegisterCustomerRSType> createRegisterCustomerRS(RegisterCustomerRSType value) {
        return new JAXBElement<RegisterCustomerRSType>(_RegisterCustomerRS_QNAME, RegisterCustomerRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "searchCustomerRQ")
    public JAXBElement<SearchCustomerRQType> createSearchCustomerRQ(SearchCustomerRQType value) {
        return new JAXBElement<SearchCustomerRQType>(_SearchCustomerRQ_QNAME, SearchCustomerRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "searchCustomerRS")
    public JAXBElement<SearchCustomerRSType> createSearchCustomerRS(SearchCustomerRSType value) {
        return new JAXBElement<SearchCustomerRSType>(_SearchCustomerRS_QNAME, SearchCustomerRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchDriverRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "searchDriverRQ")
    public JAXBElement<SearchDriverRQType> createSearchDriverRQ(SearchDriverRQType value) {
        return new JAXBElement<SearchDriverRQType>(_SearchDriverRQ_QNAME, SearchDriverRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchDriverRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "searchDriverRS")
    public JAXBElement<SearchDriverRSType> createSearchDriverRS(SearchDriverRSType value) {
        return new JAXBElement<SearchDriverRSType>(_SearchDriverRS_QNAME, SearchDriverRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDriverServiceRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "setDriverServiceRQ")
    public JAXBElement<SetDriverServiceRQType> createSetDriverServiceRQ(SetDriverServiceRQType value) {
        return new JAXBElement<SetDriverServiceRQType>(_SetDriverServiceRQ_QNAME, SetDriverServiceRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDriverServiceRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "setDriverServiceRS")
    public JAXBElement<SetDriverServiceRSType> createSetDriverServiceRS(SetDriverServiceRSType value) {
        return new JAXBElement<SetDriverServiceRSType>(_SetDriverServiceRS_QNAME, SetDriverServiceRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewCustomerDetailsRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "viewCustomerDetailsRQ")
    public JAXBElement<ViewCustomerDetailsRQType> createViewCustomerDetailsRQ(ViewCustomerDetailsRQType value) {
        return new JAXBElement<ViewCustomerDetailsRQType>(_ViewCustomerDetailsRQ_QNAME, ViewCustomerDetailsRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewCustomerDetailsRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "viewCustomerDetailsRS")
    public JAXBElement<ViewCustomerDetailsRSType> createViewCustomerDetailsRS(ViewCustomerDetailsRSType value) {
        return new JAXBElement<ViewCustomerDetailsRSType>(_ViewCustomerDetailsRS_QNAME, ViewCustomerDetailsRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewCustomerRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "viewCustomerRQ")
    public JAXBElement<ViewCustomerRQType> createViewCustomerRQ(ViewCustomerRQType value) {
        return new JAXBElement<ViewCustomerRQType>(_ViewCustomerRQ_QNAME, ViewCustomerRQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewCustomerRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "viewCustomerRS")
    public JAXBElement<ViewCustomerRSType> createViewCustomerRS(ViewCustomerRSType value) {
        return new JAXBElement<ViewCustomerRSType>(_ViewCustomerRS_QNAME, ViewCustomerRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoBrandPrivilegeRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "CoBrandPrivilegeRS")
    public JAXBElement<CoBrandPrivilegeRSType> createCoBrandPrivilegeRS(CoBrandPrivilegeRSType value) {
        return new JAXBElement<CoBrandPrivilegeRSType>(_CoBrandPrivilegeRS_QNAME, CoBrandPrivilegeRSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.europcar.com/api/mkt/cust/v1_0", name = "driverid")
    public JAXBElement<Long> createDriverid(Long value) {
        return new JAXBElement<Long>(_Driverid_QNAME, Long.class, null, value);
    }

}
