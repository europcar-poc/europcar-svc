
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for ReturnedFragmentForDriverType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReturnedFragmentForDriverType"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;boolean"&gt;
 *       &lt;attribute name="key" use="required" type="{http://services.europcar.com/api/mkt/cust/v1_0}AllowedReturnedFragmentForDriverType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReturnedFragmentForDriverType", propOrder = {
    "value"
})
public class ReturnedFragmentForDriverType {

    @XmlValue
    protected boolean value;
    @XmlAttribute(name = "key", required = true)
    protected AllowedReturnedFragmentForDriverType key;

    /**
     * Gets the value of the value property.
     * 
     */
    public boolean isValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     */
    public void setValue(boolean value) {
        this.value = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link AllowedReturnedFragmentForDriverType }
     *     
     */
    public AllowedReturnedFragmentForDriverType getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllowedReturnedFragmentForDriverType }
     *     
     */
    public void setKey(AllowedReturnedFragmentForDriverType value) {
        this.key = value;
    }

}
