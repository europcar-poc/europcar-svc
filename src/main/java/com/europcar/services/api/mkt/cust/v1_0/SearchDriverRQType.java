
package com.europcar.services.api.mkt.cust.v1_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.RequestContextType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.customer.CustomerDTO;


/**
 * <p>Java class for SearchDriverRQType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchDriverRQType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestContext" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}RequestContextType" minOccurs="0"/&gt;
 *         &lt;element name="request"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customer" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/customer}CustomerDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="searchCriteria"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="searchCriterion" type="{http://services.europcar.com/api/mkt/cust/v1_0}SearchCriterionForDriverType"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="returnedFragments"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="returnedFragment" type="{http://services.europcar.com/api/mkt/cust/v1_0}ReturnedFragmentForDriverType" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchDriverRQType", propOrder = {
    "requestContext",
    "request"
})
public class SearchDriverRQType {

    protected RequestContextType requestContext;
    @XmlElement(required = true)
    protected SearchDriverRQType.Request request;

    /**
     * Gets the value of the requestContext property.
     * 
     * @return
     *     possible object is
     *     {@link RequestContextType }
     *     
     */
    public RequestContextType getRequestContext() {
        return requestContext;
    }

    /**
     * Sets the value of the requestContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestContextType }
     *     
     */
    public void setRequestContext(RequestContextType value) {
        this.requestContext = value;
    }

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDriverRQType.Request }
     *     
     */
    public SearchDriverRQType.Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDriverRQType.Request }
     *     
     */
    public void setRequest(SearchDriverRQType.Request value) {
        this.request = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="customer" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/customer}CustomerDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="searchCriteria"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="searchCriterion" type="{http://services.europcar.com/api/mkt/cust/v1_0}SearchCriterionForDriverType"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="returnedFragments"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="returnedFragment" type="{http://services.europcar.com/api/mkt/cust/v1_0}ReturnedFragmentForDriverType" maxOccurs="unbounded"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customer",
        "searchCriteria",
        "returnedFragments"
    })
    public static class Request {

        @XmlElement(nillable = true)
        protected List<CustomerDTO> customer;
        @XmlElement(required = true)
        protected SearchDriverRQType.Request.SearchCriteria searchCriteria;
        @XmlElement(required = true)
        protected SearchDriverRQType.Request.ReturnedFragments returnedFragments;

        /**
         * Gets the value of the customer property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the customer property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCustomer().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CustomerDTO }
         * 
         * 
         */
        public List<CustomerDTO> getCustomer() {
            if (customer == null) {
                customer = new ArrayList<CustomerDTO>();
            }
            return this.customer;
        }

        /**
         * Gets the value of the searchCriteria property.
         * 
         * @return
         *     possible object is
         *     {@link SearchDriverRQType.Request.SearchCriteria }
         *     
         */
        public SearchDriverRQType.Request.SearchCriteria getSearchCriteria() {
            return searchCriteria;
        }

        /**
         * Sets the value of the searchCriteria property.
         * 
         * @param value
         *     allowed object is
         *     {@link SearchDriverRQType.Request.SearchCriteria }
         *     
         */
        public void setSearchCriteria(SearchDriverRQType.Request.SearchCriteria value) {
            this.searchCriteria = value;
        }

        /**
         * Gets the value of the returnedFragments property.
         * 
         * @return
         *     possible object is
         *     {@link SearchDriverRQType.Request.ReturnedFragments }
         *     
         */
        public SearchDriverRQType.Request.ReturnedFragments getReturnedFragments() {
            return returnedFragments;
        }

        /**
         * Sets the value of the returnedFragments property.
         * 
         * @param value
         *     allowed object is
         *     {@link SearchDriverRQType.Request.ReturnedFragments }
         *     
         */
        public void setReturnedFragments(SearchDriverRQType.Request.ReturnedFragments value) {
            this.returnedFragments = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="returnedFragment" type="{http://services.europcar.com/api/mkt/cust/v1_0}ReturnedFragmentForDriverType" maxOccurs="unbounded"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "returnedFragment"
        })
        public static class ReturnedFragments {

            @XmlElement(required = true)
            protected List<ReturnedFragmentForDriverType> returnedFragment;

            /**
             * Gets the value of the returnedFragment property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the returnedFragment property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getReturnedFragment().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ReturnedFragmentForDriverType }
             * 
             * 
             */
            public List<ReturnedFragmentForDriverType> getReturnedFragment() {
                if (returnedFragment == null) {
                    returnedFragment = new ArrayList<ReturnedFragmentForDriverType>();
                }
                return this.returnedFragment;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="searchCriterion" type="{http://services.europcar.com/api/mkt/cust/v1_0}SearchCriterionForDriverType"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "searchCriterion"
        })
        public static class SearchCriteria {

            @XmlElement(required = true)
            protected SearchCriterionForDriverType searchCriterion;

            /**
             * Gets the value of the searchCriterion property.
             * 
             * @return
             *     possible object is
             *     {@link SearchCriterionForDriverType }
             *     
             */
            public SearchCriterionForDriverType getSearchCriterion() {
                return searchCriterion;
            }

            /**
             * Sets the value of the searchCriterion property.
             * 
             * @param value
             *     allowed object is
             *     {@link SearchCriterionForDriverType }
             *     
             */
            public void setSearchCriterion(SearchCriterionForDriverType value) {
                this.searchCriterion = value;
            }

        }

    }

}
