
package com.europcar.services.api.mkt.cust.v1_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ErrorMessageListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ResponseContextType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.WarningMessageListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.customer.CustomerDTO;


/**
 * <p>Java class for SearchDriverRSType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchDriverRSType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="errorList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ErrorMessageListType" minOccurs="0"/&gt;
 *         &lt;element name="responseContext" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ResponseContextType" minOccurs="0"/&gt;
 *         &lt;element name="warningList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}WarningMessageListType" minOccurs="0"/&gt;
 *         &lt;element name="response" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customer" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/customer}CustomerDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchDriverRSType", propOrder = {
    "errorList",
    "responseContext",
    "warningList",
    "response"
})
public class SearchDriverRSType {

    protected ErrorMessageListType errorList;
    protected ResponseContextType responseContext;
    protected WarningMessageListType warningList;
    protected SearchDriverRSType.Response response;

    /**
     * Gets the value of the errorList property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorMessageListType }
     *     
     */
    public ErrorMessageListType getErrorList() {
        return errorList;
    }

    /**
     * Sets the value of the errorList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorMessageListType }
     *     
     */
    public void setErrorList(ErrorMessageListType value) {
        this.errorList = value;
    }

    /**
     * Gets the value of the responseContext property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseContextType }
     *     
     */
    public ResponseContextType getResponseContext() {
        return responseContext;
    }

    /**
     * Sets the value of the responseContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseContextType }
     *     
     */
    public void setResponseContext(ResponseContextType value) {
        this.responseContext = value;
    }

    /**
     * Gets the value of the warningList property.
     * 
     * @return
     *     possible object is
     *     {@link WarningMessageListType }
     *     
     */
    public WarningMessageListType getWarningList() {
        return warningList;
    }

    /**
     * Sets the value of the warningList property.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningMessageListType }
     *     
     */
    public void setWarningList(WarningMessageListType value) {
        this.warningList = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDriverRSType.Response }
     *     
     */
    public SearchDriverRSType.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDriverRSType.Response }
     *     
     */
    public void setResponse(SearchDriverRSType.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="customer" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/customer}CustomerDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customer"
    })
    public static class Response {

        @XmlElement(nillable = true)
        protected List<CustomerDTO> customer;

        /**
         * Gets the value of the customer property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the customer property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCustomer().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CustomerDTO }
         * 
         * 
         */
        public List<CustomerDTO> getCustomer() {
            if (customer == null) {
                customer = new ArrayList<CustomerDTO>();
            }
            return this.customer;
        }

    }

}
