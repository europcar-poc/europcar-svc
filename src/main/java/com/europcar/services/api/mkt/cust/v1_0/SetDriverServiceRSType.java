
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ErrorMessageListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.ResponseContextType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.WarningMessageListType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver.DriversServiceDTO;


/**
 * <p>Java class for SetDriverServiceRSType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SetDriverServiceRSType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="errorList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ErrorMessageListType" minOccurs="0"/&gt;
 *         &lt;element name="responseContext" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}ResponseContextType" minOccurs="0"/&gt;
 *         &lt;element name="warningList" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}WarningMessageListType" minOccurs="0"/&gt;
 *         &lt;element name="response" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="services" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriversServiceDTO" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetDriverServiceRSType", propOrder = {
    "errorList",
    "responseContext",
    "warningList",
    "response"
})
public class SetDriverServiceRSType {

    protected ErrorMessageListType errorList;
    protected ResponseContextType responseContext;
    protected WarningMessageListType warningList;
    protected SetDriverServiceRSType.Response response;

    /**
     * Gets the value of the errorList property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorMessageListType }
     *     
     */
    public ErrorMessageListType getErrorList() {
        return errorList;
    }

    /**
     * Sets the value of the errorList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorMessageListType }
     *     
     */
    public void setErrorList(ErrorMessageListType value) {
        this.errorList = value;
    }

    /**
     * Gets the value of the responseContext property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseContextType }
     *     
     */
    public ResponseContextType getResponseContext() {
        return responseContext;
    }

    /**
     * Sets the value of the responseContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseContextType }
     *     
     */
    public void setResponseContext(ResponseContextType value) {
        this.responseContext = value;
    }

    /**
     * Gets the value of the warningList property.
     * 
     * @return
     *     possible object is
     *     {@link WarningMessageListType }
     *     
     */
    public WarningMessageListType getWarningList() {
        return warningList;
    }

    /**
     * Sets the value of the warningList property.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningMessageListType }
     *     
     */
    public void setWarningList(WarningMessageListType value) {
        this.warningList = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link SetDriverServiceRSType.Response }
     *     
     */
    public SetDriverServiceRSType.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link SetDriverServiceRSType.Response }
     *     
     */
    public void setResponse(SetDriverServiceRSType.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="services" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriversServiceDTO" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "services"
    })
    public static class Response {

        protected DriversServiceDTO services;

        /**
         * Gets the value of the services property.
         * 
         * @return
         *     possible object is
         *     {@link DriversServiceDTO }
         *     
         */
        public DriversServiceDTO getServices() {
            return services;
        }

        /**
         * Sets the value of the services property.
         * 
         * @param value
         *     allowed object is
         *     {@link DriversServiceDTO }
         *     
         */
        public void setServices(DriversServiceDTO value) {
            this.services = value;
        }

    }

}
