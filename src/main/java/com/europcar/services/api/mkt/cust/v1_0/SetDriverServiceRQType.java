
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.driver.DriversServiceDTO;


/**
 * <p>Java class for SetDriverServiceRQType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SetDriverServiceRQType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="request"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="services" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriversServiceDTO" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetDriverServiceRQType", propOrder = {
    "request"
})
public class SetDriverServiceRQType {

    @XmlElement(required = true)
    protected SetDriverServiceRQType.Request request;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link SetDriverServiceRQType.Request }
     *     
     */
    public SetDriverServiceRQType.Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link SetDriverServiceRQType.Request }
     *     
     */
    public void setRequest(SetDriverServiceRQType.Request value) {
        this.request = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="services" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/driver}DriversServiceDTO" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "services"
    })
    public static class Request {

        protected DriversServiceDTO services;

        /**
         * Gets the value of the services property.
         * 
         * @return
         *     possible object is
         *     {@link DriversServiceDTO }
         *     
         */
        public DriversServiceDTO getServices() {
            return services;
        }

        /**
         * Sets the value of the services property.
         * 
         * @param value
         *     allowed object is
         *     {@link DriversServiceDTO }
         *     
         */
        public void setServices(DriversServiceDTO value) {
            this.services = value;
        }

    }

}
