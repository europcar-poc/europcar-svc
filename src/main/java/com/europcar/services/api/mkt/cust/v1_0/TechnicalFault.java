
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.1.11.fuse-000243-redhat-1
 * 2018-06-02T14:50:48.532+02:00
 * Generated source version: 3.1.11.fuse-000243-redhat-1
 */

@WebFault(name = "technicalFault", targetNamespace = "http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/technical")
public class TechnicalFault extends Exception {
    
    private com.europcar.services.api.dto.cor.typ.ectypes.v1_2.technical.TechnicalFaultDTO technicalFault;

    public TechnicalFault() {
        super();
    }
    
    public TechnicalFault(String message) {
        super(message);
    }
    
    public TechnicalFault(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnicalFault(String message, com.europcar.services.api.dto.cor.typ.ectypes.v1_2.technical.TechnicalFaultDTO technicalFault) {
        super(message);
        this.technicalFault = technicalFault;
    }

    public TechnicalFault(String message, com.europcar.services.api.dto.cor.typ.ectypes.v1_2.technical.TechnicalFaultDTO technicalFault, Throwable cause) {
        super(message, cause);
        this.technicalFault = technicalFault;
    }

    public com.europcar.services.api.dto.cor.typ.ectypes.v1_2.technical.TechnicalFaultDTO getFaultInfo() {
        return this.technicalFault;
    }
}
