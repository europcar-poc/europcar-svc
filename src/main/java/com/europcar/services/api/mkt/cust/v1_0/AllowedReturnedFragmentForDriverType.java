
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AllowedReturnedFragmentForDriverType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AllowedReturnedFragmentForDriverType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="approachPoints"/&gt;
 *     &lt;enumeration value="licenses"/&gt;
 *     &lt;enumeration value="legalIds"/&gt;
 *     &lt;enumeration value="meanOfPayments"/&gt;
 *     &lt;enumeration value="readyServices"/&gt;
 *     &lt;enumeration value="branding"/&gt;
 *     &lt;enumeration value="externalReference"/&gt;
 *     &lt;enumeration value="preferences"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AllowedReturnedFragmentForDriverType")
@XmlEnum
public enum AllowedReturnedFragmentForDriverType {

    @XmlEnumValue("approachPoints")
    APPROACH_POINTS("approachPoints"),
    @XmlEnumValue("licenses")
    LICENSES("licenses"),
    @XmlEnumValue("legalIds")
    LEGAL_IDS("legalIds"),
    @XmlEnumValue("meanOfPayments")
    MEAN_OF_PAYMENTS("meanOfPayments"),
    @XmlEnumValue("readyServices")
    READY_SERVICES("readyServices"),
    @XmlEnumValue("branding")
    BRANDING("branding"),
    @XmlEnumValue("externalReference")
    EXTERNAL_REFERENCE("externalReference"),
    @XmlEnumValue("preferences")
    PREFERENCES("preferences");
    private final String value;

    AllowedReturnedFragmentForDriverType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AllowedReturnedFragmentForDriverType fromValue(String v) {
        for (AllowedReturnedFragmentForDriverType c: AllowedReturnedFragmentForDriverType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
