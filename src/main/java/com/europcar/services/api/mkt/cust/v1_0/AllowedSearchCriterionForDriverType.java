
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AllowedSearchCriterionForDriverType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AllowedSearchCriterionForDriverType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="rentalAgreement"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AllowedSearchCriterionForDriverType")
@XmlEnum
public enum AllowedSearchCriterionForDriverType {

    @XmlEnumValue("rentalAgreement")
    RENTAL_AGREEMENT("rentalAgreement");
    private final String value;

    AllowedSearchCriterionForDriverType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AllowedSearchCriterionForDriverType fromValue(String v) {
        for (AllowedSearchCriterionForDriverType c: AllowedSearchCriterionForDriverType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
