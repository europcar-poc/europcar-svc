
package com.europcar.services.api.mkt.cust.v1_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.complex.RequestContextType;
import com.europcar.services.api.dto.cor.typ.ectypes.v1_2.customer.CustomerDTO;


/**
 * <p>Java class for RegisterCustomerRQType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegisterCustomerRQType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestContext" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/complex}RequestContextType" minOccurs="0"/&gt;
 *         &lt;element name="request"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customer" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/customer}CustomerDTO" minOccurs="0"/&gt;
 *                   &lt;element name="optionList" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="driverOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="cardOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="membershipOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="membershipTransactionOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegisterCustomerRQType", propOrder = {
    "requestContext",
    "request"
})
public class RegisterCustomerRQType {

    protected RequestContextType requestContext;
    @XmlElement(required = true)
    protected RegisterCustomerRQType.Request request;

    /**
     * Gets the value of the requestContext property.
     * 
     * @return
     *     possible object is
     *     {@link RequestContextType }
     *     
     */
    public RequestContextType getRequestContext() {
        return requestContext;
    }

    /**
     * Sets the value of the requestContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestContextType }
     *     
     */
    public void setRequestContext(RequestContextType value) {
        this.requestContext = value;
    }

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link RegisterCustomerRQType.Request }
     *     
     */
    public RegisterCustomerRQType.Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegisterCustomerRQType.Request }
     *     
     */
    public void setRequest(RegisterCustomerRQType.Request value) {
        this.request = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="customer" type="{http://services.europcar.com/api/dto/cor/typ/ectypes/v1_2/customer}CustomerDTO" minOccurs="0"/&gt;
     *         &lt;element name="optionList" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="driverOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="cardOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="membershipOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="membershipTransactionOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customer",
        "optionList"
    })
    public static class Request {

        protected CustomerDTO customer;
        protected RegisterCustomerRQType.Request.OptionList optionList;

        /**
         * Gets the value of the customer property.
         * 
         * @return
         *     possible object is
         *     {@link CustomerDTO }
         *     
         */
        public CustomerDTO getCustomer() {
            return customer;
        }

        /**
         * Sets the value of the customer property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustomerDTO }
         *     
         */
        public void setCustomer(CustomerDTO value) {
            this.customer = value;
        }

        /**
         * Gets the value of the optionList property.
         * 
         * @return
         *     possible object is
         *     {@link RegisterCustomerRQType.Request.OptionList }
         *     
         */
        public RegisterCustomerRQType.Request.OptionList getOptionList() {
            return optionList;
        }

        /**
         * Sets the value of the optionList property.
         * 
         * @param value
         *     allowed object is
         *     {@link RegisterCustomerRQType.Request.OptionList }
         *     
         */
        public void setOptionList(RegisterCustomerRQType.Request.OptionList value) {
            this.optionList = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="driverOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="cardOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="membershipOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="membershipTransactionOption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class OptionList {

            @XmlAttribute(name = "driverOption")
            protected String driverOption;
            @XmlAttribute(name = "cardOption")
            protected String cardOption;
            @XmlAttribute(name = "membershipOption")
            protected String membershipOption;
            @XmlAttribute(name = "membershipTransactionOption")
            protected String membershipTransactionOption;

            /**
             * Gets the value of the driverOption property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDriverOption() {
                return driverOption;
            }

            /**
             * Sets the value of the driverOption property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDriverOption(String value) {
                this.driverOption = value;
            }

            /**
             * Gets the value of the cardOption property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCardOption() {
                return cardOption;
            }

            /**
             * Sets the value of the cardOption property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCardOption(String value) {
                this.cardOption = value;
            }

            /**
             * Gets the value of the membershipOption property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMembershipOption() {
                return membershipOption;
            }

            /**
             * Sets the value of the membershipOption property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMembershipOption(String value) {
                this.membershipOption = value;
            }

            /**
             * Gets the value of the membershipTransactionOption property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMembershipTransactionOption() {
                return membershipTransactionOption;
            }

            /**
             * Sets the value of the membershipTransactionOption property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMembershipTransactionOption(String value) {
                this.membershipTransactionOption = value;
            }

        }

    }

}
