# Generate swagger code 

swc="java -jar swagger-codegen-cli.jar" 
swc generate -i src/main/resources/swaggerdef/swagger.json -l jaxrs-cxf -DdateLibrary=legacy  --model-package restsvc.model --api-package restsvc.api -o out

# Run

mvn spring-boot:run


swagger-ui links
http://localhost:8080/services

##call soap mock
curl http://localhost:8080/camel/soapmock

##call mock rest
curl http://localhost:8080/services/mock/customers/1234?view=basic

##call service

